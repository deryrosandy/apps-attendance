<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
		$this->load->model('M_Komisi_lulur');
	}

	public function index() {
		
	}
	

	public function get_branch_name_by_branch_id(){
		
		$branch_id = $this->input->post('branch_id');
		$data = $this->M_General->get_branch_name_by_branch_id($branch_id);
		
		if($data > 0){
			echo '-';
		}else{
			echo $data;
		}
	}

	public function get_branch_list(){
		
		$branchs = $this->M_General->getAllBranch();
		
		echo json_encode($branchs);
	}

	public function data_premium_room(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		$branch_id 		= $this->input->post('branch_id');
		
		//$tkars = $this->M_General->get_kodekar_by_branch($branch_id);
		
		$premiums = $this->M_General->get_premium_room_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
		/*
		$data = array();
		
        $no = 0;
        foreach ($premiums as $prem){			
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = $prem->kodekar;
			$row[] = $prem->namakar;
			$row[] = $prem->total_premium;
			$row[] = format_digit($prem->total);
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        */
		//output to json format
        echo json_encode($premiums);
	}

	public function data_premium_room_kar(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		
		//$tkars = $this->M_General->get_kodekar_by_branch($branch_id);
		
		$premiums = $this->M_General->get_premium_room_kar_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar);
		/*
		$data = array();
		
        $no = 0;
        foreach ($premiums as $prem){			
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = $prem->kodekar;
			$row[] = $prem->namakar;
			$row[] = $prem->total_premium;
			$row[] = format_digit($prem->total);
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        */
		//output to json format
        echo json_encode($premiums);
	}

	public function get_komisi_fnb_by_kodekar(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		$branch_id 		= $this->input->post('branch_id');
		
		$komisifnb = $this->M_General->get_komisi_fnb_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
		//var_dump($komisifnb);
		//die();
		
        echo json_encode($komisifnb);
	}
	
	public function get_komisi_fnb_kar_by_kodekar(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		$branch_id 		= $this->input->post('branch_id');
		
		$komisifnb = $this->M_General->get_komisi_fnb_kar_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
		//var_dump($komisifnb);
		//die();
		
        echo json_encode($komisifnb);
	}
	
	public function delete_spv_therapist(){
		$id_spv = $this->input->post('id_spv');
		
		$result = $this->M_General->delete_spv_therapist($id_spv);
		
        echo json_encode($result);
    }
	
	public function get_komisi_lain_by_kodekar(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		$branch_id 		= $this->input->post('branch_id');
		
		$komisilain = $this->M_General->get_komisi_lain_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
		
        echo json_encode($komisilain);
	}

	public function get_komisi_kuesioner_by_kodekar(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		$branch_id 		= $this->input->post('branch_id');
		
		$komisi_kuesioner = $this->M_General->get_komisi_kuesioner_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
		
        echo json_encode($komisi_kuesioner);
	}
	
	public function save_tabungan(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		$branch_id 		= $this->input->post('branch_id');
		$jumlah_tabungan 		= $this->input->post('jumlah_tabungan');
		
		$tabungan = $this->M_General->save_tabungan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $jumlah_tabungan,  $branch_id);
		
        return 'Berhasil';
    }

	public function get_total_komisi_premium_room(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		$branch_id 		= $this->input->post('branch_id');
		
		$premiums = $this->M_General->get_total_komisi_premium_room($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
		
        echo json_encode($premiums);
    }
	
	public function get_total_sesi(){
		
		$kodekar = $this->input->post('kodekar');
		$branch_id = $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		$data = $this->M_General->get_total_session_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);

		$data_point = $this->M_General->get_total_sesi_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal);
		
		$levelkomisi 	= $this->M_General->get_level_komisi_by_kodekar($data->kodekar);
			
		$skema_komisi	 = $this->M_Komisi_lulur->get_skema_komisi_lulur($data_point->total_point, $levelkomisi);		
		
		$total_komisi_lulur = $data->total_sesi * intVal($skema_komisi);

		$data->komisi_lulur = $total_komisi_lulur;
	
		echo json_encode($data);;
	}

	public function get_total_sesi_by_branch(){
		
		$branch_id = $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		//$data = $this->M_General->get_total_session_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
		$data = $this->M_General->get_total_session_by_branch($dari_tanggal, $sampai_tanggal, $branch_id);
		//$levelkomisi 	= $this->M_General->get_level_komisi_by_kodekar($data->kodekar);
			
		//$skema_komisi	 = $this->M_Komisi_lulur->get_skema_komisi_lulur($data->total_point, $levelkomisi);	
		
		//$total_komisi_lulur = ($data->total_sesi * $skema_komisi);

		//$data->komisi_lulur = $total_komisi_lulur;
	
		echo json_encode($data);;
	}

	public function get_total_sesi_kar_by_kodekar(){
		
		$kodekar = $this->input->post('kodekar');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id = $this->input->post('branch_id');
		
		//$data = $this->M_General->get_total_session_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
		$data = $this->M_General->get_total_sesi_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal);

		//var_dump($data); die();
		
		$levelkomisi 	= $this->M_General->get_level_komisi_by_kodekar($data->kodekar);
		
		$skema_komisi	 = $this->M_Komisi_lulur->get_skema_komisi_lulur($data->total_point, $levelkomisi);	

		$total_komisi_lulur = ($data->total_sesi * intVal($skema_komisi));

		$data->komisi_lulur = $total_komisi_lulur;
	
		echo json_encode($data);;
	}

	public function get_total_sesi_by_kodekar(){
		
		$kodekar = $this->input->post('kodekar');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id = $this->input->post('branch_id');
		
		//$data = $this->M_General->get_total_session_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
		$data = $this->M_General->get_total_sesi_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal);
		
		$levelkomisi 	= $this->M_General->get_level_komisi_by_kodekar($data->kodekar);
		
		$skema_komisi	 = $this->M_Komisi_lulur->get_skema_komisi_lulur($data->total_point, $levelkomisi);	

		$total_komisi_lulur = ($data->total_sesi * intVal($skema_komisi));

		$data->komisi_lulur = $total_komisi_lulur;
	
		echo json_encode($data);;
	}

	public function get_detail_komisi_fnb(){
		
		$kodekar = $this->input->post('kodekar');
		$branch_id = $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');		
		
		$data = $this->M_General->get_detail_komisi_fnb($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	
		echo json_encode($data);;
	}

	public function get_detail_komisi_lulur(){
		
		$kodekar = $this->input->post('kodekar');
		$branch_id = $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');

		$data = $this->M_General->get_detail_komisi_lulur($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	
		echo json_encode($data);;
	}

	public function get_detail_komisi_lulur_category(){
		
		$kodekar = $this->input->post('kodekar');
		$branch_id = $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		$data = $this->M_General->get_detail_komisi_lulur_category($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	
		echo json_encode($data);;
	}

	public function get_total_add_by_kodekar(){

		$kodekar = $this->input->post('kodekar');
		$branch_id = $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		$data = $this->M_General->get_total_additional_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);	
	
		echo json_encode($data);
	}
	
	public function get_total_add_kar_by_kodekar(){

		$kodekar = $this->input->post('kodekar');
		$branch_id = $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		$data = $this->M_General->get_total_additional_kar_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);	
	
		echo json_encode($data);
	}
	
	public function get_potongan_by_kodekar(){
		$kodekar = $this->input->post('kodekar');
		$branch_id = $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');

		$data = $this->M_General->get_potongan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);	
	
		echo json_encode($data);
	}

	public function get_potongan_kar_by_kodekar(){
		$kodekar = $this->input->post('kodekar');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');

		$data = $this->M_General->get_potongan_kar_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal);	
	
		echo json_encode($data);
	}

	public function get_double_outlet_therapist(){

		$kodekar = $this->input->post('kodekar');
		$branch_id = $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');

		$data = $this->M_General->get_double_outlet_therapist($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);	
	
		echo json_encode($data);
	}

	public function get_tabungan_therapist_by_kodekar(){

		$kodekar = $this->input->post('kodekar');
		$branch_id = $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');

		$data = $this->M_General->get_tabungan_therapist_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);	
	
		echo json_encode($data);
	}

	public function edit_potongan_yayasan(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
	}

	public function save_potongan_yayasan(){
		
		$id_potongan 	= $this->input->post('id_potongan');
		$kodekar 	= $this->input->post('kodekar');
		$branch_id = $this->input->post('branch_id');
		$yayasan_value = $this->input->post('yayasan_value');
		
		$data = $this->M_General->save_potongan_by_kodekar($kodekar, $yayasan_value, $id_potongan, $branch_id);

	}

	public function set_approval_registry(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		
		$data = $this->M_General->set_approval_registry($dari_tanggal, $sampai_tanggal, $branch_id);

	}

	public function get_approval_registry(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		
		$data = $this->M_General->get_approval_registry($dari_tanggal, $sampai_tanggal, $branch_id);
		
		if(count($data) > 0){
			echo 1;
		}else{
			echo 0;
		}
	}

	public function insert_notes(){
		
		$data 	= $this->input->post();
		
		$this->M_General->insert_notes($data);
	}
	
	public function view_notes(){
		
		$kodekar 	= $this->input->post('kodekar');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		$data = $this->M_General->view_notes($dari_tanggal, $sampai_tanggal, $kodekar);

		echo json_encode($data);
	}
	
	public function update_insert_spv_therapist(){
		
		$data 	= $this->input->post();

		$data = $this->M_General->update_insert_spv_therapist($data);
	}
	
	public function hitung_gaji_spv_therapist(){
		
		$branch_id 	= $this->input->post('branch_id');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');

		$data = $this->M_General->hitung_gaji_spv_therapist($dari_tanggal, $sampai_tanggal, $branch_id);
	}

	public function update_gaji_spv_therapist(){
		
		$data 	= $this->input->post();

		$data = $this->M_General->update_gaji_spv_therapist($data);
	}

	public function get_notes(){
		
		$kodekar 	= $this->input->post('kodekar');
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		//var_dump($dari_tanggal); die();
		$count_notes = $this->M_General->get_count_notes($kodekar, $dari_tanggal, $sampai_tanggal);
		
		if($count_notes > 0){
			echo '<a href="#" id="view_notes" data-tkar="" data-from="" data-to="" style="font-size: 14px;font-weight: 700;" class=" text-yellow"><i class="fa fa-envelope-o"></i> ' . $count_notes . '</a>';
		}
	}

	public function get_user_by_id(){
		
		$id_user 	= $this->input->post('id_user');
		
		$data = $this->M_General->get_user_by_id($id_user);
		
		echo json_encode($data);

	}
	
	public function net_sales(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		$data = $this->M_General->getNetSales($dari_tanggal, $sampai_tanggal);
		
		if($data == 0){
			echo '-';
		}else{
			echo format_rupiah($data);
		}
	}
	
	public function number_of_transaction(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		$data = $this->M_General->getNumbOfTransaction($dari_tanggal, $sampai_tanggal);
		
		if($data == 0){
			echo '-';
		}else{
			echo $data;
		}
	}
	
	public function avg_sale(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		$data = $this->M_General->getAvgSale($dari_tanggal, $sampai_tanggal);
		
		if($data == 0){
			echo '-';
		}else{
			echo format_rupiah($data);
		}
	}
	
	public function total_treatment(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$data = $this->M_General->get_total_treatment($dari_tanggal, $sampai_tanggal, $branch_id);
		
		echo json_encode($data);
	}

	public function total_mud(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$data = $this->M_General->get_total_mud($dari_tanggal, $sampai_tanggal, $branch_id);
		
		echo json_encode($data);
	}
	
	public function total_hotstone(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$data = $this->M_General->get_total_hotstone($dari_tanggal, $sampai_tanggal, $branch_id);
		
		echo json_encode($data);
	}
	
	public function total_aroma(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$data = $this->M_General->get_total_aroma($dari_tanggal, $sampai_tanggal, $branch_id);
		
		echo json_encode($data);
	}
	
	public function total_lulur(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$data = $this->M_General->get_total_lulur($dari_tanggal, $sampai_tanggal, $branch_id);
		//var_dump($data); die();
		echo json_encode($data);
	}
	
	public function komisi_hotstone(){
		$status 	= $this->input->post('status');
		$data = $this->M_General->get_komisi_hotstone($status);
		//var_dump($data); die();
		echo json_encode($data);
	}

	public function komisi_aroma(){
		$data = $this->M_General->get_komisi_aroma();
		echo json_encode($data);
	}
	
	public function komisi_mud(){
		$data = $this->M_General->get_komisi_mud();
		echo json_encode($data);
	}
	
	public function komisi_lulur(){
		$data = $this->M_General->get_komisi_lulur();
		echo json_encode($data);
	}
	
	public function data_point_detail_gro(){
		$data = $this->M_General->get_point_detail_gro();
		echo json_encode($data);
	}

	public function data_gro_period(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$data = $this->M_General->get_gro_period($dari_tanggal, $sampai_tanggal, $branch_id);

		echo json_encode($data);
	}
	
	public function data_hot_by_kodegro(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$kodegro		= $this->input->post('kodegro');

		$data = $this->M_General->get_total_hot_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);

		echo json_encode($data);
	}
	
	public function data_gaji_spv_therapist(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');

		$data = $this->M_General->get_data_gaji_spv_therapist($dari_tanggal, $sampai_tanggal, $branch_id);

		echo json_encode($data);
	}
	
	public function data_spv_therapist(){
		$branch_id	= $this->input->post('branch_id');

		$data = $this->M_General->get_data_spv_therapist($branch_id);

		echo json_encode($data);
	}

	public function data_edit_spv_therapist(){
		$id_spv 	= $this->input->post('id_spv');
		
		$data = $this->M_General->get_spv_therapist_by_id($id_spv);
		
        $output = array(
				"data" => $data,
				);
				
        echo json_encode($output);
	}

	public function data_edit_gaji_spv_therapist(){
		$id_gaji 			= $this->input->post('id_gaji');
		$dari_tanggal 		= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');

		$data = $this->M_General->get_data_gaji_spv_therapist_periode_by_id($id_gaji, $dari_tanggal, $sampai_tanggal);
		
        $output = array(
				"data" => $data,
				);
				
        echo json_encode($output);
	}

	public function data_edit_potongan_spv_therapist(){
		$id_spv 			= $this->input->post('id_spv');
		$dari_tanggal 		= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');

		$data = $this->M_General->get_data_potongan_spv_therapist_periode_by_id($id_spv, $dari_tanggal, $sampai_tanggal);
		
        $output = array(
				"data" => $data,
				);
				
        echo json_encode($output);
	}

	
	public function data_aroma_by_kodegro(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$kodegro		= $this->input->post('kodegro');

		$data = $this->M_General->get_total_aroma_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);

		echo json_encode($data);
	}
	
	public function data_lulur_by_kodegro(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$kodegro		= $this->input->post('kodegro');

		$data = $this->M_General->get_total_lulur_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);

		echo json_encode($data);
	}
	
	public function data_mud_by_kodegro(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$kodegro		= $this->input->post('kodegro');

		$data = $this->M_General->get_total_mud_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);

		echo json_encode($data);
	}
	
	public function data_suite_by_kodegro(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$kodegro		= $this->input->post('kodegro');

		$data = $this->M_General->get_total_suite_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);
		//var_dump($data);

		echo json_encode($data);
	}
	
	public function data_total_point_by_kodegro(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');
		$kodegro		= $this->input->post('kodegro');

		//var_dump($branch_id); die();

		$data = $this->M_General->get_total_point_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);

		echo json_encode($data);
	}
	
	public function data_total_point_all(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');

		$data = $this->M_General->get_total_point_all($dari_tanggal, $sampai_tanggal, $branch_id);

		echo json_encode($data);
	}

	public function data_point_per_gro(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id		= $this->input->post('branch_id');

		$data = $this->M_General->get_point_per_gro($dari_tanggal, $sampai_tanggal, $branch_id);

		echo json_encode($data);
	}
	
	public function sales_average(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		$columns = array( 
                            0 =>'Tanggal',
                            1=> 'TotalBill',
                            2=> 'AvgSales',
                        );

		$limit = $this->input->post('length');
        $start = $this->input->post('start');
		
		$order = $columns[$this->input->post('order')[0]['column']];
		
		$dir = $this->input->post('order')[0]['dir'];
		
        $totalData = $this->M_General->getCountSalesAverage($dari_tanggal, $sampai_tanggal);
         
		$totalFiltered = $totalData; 
        
		$lists = $this->M_General->getSalesAverage($dari_tanggal, $sampai_tanggal, $limit,$start, $order, $dir);
		
		//var_dump($lists); die();
		
		$data = array();
        
		if(!empty($lists)){
			
			$no = 1;
			
            foreach ($lists as $list){
				$nestedData['No'] = $no;
				$nestedData['Tanggal'] = tgl_indo($list->Tanggal);
                $nestedData['TotalBill'] = $list->TotalBill;
                $nestedData['AvgSales'] = format_digit($list->AvgSales);
                
                $data[] = $nestedData;
				$no++;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
		
	}
	
	/*
	public function sales_average(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
		$list = $this->M_General->getSalesAverage($dari_tanggal, $sampai_tanggal);
		
		$data = array();
		
        $no = 0;
		
        foreach ($list as $list_data) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->TotalBill;
            $row[] = format_digit($list_data->AvgSales);
            $data[] = $row;
        }

		$output = array(
					"draw" => $_POST['draw'],
					"data" => $data,
                );
				
        echo json_encode($output);
	}
	*/
}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */