<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
		$this->load->model('M_Attendance');
	}

	public function index() {
		$this->attendance_list();
	}

	public function attendance_list($date = NULL) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "Attendance Log";
		$data['judul'] 			= "Attendance Log";
		$data['deskripsi'] 		= "";	

		if (!empty($date)){
			
			$data['attendance'] = $this->M_General->get_all_attendance($date);
			$data['date'] = $date;

			$this->template->views('attendance/attendance_list', $data);

		}else{
			
			$flag = $this->input->post('flag', TRUE);

			$date = $this->input->post('date', TRUE);

			if (!empty($flag) && !empty($date)) { 
                $data['flag'] = 1;
				if (!empty($date)) {
                    $data['date'] = $date;
                } else {
                    $data['date'] = substr($this->input->post('date', TRUE), 0, 7);
				}

				$data['attendance'] = $this->M_General->get_all_attendance($date);                   
                   
			}else{
				
				$data['attendance'] = $this->M_General->get_all_attendance($date);  
			}			
			
			$this->template->views('attendance/attendance_list', $data);

		}
		
	}

	public function Pull_log($branch_id = NULL) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "pull_log";
		$data['judul'] 			= "Pull Log";
		$data['deskripsi'] 		= "";	
		
		//$data['branchs'] = $this->M_General->get_all_branch();

		//var_dump($this->input->post()); die();
		if (empty($branch_id)){
			
			//$data['attendance_info'] = $this->M_Attendance->pull_attendance_log($branch_id); 
			$data['branchs'] = $this->M_General->get_all_branch();	
			$data['branch_id'] = '';
			//var_dump($data); die();		
			$this->template->views('attendance/pull_finger', $data);

		}else{
		
			$flag = $this->input->post('flag', TRUE);

			$branch_id = $this->input->post('branch_id', TRUE);

			if (!empty($flag) && !empty($branch_id)) { 
                $data['flag'] = 1;
				if (!empty($branch_id)) {
                    $data['branch_id'] = $branch_id;
                } else {
                    $data['branch_id'] = $this->input->post('branch_id', TRUE);
				}

                if (!empty($branch_id)) {
                    $data['branch_id'] = $branch_id;
                } else {
                    if($this->input->post('branch_id') == '' || $this->input->post('branch_id') == '0'){
                        $data['branch_id'] = 'all';
                    }else{
                        $data['branch_id'] = $this->input->post('branch_id', TRUE);
                    }
				}
				
				$data['fingerspot_status'] = $this->M_Attendance->get_status_fingerspot($data['branch_id']);                     
				
				if($data['fingerspot_status'] == true){

					$data['attendance_log'] = $this->M_Attendance->get_attendance_log_by_branch_id($data['branch_id']);
					
					$this->template->views('attendance/pull_finger', $data);

				}else{
					$this->session->set_flashdata('msg', show_err_msg('Fingerprint Offline'));
				}
			}		
			
			$this->template->views('attendance/pull_finger', $data);

		}
        
	}

	public function print_kinerja_therapist($kodekar = NULL, $report_month = NULL) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "kinerja_therapist";
		$data['judul'] 			= "Detail Therapist";
		$data['deskripsi'] 		= "Detail Therapist";
		$data['icon'] 			= "icon ion-ios-gear-outline";		

		$data['branchs'] = $this->M_General->get_all_branch();

		if (!empty($kodekar) || !empty($report_month)){
			
			$data['therapist'] = $this->M_General->get_therapist_detail_by_kodekar_month($kodekar, $report_month); 
			$data['therapist_massage'] = $this->M_General->get_therapist_massage_by_kodekar_month($kodekar, $report_month); 
			$data['therapist_performance'] = $this->M_General->get_therapist_performance_by_kodekar_month($kodekar, $report_month); 
			$data['therapist_services'] = $this->M_General->get_therapist_services_by_kodekar_month($kodekar, $report_month); 
			$data['therapist_kedisiplinan'] = $this->M_General->get_therapist_kedisiplinan_by_kodekar_month($kodekar, $report_month); 
			$data['therapist_kesehatan'] = $this->M_General->get_therapist_kesehatan_by_kodekar_month($kodekar, $report_month); 
			$data['therapist_sikap'] = $this->M_General->get_therapist_sikap_by_kodekar_month($kodekar, $report_month); 
			$data['therapist_grade'] = $this->M_General->get_therapist_grade_by_kodekar_month($kodekar, $report_month); 
			$data['month'] = $report_month;

			$this->pdf->load_view('penilaian/print_kinerja_therapist', $data);
			$this->pdf->set_paper('A4', 'portrait');
			$this->pdf->render();
			$this->pdf->stream("kinerja_therapist" . str_replace("-","", $data['month']) . $data['therapist']->nit . ".pdf", array("Attachment" => false));		

		}else{
			
			$flag = $this->input->post('flag', TRUE);

			$report_month = $this->input->post('report_month', TRUE);

			if (!empty($flag) && !empty($report_month)) { 
                $data['flag'] = 1;
				if (!empty($report_month)) {
                    $data['report_month'] = $report_month;
                } else {
                    $data['report_month'] = substr($this->input->post('report_month', TRUE), 0, 7);
				}
				
				//var_dump($this->input->post('branch_id')); die();

                if (!empty($branch_id)) {
                    $data['branch_id'] = $branch_id;
                } else {
                    if($this->input->post('branch_id') == '' || $this->input->post('branch_id') == '0'){
                        $data['branch_id'] = 'all';
                    }else{
                        $data['branch_id'] = $this->input->post('branch_id', TRUE);
                    }
				}

				$data['therapists'] = $this->M_General->get_therapist_by_branch_id($data['branch_id']);                     
                   
			}			
			
			$this->template->views('penilaian/kinerja_therapist', $data);

		}

		/*
        if($data['userdata']->user_type == 'superadmin'){
			
			
			$this->template->views('detail_therapist', $data);
		
		}elseif($data['userdata']->user_type == 'admin_outlet'){
			
			$this->template->views('detail_therapist', $data);
		
		}else{
			
			$this->template->views('detail_therapist', $data);
		
		}
		*/
        
	}

	public function Pull_attendance_log($branch_id = NULL, $flag = null) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "pull_log";
		$data['judul'] 			= "Pull Log";	

		
		$flag = $this->input->post('flag', TRUE);

		$branch_id = $this->input->post('branch_id', TRUE);

		$data['branch_id'] = $branch_id;

		$data['branchs'] = $this->M_General->get_all_branch();			
		//var_dump($flag); die();
		if (!empty($flag)) { 
			
			$data['flag'] = 1;
			if (!empty($branch_id)) {
				$data['branch_id'] = $branch_id;
			} else {
				$data['branch_id'] = $this->input->post('branch_id', TRUE);
			}

			if (!empty($branch_id)) {
				$data['branch_id'] = $branch_id;
			} else {
				if($this->input->post('branch_id') == '' || $this->input->post('branch_id') == '0'){
					$data['branch_id'] = 'all';
				}else{
					$data['branch_id'] = $this->input->post('branch_id', TRUE);
				}
			}
			
			$data['fingerspot_status'] = $this->M_Attendance->get_status_fingerspot($data['branch_id']);                     
			
			if($data['fingerspot_status'] == true){

				$data['attendance_log'] = $this->M_Attendance->get_attendance_log_by_branch_id($data['branch_id']);
				
				$this->template->views('attendance/pull_finger', $data);

			}else{
				
				$this->session->set_flashdata('msg', show_err_msg('Fingerprint Offline'));
			}
		
		}else{
			/*
			if (!empty($flag)) { 
				$data['flag'] = 1;

				if (!empty($branch_id)) {
					$data['branch_id'] = $branch_id;
				} else {
					if($this->input->post('branch_id') == '' || $this->input->post('branch_id') == '0'){
						$data['branch_id'] = 'all';
					}else{
						$data['branch_id'] = $this->input->post('branch_id', TRUE);
					}
				}  
				
			}
			*/

		}

		$this->template->views('attendance/pull_finger', $data);
        
	}

	public function upload_image($kodekar) {
		var_dump($_FILES['therapist_image']); die();
	}

	public function change_password() {
		$this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
		$this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
		$this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');
		
		$UserID = $this->userdata->id_user;
	
		if ($this->form_validation->run() == TRUE) {
			if (($this->input->post('passLama')) == $this->userdata->password) {
				if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
					$this->session->set_flashdata('msg', show_err_msg('Password Baru dan Konfirmasi Password harus sama'));
					redirect('profile');
				} else {
					$data = [
						'password' => $this->input->post('passBaru')
					];
					
					$result = $this->M_admin->update($data, $UserID);
					
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Password Berhasil diubah'));
						redirect('profile');
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Password Gagal diubah'));
						redirect('profile');
					}
				}
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Password Salah'));
				redirect('profile');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('profile');
		}
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */