<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
		$this->load->model('M_Additional_Prod');
		$this->load->model('M_Komisi_lulur');
	}

	public function index() {
		
		$this->branch_list();
	}

	public function branch_list() {

		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "Laporan Gaji Therapist";
		$data['judul'] 			= "Laporan Gaji Therapist";
		$data['deskripsi'] 		= "Pilih Outlet";

		$data['list_branch'] = $this->M_General->get_branch_list();

		$this->template->views('list_branch', $data);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */