<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_waiter');
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "calendar";
		$data['judul'] = "Data Calendar";
		$data['deskripsi'] = "";
		$this->template->views('calendar', $data);
    }
}