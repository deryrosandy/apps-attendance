<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
	}

	public function index() {

		$data['userdata'] = $this->userdata;

		$data['page'] 			= "dashboard";
		$data['judul'] 			= "Dashboard";
		$data['deskripsi'] 		= "Anev Report System";
		$data['icon'] 			= "ion-ios-home-outline";

		$date_now = date('Y-m-d');
			
		if($data['userdata']->user_type == 'superadmin'){
			
			$data['branchs'] = $this->M_General->get_all_branch();
			$this->template->views('dashboard', $data);
		
		}elseif($data['userdata']->user_type == 'supervisor'){
			
			$this->template->views('dashboard', $data);
		
		}else{
			
			$this->template->views('dashboard', $data);
		
		}
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */