<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guest_Comment extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_Data_therapist', 'M_General'));
	}
	
	public function index() {
		$this->guest_comment_menu();
	}

	public function guest_comment_menu() {
		
		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "guest_comment";
		$data['judul'] 			= "Guest Comment";
		$data['deskripsi'] 		= "Pilih Menu";

		$data['branch_id'] = $this->userdata->branch_id;

		$this->template->views('guest_comment_menu', $data);
	}

	public function branch_list() {
		
		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "guest_comment";
		$data['judul'] 			= "Grade Therapist";
		$data['deskripsi'] 		= "Pilih Outlet";

		$data['list_branch'] = $this->M_General->get_branch_list();

		$this->template->views('guest_comment_list', $data);
	}
	
	public function guest_comment_filter(){

		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "guest_comment";
		$data['judul'] 			= "Guest Comment";
		$data['deskripsi'] 		= "Pilih Outlet";

		$data['list_branch'] = $this->M_General->get_branch_list();

		$this->template->views('guest_comment_list_filter', $data);
	}

	public function select_branch(){

		$data['userdata'] = $this->userdata;
		$data['page'] = "Grade Therapist";
		$data['judul'] = "Grade Therapist";		
	
		$branch_id = $_GET['branch_code'];
		$date1 = date("Y-m-21",strtotime("-1 month"));
		$date2 = date("Y-m-20",strtotime("0 month"));
		$datenow = date("Y-m-20");

		$data['branch'] = $this->M_General->get_branch_by_branch_id($branch_id);

		$data['deskripsi'] = $data['branch']->cname;

		$data['therapist'] = $this->M_General->get_therapist_by_branch_id($date1, $date2, $branch_id);

		$this->template->views('guest_comment_branch', $data);

	}

	public function select_branch_filter(){

		$data['userdata'] = $this->userdata;
		$data['page'] = "Data Therapist";
		$data['judul'] = "Data Therapist";		
	
		$branch_id = $_GET['branch_code'];
		$date1 = date("Y-m-21",strtotime("-1 month"));
		$date2 = date("Y-m-20",strtotime("0 month"));
		$datenow = date("Y-m-20");

		$data['branch'] = $this->M_General->get_branch_by_branch_id($branch_id);

		$data['deskripsi'] = $data['branch']->cname;

		$data['therapist'] = $this->M_General->get_therapist_by_branch_id($date1, $date2, $branch_id);

		$this->template->views('guest_comment_branch_filter', $data);

	}

	public function list_data_therapist() {

		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];

		/*Menghitung total desa didalam database*/
		$total=$this->db->count_all_results("tkar");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();

		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("namakar",$search);
		}

		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('kodekar','DESC');
		$query=$this->db->get('tkar');

		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("namakar",$search);
		$jum=$this->db->get('tkar');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		$no=$start+1;
		foreach ($query->result_array() as $therapist) {
			$output['data'][]=array(
				$no,
				$therapist['kodetrp'],
				$therapist['namakar'],
				tgl_indo2($therapist['tglmasuk']),
				get_level_therapist($therapist['levelkomisi']),
				$therapist['no_rek'],
				$therapist['no_ktp'],
				$therapist['supplier_name'],
				$therapist['supplier_phone'],
				$therapist['supplier_phone'],
				'<div class="btn-group" style="display: -webkit-inline-box">
					<button class="btn btn-xs btn-primary view_therapist" kodekar=' . $therapist["kodekar"] . '><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View</button>
				</div>',
			);
		$no++;
		}
		
		echo json_encode($output);
	}

	public function detail_therapist() {
		$kodekar 	= $this->input->post('kodekar');
		
		$data_therapist = $this->M_General->get_therapist_by_kodekar($kodekar);
		
		$output = array(
			"data" => $data_therapist,
		);
		
		echo json_encode($data_therapist);
	}
	
	public function update_therapist(){		
		$data = $this->input->post();		
		$branch_code = $this->input->post('branch_code');
		$kodekar  = $data['kodekar'];
		$operation  =	$data['operation'];
		//var_dump($data); die();
		unset($data['kodekar']);
		unset($data['operation']);
		unset($data['branch_code']);
		
		$result = $this->M_Data_therapist->update($kodekar, $data);
			
		if ($result > 0) {
			//$this->update_therapist();
			$this->session->set_flashdata('msg', show_succ_msg('Data Karyawan Berhasil diubah'));
			redirect('data_therapist/select_branch?branch_code=' . $branch_code);
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Data Karyawan Gagal diubah'));
			redirect('data_therapist/select_branch?branch_code=' . $branch_code);
		}
	}

	public function ranking_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Ranking Penjualan";
		$this->template->views('kasir/ranking_penjualan', $data);
	}
	
	public function ajax_ranking_penjualan() {
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');		
		
		$list = $this->M_kasir->get_ranking_penjualan($dari_tanggal, $sampai_tanggal);
		$data = array();
		$toal_all = 0;
		
		$no = $_POST['start'];
		foreach ($list as $list_data){
			$menu = $this->M_kasir->get_menu_name($list_data->Kode);
			$total = $list_data->Total + $list_data->PPN + $list_data->Service;			
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $menu->Nama;
			$row[] = $list_data->Qty;
			//$row[] = (format_digit($total));
			$row[] = $total;
			$data[] = $row;
			$toal_all += $list_data->Total;
		}
		
		//var_dump($sum); die();
		
		$output = array(
					"draw" => $_POST['draw'],
					"data" => $data,
					"toal_all" => $toal_all,
				);
		//output to json format
		echo json_encode($output);
	}

	public function laba_rugi() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Laba/Rugi";
		$this->template->views('kasir/laba_rugi', $data);
	}

	public function summary_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Summary";
		$this->template->views('kasir/summary_penjualan', $data);
	}

	public function rekap_nominal() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Rekap Nominal";
		$this->template->views('kasir/rekap_nominal', $data);
	}

	public function ajax_harian(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_filter_harian($dari_tanggal, $sampai_tanggal);
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
            $row[] = getHour($list_data->Jam);
            $row[] = format_digit($list_data->Total);
            $data[] = $row;
        }

		//var_dump($_POST); die();
		
        $output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->M_kasir->count_harian($dari_tanggal, $sampai_tanggal),
					"recordsFiltered" => $this->M_kasir->count_filtered_harian($dari_tanggal, $sampai_tanggal),
					"data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function ajax_detail_penjualan(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_detail_penjualan($dari_tanggal, $sampai_tanggal);
		//var_dump($list); die();
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
			//print_r($list_data);
			$detail 		= $this->M_kasir->get_detail_bill($list_data->Nomor);
			$tabel_guard 	= $this->M_kasir->get_tabel_guard($list_data->Table_Guard)->Nama;
			foreach($detail as $list){
				$Nama 		= $list->Nama;
				$Quantity 	= $list->Quantity;
				$Harga 		= $list->Harga;
				$Disc 		= $list->Disc;
				$Jumlah 	= $Quantity * $Harga;

				$sub_total[] = $Jumlah; 
				$output2[] = array(
								"Nama" => $Nama,
								"Quantity" => $Quantity,
								"Harga" => format_digit($Harga),
								"Disc" => $Disc,
								"Jumlah" => format_digit($Jumlah)
							);
			};
			$Jumlah = array_sum($sub_total);
			
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
			$row[] = $tabel_guard;
            //$row[] = format_digit($list_data->Harga * $list_data->Quantity);
            $row[] = format_digit($Jumlah);
            $row[] = format_digit($list_data->Disc);
            $row[] = format_digit($list_data->Voucher);
			//PPN
            $row[] = format_decimal(($list_data->PPN / 100) * $Jumlah);
			//ServiceCharge
            $row[] = format_digit(round($list_data->ServiceCharge * $Jumlah / 100, 0));
            $row[] = format_digit($list_data->Total);
            $row[] = $list_data->Pembuat;
            $row[] = getHour($list_data->Jam);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_kasir->count_all($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_kasir->count_filtered_detail($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        echo json_encode($output);
    }
	
	public function ajax_harian_all(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_filter_harian($dari_tanggal, $sampai_tanggal);
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
            $row[] = getHour($list_data->Jam);
            $row[] = format_digit($list_data->Total);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_kasir->count_harian($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_kasir->count_filtered_harian($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function kasir_detail(){
		$nomor = $this->input->post('nomor');
        $list_data = $this->M_kasir->get_detail_bill($nomor);
		foreach($list_data as $list){
			//var_dump($list);
			$Nama 		= $list->Nama;
			$Quantity 	= $list->Quantity;
			$Harga 		= $list->Harga;
			$Disc 		= $list->Disc;
			$Jumlah 	= $Quantity * $Harga;

			$output[] = array(
							"Nama" => $Nama,
							"Quantity" => $Quantity,
							"Harga" => format_digit($Harga),
							"Disc" => $Disc,
							"Jumlah" => format_digit($Jumlah)
						);
		}
		
		 echo json_encode($output);
    }
}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */