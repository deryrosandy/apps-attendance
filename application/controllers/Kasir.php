<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasir extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_kasir');
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Transaksi Kasir";
		$data['deskripsi'] = "";
		$this->template->views('kasir/report_harian', $data);
	}
	
	public function report_harian() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Laporan Harian";
		$this->template->views('kasir/report_harian', $data);
	}

	public function detail_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Detail Penjualan";
		$this->template->views('kasir/detail_penjualan', $data);
	}

	public function rekap_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Rekap Penjualan";
		
		$this->template->views('kasir/rekap_penjualan', $data);
	}
	
	public function ajax_rekap_penjualan(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
				
        $data_rekap = $this->M_kasir->get_rekap_penjualan($dari_tanggal, $sampai_tanggal);
		
		$data = array();
		
        $no = $_POST['start'];
        foreach ($data_rekap as $rekap) {
			$no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($rekap->Tanggal);
            $row[] = $rekap->Nomor;
            $row[] = $rekap->NoMeja;
            $row[] = $rekap->NoPesan;
            $row[] = getHour($rekap->Jam);
            $row[] = format_digit($rekap->Total);
            $data[] = $row;
        }

		//var_dump($_POST); die();
		
        $output = array(
					"data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	public function ranking_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Ranking Penjualan";
		$this->template->views('kasir/ranking_penjualan', $data);
	}
	
	public function ajax_ranking_penjualan() {
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');		
		
		$list = $this->M_kasir->get_ranking_penjualan($dari_tanggal, $sampai_tanggal);
		$data = array();
		$toal_all = 0;
		
		$no = $_POST['start'];
		foreach ($list as $list_data){
			$menu = $this->M_kasir->get_menu_name($list_data->Kode);
			$total = $list_data->Total + $list_data->PPN + $list_data->Service;			
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $menu->Nama;
			$row[] = $list_data->Qty;
			//$row[] = (format_digit($total));
			$row[] = $total;
			$data[] = $row;
			$toal_all += $list_data->Total;
		}
		
		//var_dump($sum); die();
		
		$output = array(
					"draw" => $_POST['draw'],
					"data" => $data,
					"toal_all" => $toal_all,
				);
		//output to json format
		echo json_encode($output);
	}

	public function laba_rugi() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Laba/Rugi";
		$this->template->views('kasir/laba_rugi', $data);
	}

	public function summary_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Summary";
		$this->template->views('kasir/summary_penjualan', $data);
	}

	public function rekap_nominal() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Rekap Nominal";
		$this->template->views('kasir/rekap_nominal', $data);
	}

	public function ajax_harian(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_filter_harian($dari_tanggal, $sampai_tanggal);
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
            $row[] = getHour($list_data->Jam);
            $row[] = format_digit($list_data->Total);
            $data[] = $row;
        }

		//var_dump($_POST); die();
		
        $output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->M_kasir->count_harian($dari_tanggal, $sampai_tanggal),
					"recordsFiltered" => $this->M_kasir->count_filtered_harian($dari_tanggal, $sampai_tanggal),
					"data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function ajax_detail_penjualan(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_detail_penjualan($dari_tanggal, $sampai_tanggal);
		//var_dump($list); die();
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
			//print_r($list_data);
			$detail 		= $this->M_kasir->get_detail_bill($list_data->Nomor);
			$tabel_guard 	= $this->M_kasir->get_tabel_guard($list_data->Table_Guard)->Nama;
			foreach($detail as $list){
				$Nama 		= $list->Nama;
				$Quantity 	= $list->Quantity;
				$Harga 		= $list->Harga;
				$Disc 		= $list->Disc;
				$Jumlah 	= $Quantity * $Harga;

				$sub_total[] = $Jumlah; 
				$output2[] = array(
								"Nama" => $Nama,
								"Quantity" => $Quantity,
								"Harga" => format_digit($Harga),
								"Disc" => $Disc,
								"Jumlah" => format_digit($Jumlah)
							);
			};
			$Jumlah = array_sum($sub_total);
			
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
			$row[] = $tabel_guard;
            //$row[] = format_digit($list_data->Harga * $list_data->Quantity);
            $row[] = format_digit($Jumlah);
            $row[] = format_digit($list_data->Disc);
            $row[] = format_digit($list_data->Voucher);
			//PPN
            $row[] = format_decimal(($list_data->PPN / 100) * $Jumlah);
			//ServiceCharge
            $row[] = format_digit(round($list_data->ServiceCharge * $Jumlah / 100, 0));
            $row[] = format_digit($list_data->Total);
            $row[] = $list_data->Pembuat;
            $row[] = getHour($list_data->Jam);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_kasir->count_all($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_kasir->count_filtered_detail($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        echo json_encode($output);
    }
	
	public function ajax_harian_all(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_filter_harian($dari_tanggal, $sampai_tanggal);
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
            $row[] = getHour($list_data->Jam);
            $row[] = format_digit($list_data->Total);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_kasir->count_harian($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_kasir->count_filtered_harian($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function kasir_detail(){
		$nomor = $this->input->post('nomor');
        $list_data = $this->M_kasir->get_detail_bill($nomor);
		foreach($list_data as $list){
			//var_dump($list);
			$Nama 		= $list->Nama;
			$Quantity 	= $list->Quantity;
			$Harga 		= $list->Harga;
			$Disc 		= $list->Disc;
			$Jumlah 	= $Quantity * $Harga;

			$output[] = array(
							"Nama" => $Nama,
							"Quantity" => $Quantity,
							"Harga" => format_digit($Harga),
							"Disc" => $Disc,
							"Jumlah" => format_digit($Jumlah)
						);
		}
		
		 echo json_encode($output);
    }
}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */