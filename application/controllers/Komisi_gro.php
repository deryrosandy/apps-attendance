<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komisi_gro extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
	}

	public function index() {
		
		$this->branch_list();
	}

	public function branch_list() {

		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "Laporan Komisi GRO";
		$data['judul'] 			= "Laporan Komisi GRO";
		$data['deskripsi'] 		= "Pilih Outlet";

		$data['list_branch'] = $this->M_General->get_branch_list();

		$this->template->views('list_branch_gro', $data);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */