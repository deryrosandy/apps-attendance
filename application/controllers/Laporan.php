<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
	}

	public function index() {
		$this->laporan_tahunan();
	}
	
	public function laporan_tahunan($kodekar = NULL, $year = NULL) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "laporan_tahunan";
		$data['judul'] 			= "Laporan Therapist";
		$data['deskripsi'] 		= "Laporan Therapist";
		$data['icon'] 			= "icon ion-ios-gear-outline";		

		$data['branchs'] = $this->M_General->get_all_branch();

		//var_dump($this->input->post()); die();

		if (!empty($kodekar) || !empty($year)){
			
			$data['therapist'] = $this->M_General->get_therapist_detail_by_kodekar_month($kodekar, $year); 
			$data['therapist_massage'] = $this->M_General->get_therapist_massage_by_kodekar_year($kodekar, $year); 
			$data['therapist_performance'] = $this->M_General->get_therapist_performance_by_kodekar_year($kodekar, $year); 
			$data['therapist_services'] = $this->M_General->get_therapist_services_by_kodekar_year($kodekar, $year); 
			$data['therapist_kedisiplinan'] = $this->M_General->get_therapist_kedisiplinan_by_kodekar_year($kodekar, $year); 
			$data['therapist_kesehatan'] = $this->M_General->get_therapist_kesehatan_by_kodekar_year($kodekar, $year); 
			$data['therapist_sikap'] = $this->M_General->get_therapist_sikap_by_kodekar_year($kodekar, $year); 
			$data['therapist_grade'] = $this->M_General->get_therapist_grade_by_kodekar_year($kodekar, $year); 
			$data['therapist_photo'] = $this->M_General->get_therapist_photo_by_kodekar_year($kodekar, $year); 
			$data['year'] = $year;	
			
			$this->pdf->load_view('laporan/print_kinerja_therapist', $data);
			$this->pdf->set_paper('Letter', 'landscape');
			$this->pdf->render();
			$this->pdf->stream("laporan_kinerja_therapist" . str_replace("-","", $data['year']) . $data['therapist']->nit . ".pdf", array("Attachment" => false));		

		}else{
			
			$flag = $this->input->post('flag', TRUE);

			$year = $this->input->post('year', TRUE);

			if (!empty($flag) && !empty($year)) { 
                $data['flag'] = 1;
				if (!empty($year)) {
                    $data['year'] = $year;
                } else {
                    $data['year'] = $this->input->post('year', TRUE);
				}

                if (!empty($branch_id)) {
                    $data['branch_id'] = $branch_id;
                } else {
                    if($this->input->post('branch_id') == '' || $this->input->post('branch_id') == '0'){
                        $data['branch_id'] = 'all';
                    }else{
                        $data['branch_id'] = $this->input->post('branch_id', TRUE);
                    }
				}

				$data['therapists'] = $this->M_General->get_therapist_by_branch_id($data['branch_id']);                     
                   
			}		
			
			$this->template->views('laporan/laporan_tahunan', $data);

		}
        
	}

}