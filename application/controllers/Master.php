<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_admin', 'M_General'));
	}

	public function index() {

		$this->users();
	}

	public function ajax_get_detail_user() {

		$id_user = $this->input->post('id_user');	

		$output = $this->M_General->get_user_by_id($id_user);
		
		echo json_encode($output);
	
	}

	public function division() {

		$data['userdata'] 		= $this->userdata;	

		$data['paket_layanan'] = $this->M_General->get_all_division();

		$data['page'] 			= "division";
		$data['judul'] 			= "Division List";
		$data['deskripsi'] 		= "";
		
		$this->template->views('master/division_list', $data);
	}

	public function tambah_paket_layanan() {

		$data['userdata'] 		= $this->userdata;	

		$data['paket_layanan'] = $this->M_General->get_all_paket_layanan();

		$data['page'] 			= "tambah_paket_layanan";
		$data['judul'] 			= "Data Paket Layanan";
		$data['deskripsi'] 		= "";
		
		$this->template->views('master/tambah_paket_layanan', $data);
	}


	public function customer() {

		$data['userdata'] 		= $this->userdata;	

		$data['customers'] = $this->M_General->get_all_customer();

		$data['page'] 			= "customer";
		$data['judul'] 			= "Data Customer";
		$data['deskripsi'] 		= "";
		
		$this->template->views('master/list_customer', $data);
	}

	public function users() {

		$data['userdata'] 		= $this->userdata;	

		$data['page'] 			= "users";
		$data['judul'] 			= "Data Customer";
		$data['deskripsi'] 		= "";
		
		$this->template->views('master/list_user', $data);
	}

	public function tambah_customer() {

		$data['userdata'] 		= $this->userdata;	

		$data['customers'] = $this->M_General->get_all_customer();

		$data['page'] 			= "tambah_customer";
		$data['judul'] 			= "Tambah Customer";
		$data['deskripsi'] 		= "";
		
		$this->template->views('master/tambah_customer', $data);
	}

	public function update_paket_layanan($paket_layanan_id) {

		$data['userdata'] 		= $this->userdata;	

		$data['paket'] = $this->M_General->get_paket_layanan_by_id($paket_layanan_id);

		$data['paket_layanan'] = $this->M_General->get_all_paket_layanan();

		$data['page'] 			= "paket_layanan";
		$data['judul'] 			= "Update Paket Layanan";
		$data['deskripsi'] 		= "";
		
		$this->template->views('master/tambah_paket_layanan', $data);
	
	}

	public function update_customer($customer_id) {

		$data['userdata'] 		= $this->userdata;	

		$data['customer'] = $this->M_General->get_customer_by_customer_id($customer_id);

		$data['customers'] = $this->M_General->get_all_customer();

		$data['page'] 			= "customer";
		$data['judul'] 			= "Update Customer";
		$data['deskripsi'] 		= "";
		
		$this->template->views('master/tambah_customer', $data);
	
	}

	public function update_user($user_id) {

		$data['userdata'] 		= $this->userdata;	

		$data['user'] = $this->M_General->get_user_by_id($user_id);

		$data['page'] 			= "users";
		$data['judul'] 			= "Update User";
		$data['deskripsi'] 		= "";
		
		$this->template->views('master/tambah_user', $data);
	
	}

	public function insert_paket_layanan() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$paket_layanan_id = $this->input->post('paket_layanan_id', TRUE);
		unset($data['paket_layanan_id']);

		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag'){

			if (!empty($flag)) { 
				if (!empty($paket_layanan_id)) { 
					
					$result = $this->M_General->update_paket_layanan($data, $paket_layanan_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Paket Layanan Berhasil Di Update'));
						$url = base_url() . 'master/paket_layanan';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Paket Layanan Gagal Di Update'));
						$url = base_url() . 'master/paket_layanan/';
						redirect($url);
						header("Refresh:0");
					}

				}else{

					$result = $this->M_General->insert_paket_layanan($data);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Paket Layanan Dimasukkan'));
						$url = base_url() . 'master/paket_layanan';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Paket Layanan Gagal Dimasukkan'));
						$url = base_url() . 'master/paket_layanan/';
						redirect($url);
						header("Refresh:0");
					}
				
				}
			}
		}
		
		$this->template->views('master/tambah_paket_layanan', $data);
	
	}

	public function insert_customer() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$customer_id = $this->input->post('customer_id', TRUE);
		unset($data['customer_id']);

		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag'){

			if (!empty($flag)) { 
				if (!empty($customer_id)) { 
					
					$result = $this->M_General->update_customer($data, $customer_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Customer Berhasil Di Update'));
						$url = base_url() . 'master/customer';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Customer Gagal Di Update'));
						$url = base_url() . 'master/customer/';
						redirect($url);
						header("Refresh:0");
					}

				}else{

					$result = $this->M_General->insert_customer($data);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Customer Dimasukkan'));
						$url = base_url() . 'master/customer';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Customer Gagal Dimasukkan'));
						$url = base_url() . 'master/customer/';
						redirect($url);
						header("Refresh:0");
					}
				
				}
			}
		}
		
		$this->template->views('master/list_customer', $data);
	
	}


	public function insert_user() {

		$data['userdata'] 		= $this->userdata;	
		
		$data = $this->input->post();	
		$data['status'] = (@$data['status']=='on' ? '1':'0');
		$user_id  = $data['user_id'];
		$operation  =	$data['operation'];
		unset($data['user_id']);
		unset($data['operation']);
		if($data['password']==''){
			unset($data['password']);
		}
		
		$result = $this->M_General->update_user($user_id, $data);

		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag'){

			if (!empty($flag)) { 
				if (!empty($customer_id)) { 
					
					$result = $this->M_General->update_customer($data, $customer_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Customer Berhasil Di Update'));
						$url = base_url() . 'master/customer';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Customer Gagal Di Update'));
						$url = base_url() . 'master/customer/';
						redirect($url);
						header("Refresh:0");
					}

				}else{

					$result = $this->M_General->insert_customer($data);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Customer Dimasukkan'));
						$url = base_url() . 'master/customer';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Customer Gagal Dimasukkan'));
						$url = base_url() . 'master/customer/';
						redirect($url);
						header("Refresh:0");
					}
				
				}
			}
		}
		
		$this->template->views('master/list_customer', $data);
	
	}

	public function delete_paket_layanan() {

		$id = $this->input->post('id');

		$result = $this->M_General->delete_paket_layanan($id);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Paket Layanan Berhasil Di Delete'));
			//redirect('master/paket_layanan');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Paket Layanan Gagal Di Delete'));
			//redirect('master/paket_layanan');
		}
	}

	public function delete_customer() {

		$id = $this->input->post('id');

		$result = $this->M_General->delete_customer($id);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Customer Berhasil Di Delete'));
			//redirect('master/paket_layanan');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Customer Gagal Di Delete'));
			//redirect('master/paket_layanan');
		}
	}

	public function list_data_therapist() {

		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];

		/*Menghitung total desa didalam database*/
		$total=$this->db->count_all_results("tkar");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();

		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("namakar",$search);
		}

		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('kodekar','DESC');
		$query=$this->db->get('tkar');

		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("namakar",$search);
		$jum=$this->db->get('tkar');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		$no=$start+1;
		foreach ($query->result_array() as $therapist) {
			$output['data'][]=array(
				$no,
				$therapist['kodetrp'],
				$therapist['namakar'],
				tgl_indo2($therapist['tglmasuk']),
				get_level_therapist($therapist['levelkomisi']),
				$therapist['no_rek'],
				$therapist['no_ktp'],
				$therapist['supplier_name'],
				$therapist['supplier_phone'],
				$therapist['supplier_phone'],
				'<div class="btn-group" style="display: -webkit-inline-box">
					<button class="btn btn-xs btn-primary view_therapist" kodekar=' . $therapist["kodekar"] . '><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View</button>
				</div>',
			);
		$no++;
		}
		
		echo json_encode($output);
	}

	public function list_data_users() {

		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];

		/*Menghitung total desa didalam database*/
		$total=$this->db->count_all_results("users");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();

		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
			$this->db->or_like("fullname",$search);
			$this->db->or_like("username",$search);
			$this->db->or_like("level",$search);
		}

		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_user','DESC');
		
		$query=$this->db->get('users');
		
		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("fullname",$search);
		$jum=$this->db->get('users');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		$no=$start+1;
		foreach ($query->result_array() as $users) {
			$output['data'][]=array(
				$no,
				$users['username'],
				$users['fullname'],
				get_level_user($users['level']),
				get_status_name($users['status']),
				get_branch_name($users['branch_id']),
				'<div class="btn-group" style="display: -webkit-inline-box">
				<button class="btn btn-xs btn-primary" id="view_users" title="Edit" id_user="' . $users["id_user"] . '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
				<button class="btn btn-xs btn-danger" id="delete_user" title="Delete"  Onclick="ConfirmDeleteUser()"  id_user="' . $users["id_user"] . '"><i class="fa fa-trash" aria-hidden="true"></i></button>
			  </div>',
			);
		$no++;
		}
		
		echo json_encode($output);
	}


	public function add_user() {

		$data = $this->input->post();	
		
		$data['status'] = ($data['status']=='on' ? '1':'0');
		$operation  =	$data['operation'];
		unset($data['operation']);
		$result = $this->M_General->insert_user($data);
	//	var_dump($result); die();
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('User Berhasil ditambah'));
			redirect('master/users');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('User Gagal ditambah'));
			redirect('master/users');
		}
	}

	public function delete_user() {

		$id_user = $this->input->post('id_user');

		$result = $this->M_General->delete_user($id_user);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('User Berhasil ditambah'));
			redirect('master/users');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('User Gagal ditambah'));
			redirect('master/users');
		}
	}

	public function change_password() {
		$this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
		$this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
		$this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');
		
		$UserID = $this->userdata->UserID;
		
		if ($this->form_validation->run() == TRUE) {
			if (($this->input->post('passLama')) == $this->userdata->Passwd) {
				if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
					$this->session->set_flashdata('msg', show_err_msg('Password Baru dan Konfirmasi Password harus sama'));
					redirect('profile');
				} else {
					$data = [
						'Passwd' => $this->input->post('passBaru')
					];
					
					$result = $this->M_admin->update($data, $UserID);
					
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Password Berhasil diubah'));
						redirect('profile');
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Password Gagal diubah'));
						redirect('profile');
					}
				}
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Password Salah'));
				redirect('profile');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('profile');
		}
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */