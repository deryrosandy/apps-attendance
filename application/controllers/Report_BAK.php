<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_Additional_Prod');
		$this->load->model('M_General');
		$this->load->model('M_Premium_Room');
		$this->load->model('M_Komisi_lulur');
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Report";
		$data['deskripsi'] = "Laporan Harian";
		$this->template->views('report/report_harian', $data);
	}
	
	public function rekap_gaji_therapist() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Rekap Gaji Therapist";
		$data['deskripsi'] = "";
		$this->template->views('report/rekap_gaji_therapist', $data);
	}	

	public function laporan_komisi_therapist() {
		$data['userdata'] = $this->userdata;
		$kodekar = $data['userdata']->kodekar;
		$namakar = $data['userdata']->fullname;
		$kodetrp = $data['userdata']->username;

		$data_tkar 	= $this->M_General->get_tkar_by_kodetrp($kodetrp);

		$branch_id = $data_tkar->branch_code;

		$dari_tanggal = (date('d') <= 25) ? date('Y-m-d', strtotime(date('Y-m-21') . '- 1 month')) : date('Y-m-21');
		$date_now = date('Y-m-d');
		$data['dari_tanggal'] = $dari_tanggal;
		$data['date_now'] = $date_now;
		$data['kodekar'] = $kodekar;
		$data['kodetrp'] = $kodetrp;

		$data['page'] = "report";
		//@$data['judul'] = "Laporan Komisi Therapist (" . tgl_indo2($dari_tanggal) . ' - ' . tgl_indo2($date_now) . ')';
		@$data['deskripsi'] = "";

		@$data['total_sesi'] 	= $this->M_General->getTotalSesiTherapist($kodekar);		
		
		//@$data['data_therapist'] =  $this->M_General->get_therapist_detail_by_kodekar($kodekar);
		@$data['data_therapist'] =  $this->M_General->get_therapist_detail_by_kodetrp($kodetrp);
		@$data['data_sesi'] =  $this->M_General->get_total_sesi_by_kodekar($kodekar, $dari_tanggal, $date_now, $branch_id);
		$levelkomisi 	= $this->M_General->get_level_komisi_by_kodekar($kodekar);	
	
		@$skema_komisi	 = $this->M_Komisi_lulur->get_skema_komisi_lulur($data['data_sesi']->total_point, $levelkomisi);	
		@$total_komisi_lulur = $data['data_sesi']->total_sesi * intVal($skema_komisi);	
		
		@$data['total_komisi_lulur'] 	= $total_komisi_lulur;

		@$komisi_add_prod	= $this->M_General->get_total_add_therapist($kodekar);

		@$komisi_room	= ($data['total_sesi']->total_pre + $data['total_sesi']->total_prs)*10000;
		//var_dump($komisi_room); 
		@$data['komisi_fnb'] = $this->M_General->get_total_komisi_fnb($dari_tanggal, $date_now, $namakar)*0.05;
		@$data['komisi_kuesioner'] = $this->M_General->get_komisi_kuesioner_by_kodekar($dari_tanggal, $date_now, $kodekar)->jumlah_komisi;
		@$data['komisi_lain'] = $this->M_General->get_komisi_lain_by_kodekar($dari_tanggal, $date_now, $kodekar);
		@$data['total_komisi_additional'] 	=  $komisi_room+$komisi_add_prod;

		@$data['total_penghasilan'] =  $total_komisi_lulur+$data['total_komisi_additional']+$data['komisi_fnb']+$data['komisi_kuesioner']+$data['komisi_lain'];
		@$potongan  = $this->M_General->get_potongan_kar_by_kodekar($kodekar, $dari_tanggal, $date_now, $branch_id);
		//var_dump($potongan); die();
		if(@$potongan != null){
			$data['potongan']  = $potongan;
			$data['total_potongan'] = $potongan->yayasan+$potongan->asuransi+$potongan->koperasi+$potongan->mess+$potongan->tabungan+$potongan->trt_glx+$potongan->zion+$potongan->denda+$potongan->bpjs_kry+$potongan->bpjs_prs+$potongan->hutang+$potongan->tiket+$potongan->sertifikasi;
		}else{
			$data['total_potongan'] = 0;
		}
			//$tabungan = $this->M_General->get_tabungan_by_kodekar($kodekar, $dari_tanggal, $date_now); 
		//var_dump($tabungan); die();
		$sisa_penghasilan = $data['total_penghasilan']-$data['total_potongan'];
		if($sisa_penghasilan >= 1500000 && $levelkomisi =='SR'){
			$data['tabungan'] = 0.3*$data['total_penghasilan'];
		}else{
			$data['tabungan'] = 0;
		}

		$data['take_homepay'] = $sisa_penghasilan-$data['tabungan'];

		$this->template->views('report/laporan_komisi_therapist', $data);
	}	


	public function additional_product() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Additional";
		$data['deskripsi'] = "";
		$this->template->views('report/additional_product', $data);
	}	

	public function premium_room() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Premium Room";
		$data['deskripsi'] = "";
		$this->template->views('report/premium_room', $data);
	}

	public function komisi_lulur() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Komisi Lulur";
		$data['deskripsi'] = "";
		$this->template->views('report/komisi_lulur', $data);
	}

	public function komisi_additional() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Komisi Additional";
		$data['deskripsi'] = "";
		$this->template->views('report/komisi_additional', $data);
	}

	public function gaji_therapist() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Gaji Therapist";
		$data['deskripsi'] = "";
		$data['branch_id'] = $this->userdata->branch_id;

		$this->template->views('report/laporan_gaji_therapist', $data);
	}

	public function penjualan_gro() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Komisi Penjualan GRO";
		$data['deskripsi'] = "";
		$this->template->views('report/penjualan_gro', $data);
	}


	public function rekap_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Transaksi";
		$data['deskripsi'] = "Rekap Penjualan";
		
		$this->template->views('report/rekap_penjualan', $data);
	}
	
	public function ajax_rekap_penjualan(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
				
        $data_rekap = $this->M_kasir->get_rekap_penjualan($dari_tanggal, $sampai_tanggal);
		
		$data = array();
		
        $no = $_POST['start'];
        foreach ($data_rekap as $rekap) {
			$no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($rekap->Tanggal);
            $row[] = $rekap->Nomor;
            $row[] = $rekap->NoMeja;
            $row[] = $rekap->NoPesan;
            $row[] = getHour($rekap->Jam);
            $row[] = format_digit($rekap->Total);
            $data[] = $row;
        }
		
        $output = array(
					"data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	public function ranking_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Transaksi";
		$data['deskripsi'] = "Ranking Penjualan";
		$this->template->views('report/ranking_penjualan', $data);
	}
	
	public function ajax_ranking_penjualan() {
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');		
		
        $list = $this->M_kasir->get_ranking_penjualan($dari_tanggal, $sampai_tanggal);
		//var_dump($list); die();
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data){
			$menu = $this->M_kasir->get_menu_name($list_data->Kode);
			$total = $list_data->Total + $list_data->PPN + $list_data->Service; 
            $no++;
            $row = array();
            $row[] = $no;
            //$row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $menu->Nama;
			$row[] = $list_data->Qty;
			//$row[] = format_decimal(round($total, 2));
			$row[] = (round($total, 2));
            $data[] = $row;
        }

		//var_dump($_POST); die();
		
        $output = array(
					"draw" => $_POST['draw'],
					"data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	public function laba_rugi() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Transaksi";
		$data['deskripsi'] = "Laba/Rugi";
		$this->template->views('report/laba_rugi', $data);
	}

	public function summary_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Transaksi";
		$data['deskripsi'] = "Summary";
		$this->template->views('report/summary_penjualan', $data);
	}

	public function rekap_nominal() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Transaksi";
		$data['deskripsi'] = "Rekap Nominal";
		$this->template->views('report/rekap_nominal', $data);
	}

	public function total_sesi_therapist(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		$branch_id 		= $this->input->post('branch_id');

		//$list = $this->M_General->get_detail_komisi_lulur_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal);
		$list = $this->M_General->get_detail_komisi_lulur($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
		$data = array();
		
        $no = 0;
        foreach ($list as $data_sesi){	
			//var_dump($data_sesi);		
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = tgl_indo2($data_sesi->tgl);
			$row[] = $data_sesi->nokamar;
			$row[] = $data_sesi->kodeservice;
			$row[] = $data_sesi->branch_name;
			$row[] = $data_sesi->jmlsesi;
			$row[] = $data_sesi->jmlext;
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        
		//output to json format
        echo json_encode($output);
	}

	public function detail_additional_therapist(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		
		//$list = $this->M_General->get_detail_komisi_additional_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal);
		$list = $this->M_Additional_Prod->get_additional_product_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar);
		/*
		$data = array();
		
        $no = 0;
        foreach ($list as $data_add){
			var_dump($data_add); die();
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = tgl_indo2($data_add->tanggal);
			$row[] = $data_add->namaservice;
			$row[] = $data_add->branch_name;
			$row[] = $data_add->jml;
			$row[] = $data_add->komisi_trp;
			$total_komisi_add = $data_add->komisi_trp*$data_add->jml;
			$row[] = $total_komisi_add;
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        */
		//output to json format
        echo json_encode($list);
	}
	

	public function data_penjualan_gro(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$list = $this->M_General->get_gro_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		//$list = $this->M_Additional_Prod->get_additional_product_by_kodekar($dari_tanggal, $sampai_tanggal, $branch_id);
		$data = array();
		
        $no = 0;
        foreach ($list as $gro){			
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = $gro->kodegro;
			$row[] = $gro->namagro;
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        //var_dump($output); die();
		//output to json format
		
        echo json_encode($list);
	}

	public function data_penjualan_gro_detail(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		$kodegro 		= $this->input->post('kodegro');
		
		//var_dump($dari_tanggal, $sampai_tanggal,  $branch_id, $kodegro);die();
		
		$list = $this->M_General->get_penjualan_gro_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);
		
		$data = array();
		
        $no = 0;
        foreach ($list as $gro){			
            $no++;
            $row = array();
			$row[] = $gro->kodeservice;
			$row[] = $gro->namaservice;
			$data[] = $row;
        }
		
        $output = array(
					"data" => $data,
                );
        //var_dump($output); die();
		//output to json format
        echo json_encode($list);
	}

	public function data_additional_product(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		//$tkars = $this->M_General->get_kodekar_by_branch($branch_id);
		
		$list = $this->M_General->get_therapist_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		//$list = $this->M_Additional_Prod->get_additional_product_by_kodekar($dari_tanggal, $sampai_tanggal, $branch_id);
		$data = array();
		
        $no = 0;
        foreach ($list as $tkar){			
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = $tkar->kodekar;
			$row[] = $tkar->kodetrp;
			$row[] = $tkar->namakar;
			$row[] = tgl_indo2($tkar->tglmasuk);
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        
		//output to json format
        echo json_encode($output);
	}
	
	public function data_rekap_gaji_therapist(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$list = $this->M_General->get_therapist_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		$data = array();
		
        $no = 0;
        foreach ($list as $tkar){			
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = $tkar->kodetrp;
			$row[] = $tkar->kodekar;
			$row[] = $tkar->namakar;
			$row[] = get_level_therapist($tkar->levelkomisi);
			$row[] = tgl_indo2($tkar->tglmasuk);
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}

	public function data_therapist_by_branch(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$list = $this->M_General->get_therapist_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		$data = array();
		
        $no = 0;
        foreach ($list as $tkar){			
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = $tkar->kodetrp;
			$row[] = $tkar->namakar;
			$row[] = get_level_therapist($tkar->levelkomisi);
			$row[] = tgl_indo2($tkar->tglmasuk);
			$row[] = $tkar->supplier_name;
			$row[] = $tkar->supplier_phone;
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}

	
	
	public function data_komisi_lulur(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$komisi = $this->M_Komisi_lulur->get_total_session_by_filter($dari_tanggal, $sampai_tanggal, $branch_id);
		
		$data = array();
		
        $no = 0;
        foreach ($komisi as $kom){	
			$no++;
            $row = array();
			$row[0] = $no;
			$row[1] = $kom->kodetrp;
			$row[2] = $kom->namakar;
			$row[3] = $kom->total_suite;
			$row[4] = $kom->total_pre;
			$row[5] = $kom->total_prs;
			$row[6] = $kom->total_std;

			$levelkomisi 	= $this->M_General->get_level_komisi_by_kodekar($kom->kodekar, $kom->kodetrp);
			
			$row[8] = $kom->total_sesi_massage;
			$row[9] = $kom->total_sesi_lulur;
			$row[10] = $kom->total_point;
			
			$skema_komisi_lulur	 = $this->M_Komisi_lulur->get_skema_komisi_lulur($kom->total_point, $levelkomisi);
			$skema_komisi_massage = $this->M_Komisi_lulur->get_skema_komisi_lulur($kom->total_point, $levelkomisi);		

			$row[7] = get_level_therapist($levelkomisi);
			//var_dump($skema_komisi, $kom->total_sesi); die();	
			$total_komisi_massage = ($kom->total_sesi_massage * $skema_komisi_massage);
			$total_komisi_lulur = ($kom->total_sesi_lulur * $skema_komisi_lulur);
			$total_komisi = $total_komisi_massage+$total_komisi_lulur;
			$row[11] = ($total_komisi_massage != '0' ? format_digit($total_komisi_massage) : '');
			$row[12] = format_digit($total_komisi_lulur);
			$row[13] = format_digit($total_komisi);
			//var_dump($skema_komisi); die();
			
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        
		//output to json format
        echo json_encode($output);
	}
	
	public function data_guest_comment(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$grade = $this->M_General->get_grade_therapist_by_filter($dari_tanggal, $sampai_tanggal, $branch_id);
		
		$data = array();
		
        $no = 0;
        foreach ($grade as $grd){	
			$no++;
            $row = array();
			$row[0] = $no;
			$row[1] = get_kodetrp($grd->kodekar);
			$row[2] = $grd->namakar;
			$row[3] = $grd->total_tamu;
			$row[4] = $grd->total_ts;
			$row[5] = $grd->ts_persen;
			$row[6] = $grd->ts_nilai;
			$row[7] = $grd->point;
			$row[8] = $grd->ratio_nilai;
			$row[9] = $grd->gc_comment;
			$row[10] = $grd->gc_comment_persen;
			$row[11] = $grd->gc_comment_nilai;
			$row[12] = $grd->hasil;
			$row[13] = $grd->grade;	
			$row[14] = $grd->gc_performance;
			$row[15] = $grd->gc_massage;
			$row[16] = $grd->gc_service;
			$row[17] = $grd->jml_gc;
			$row[18] = $grd->branch_code;
			
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        
		//output to json format
        echo json_encode($output);
	}
	
	public function print_guest_comment(){
		//membuat objek PHPExcel
		$objPHPExcel = new PHPExcel();

		$dari_tanggal 	= $this->input->get('dari_tanggal');
		$sampai_tanggal = $this->input->get('sampai_tanggal');
		$branch_id 		= $this->input->get('branch_id');
		
		$grade = $this->M_General->get_grade_therapist_by_filter($dari_tanggal, $sampai_tanggal, $branch_id);

		//set Sheet yang akan diolah 
		$objPHPExcel->setActiveSheetIndex(0)
			//mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
			//Hello merupakan isinya
				->setCellValue('A1', 'No')
					->setCellValue('B1', 'NIT')
					->setCellValue('C1', 'NAMA')
					->setCellValue('D1', 'T. Tamu')
					->mergeCells('A1:A2')
					->mergeCells('B1:B2')
					->mergeCells('C1:C2')
					->mergeCells('D1:D2')
					->setCellValue('E1', 'PERHITUNGAN G. REQUEST')
					->setCellValue('E2', 'TS')
					->setCellValue('F2', '%')
					->setCellValue('G2', 'NILAI')
					->setCellValue('H1', 'PERHITUNGAN RATIO')
					->setCellValue('H2', 'T. POINT')
					->setCellValue('I2', '%')
					->setCellValue('J2', 'NILAI')
					->setCellValue('K1', 'PERHITUNGAN G. COMMENT')
					->setCellValue('K2', 'G. COMMENT')
					->setCellValue('L2', '%')
					->setCellValue('M2', 'NILAI')
					->setCellValue('N1', 'HASIL')
					->setCellValue('O1', 'GRADE')
					->mergeCells('E1:G1')
					->mergeCells('H1:J1')
					->mergeCells('K1:M1')
					->mergeCells('N1:N2')
					->mergeCells('O1:O2');

			$rowNumber = 3;
			$no = 1;

			foreach($grade as $grd) { 
	
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowNumber,$no); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowNumber,get_kodetrp($grd->kodekar)); 

				$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowNumber,$grd->namakar)
											->getDefaultStyle()->getAlignment()
											->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
											->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

				$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowNumber,$grd->total_tamu); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowNumber,$grd->total_ts); 
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowNumber,$grd->ts_persen); 
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowNumber,$grd->ts_nilai); 
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowNumber,$grd->point); 
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$rowNumber,round($grd->point/$grd->total_tamu*100)); 
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$rowNumber,$grd->ratio_nilai); 
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$rowNumber,$grd->gc_comment); 
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$rowNumber,$grd->gc_comment_persen); 
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$rowNumber,$grd->gc_comment_nilai); 
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$rowNumber,$grd->hasil); 
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$rowNumber,$grd->grade); 

				$no++;
				$rowNumber++;

			} 
					
            //set title pada sheet (me rename nama sheet)
			$objPHPExcel->getActiveSheet()->setTitle('Grade Therapist');

			$style = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				),
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_MEDIUM
					)
				),
				
			);
		
			$objPHPExcel->getDefaultStyle()->applyFromArray($style);

            //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
 
            //sesuaikan headernya 
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //ubah nama file saat diunduh
            header('Content-Disposition: attachment;filename="GradeTherapist_' . get_branch_name($branch_id) . '_' . str_replace("-", "", $dari_tanggal) . '.xlsx"');
            //unduh file
            $objWriter->save("php://output");
 
            //Mulai dari create object PHPExcel itu ada dokumentasi lengkapnya di PHPExcel, 
            // Folder Documentation dan Example
            // untuk belajar lebih jauh mengenai PHPExcel silakan buka disitu
	}
	
	public function print_excel_rekapan_gaji_all(){
		//membuat objek PHPExcel
		$objPHPExcel = new PHPExcel();

		$dari_tanggal 	= $this->input->get('dari_tanggal');
		$sampai_tanggal = $this->input->get('sampai_tanggal');
		$branch_id 		= $this->input->get('branch_id');
		
		$grade = $this->M_General->get_grade_therapist_by_filter($dari_tanggal, $sampai_tanggal, $branch_id);

		//set Sheet yang akan diolah 
		$objPHPExcel->setActiveSheetIndex(0)
			//mengisikan value pada tiap-tiap cell, A1 itu alamat cellnya 
			//Hello merupakan isinya
				->setCellValue('A1', 'No')
					->setCellValue('B1', 'NIT')
					->setCellValue('C1', 'NAMA')
					->setCellValue('D1', 'T. Tamu')
					->mergeCells('A1:A2')
					->mergeCells('B1:B2')
					->mergeCells('C1:C2')
					->mergeCells('D1:D2')
					->setCellValue('E1', 'PERHITUNGAN G. REQUEST')
					->setCellValue('E2', 'TS')
					->setCellValue('F2', '%')
					->setCellValue('G2', 'NILAI')
					->setCellValue('H1', 'PERHITUNGAN RATIO')
					->setCellValue('H2', 'T. POINT')
					->setCellValue('I2', '%')
					->setCellValue('J2', 'NILAI')
					->setCellValue('K1', 'PERHITUNGAN G. COMMENT')
					->setCellValue('K2', 'G. COMMENT')
					->setCellValue('L2', '%')
					->setCellValue('M2', 'NILAI')
					->setCellValue('N1', 'HASIL')
					->setCellValue('O1', 'GRADE')
					->mergeCells('E1:G1')
					->mergeCells('H1:J1')
					->mergeCells('K1:M1')
					->mergeCells('N1:N2')
					->mergeCells('O1:O2');

			$rowNumber = 3;
			$no = 1;

			foreach($grade as $grd) { 
	
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowNumber,$no); 
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowNumber,get_kodetrp($grd->kodekar)); 

				$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowNumber,$grd->namakar)
											->getDefaultStyle()->getAlignment()
											->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
											->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

				$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowNumber,$grd->total_tamu); 
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowNumber,$grd->total_ts); 
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowNumber,$grd->ts_persen); 
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowNumber,$grd->ts_nilai); 
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowNumber,$grd->point); 
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$rowNumber,round($grd->point/$grd->total_tamu*100)); 
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$rowNumber,$grd->ratio_nilai); 
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$rowNumber,$grd->gc_comment); 
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$rowNumber,$grd->gc_comment_persen); 
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$rowNumber,$grd->gc_comment_nilai); 
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$rowNumber,$grd->hasil); 
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$rowNumber,$grd->grade); 

				$no++;
				$rowNumber++;

			} 
					
            //set title pada sheet (me rename nama sheet)
			$objPHPExcel->getActiveSheet()->setTitle('Grade Therapist');

			
			$style = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				),
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				),
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '77a1e5')
				)
			);
		
			$objPHPExcel->getDefaultStyle()->applyFromArray($style);
			
            //mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
 
            //sesuaikan headernya 
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //ubah nama file saat diunduh
            header('Content-Disposition: attachment;filename="GradeTherapist_' . get_branch_name($branch_id) . '_' . str_replace("-", "", $dari_tanggal) . '.xlsx"');
            //unduh file
            $objWriter->save("php://output");
 
            //Mulai dari create object PHPExcel itu ada dokumentasi lengkapnya di PHPExcel, 
            // Folder Documentation dan Example
            // untuk belajar lebih jauh mengenai PHPExcel silakan buka disitu
    }

	public function data_potongan_therapist(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$potongan = $this->M_General->get_potongan_therapist($dari_tanggal, $sampai_tanggal, $branch_id);
		$potongan_spv = $this->M_General->get_potongan_spv_therapist($dari_tanggal, $sampai_tanggal, $branch_id);
		
		$data = array();

		$no = 0;
	
        foreach ($potongan as $potong){	
			$no++;
            $row = array();
			$row[0] = $no;
			$row[1] = $potong->id_potongan;
			$row[2] = $potong->namakar;
			$row[3] = ($potong->yayasan);
			$row[4] = ($potong->asuransi);
			$row[5] = ($potong->bpjs_kry);
			$row[6] = ($potong->bpjs_prs);
			$row[7] = ($potong->tiket);
			$row[8] = ($potong->sertifikasi);
			$row[9] = ($potong->koperasi);	
			$row[10] = ($potong->mess);	
			$row[11] = ($potong->hutang);
			$row[12] = ($potong->denda);
			$row[13] = ($potong->trt_glx);
			$row[14] = ($potong->zion);
			$row[15] = ($potong->lain_lain);
			$total_potongan = $potong->yayasan+$potong->asuransi+$potong->koperasi+$potong->mess+$potong->denda+$potong->zion+$potong->hutang+$potong->lain_lain+$potong->tiket+$potong->sertifikasi+$potong->trt_glx+$potong->bpjs_kry+$potong->bpjs_prs;
			$row[16] = format_digit($total_potongan);
			$row[17] = '<div class="btn-group" style="display: -webkit-inline-box">
							<button class="btn btn-xs btn-primary btn-edit"  data-id=' . $potong->id_potongan . ' kodekar=' . $potong->kodekar . '>Edit</button>
					</div>';
			
			$data[] = $row;
		}
		
		foreach ($potongan_spv as $potong_spv){	
			$no++;
            $row = array();
			$row[0] = $no;
			$row[1] = get_level_spv($potong_spv->id_spv);
			$row[2] = $potong_spv->nama_spv;
			$row[3] = format_digit($potong->yayasan);
			$row[4] = format_digit($potong->asuransi);
			$row[5] = format_digit($potong->koperasi);	
			$row[6] = format_digit($potong->mess);	
			$row[7] = format_digit($potong->hutang);
			$row[8] = format_digit($potong->denda);
			$total_potongan = $potong->yayasan+$potong->asuransi+$potong->koperasi+$potong->mess+$potong->denda+$potong->zion+$potong->hutang+$potong->lain_lain+$potong->tiket+$potong->sertifikasi+$potong->trt_glx+$potong->bpjs_kry+$potong->bpjs_prs;
			$row[9] = format_digit($total_potongan);
			$row[10] = '<div class="btn-group" style="display: -webkit-inline-box">
							<button class="btn btn-xs btn-primary btn-edit"  data-id=' . $potong->id_potongan . ' kodekar=' . $potong->kodekar . '>Edit</button>
					</div>';
			
			$data[] = $row;
        }
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}
	
	public function hitung_tabungan_therapist(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$list_therapist = $this->M_General->get_therapist_tabungan_count_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		//var_dump($list_therapist); die();
		if($list_therapist > 0){
			$list_therapist = $this->M_General->get_therapist_tabungan_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		}else{
			$list_therapist = $this->M_General->get_therapist_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		}
		
		//var_dump($list_therapist); die();

		$data = array();
		
        $no = 0;
        foreach ($list_therapist as $tkar){		
			
			$kodekar = $tkar->kodekar;
			
			//Komisi Lulur
			$data_lulur = $this->M_General->get_total_session_kar_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal);
			
			$levelkomisi 	= $this->M_General->get_level_komisi_by_kodekar($data_lulur->kodekar);
				
			$skema_komisi	 = $this->M_Komisi_lulur->get_skema_komisi_lulur($data_lulur->total_point, $levelkomisi);	
			
			$total_komisi_lulur = ($data_lulur->total_sesi * intVal($skema_komisi));
			
			//Komisi Add
			$komisi_add = $this->M_General->get_total_additional_kar_by_kodekar($tkar->kodekar, $dari_tanggal, $sampai_tanggal)->total_komisi_additional;					
			$total_komisi_premium = (intval($data_lulur->total_pre)+intval($data_lulur->total_prs))*10000;
			$total_komisi_add = intval($komisi_add)+$total_komisi_premium;

			//Komisi FNB
			$total_komisi_fnb = $this->M_General->get_total_komisi_fnb($dari_tanggal, $sampai_tanggal, $kodekar)*0.05;	
			//Komisi Lain
			$total_komisi_lain = $this->M_General->get_komisi_lain_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
			
			$total_penghasilan = $total_komisi_lulur+intval($total_komisi_add)+$total_komisi_fnb+intval($total_komisi_lain);
			//var_dump($total_komisi_lain); die();

			$pot = $this->M_General->get_potongan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
			
			@$total_potongan = $pot->yayasan+$pot->asuransi+$pot->koperasi+$pot->mess+$pot->zion+$pot->denda+$pot->hutang+$pot->tiket+$pot->trt_glx+$pot->sertifikasi+$pot->bpjs_kry+$pot->bpjs_prs;
			//var_dump($total_komisi_add);

			$total_sisa_penghasilan = intval($total_penghasilan)-$total_potongan;

			if($total_sisa_penghasilan >= 1500000 && get_level_therapist($levelkomisi) =='SR'){
				$tabungan = 0.3*intval($total_penghasilan);
			}else{
				$tabungan = 0;
			}
			
			$save_tabungan = $this->M_General->save_tabungan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $tabungan, $branch_id);

			$no++;
            $row = array();
			$row[0] = $no;
			$row[1] = $tkar->kodetrp;
			$row[2] = $tkar->namakar;
			$row[3] = get_level_therapist($levelkomisi);
			$row[4] = format_digit($tabungan);
			
			$data[] = $row;
        }
		
        $output = array(
					"data" => $data,
                );
        echo json_encode($output);
	}
	
	public function reset_tabungan_therapist(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$list_therapist = $this->M_General->get_therapist_tabungan_count_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		//var_dump($list_therapist); die();
		if($list_therapist > 0){
			$list_therapist = $this->M_General->get_therapist_tabungan_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		}else{
			$list_therapist = $this->M_General->get_therapist_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		}

		$data = array();
		
        $no = 0;
        foreach ($list_therapist as $tkar){		
			
			$kodekar = $tkar->kodekar;
			$levelkomisi 	= $this->M_General->get_level_komisi_by_kodekar($kodekar);
			
			$reset_tabungan = $this->M_General->reset_tabungan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
			$tabungan = $this->M_General->get_tabungan_therapist_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
			
			$no++;
            $row = array();
			$row[0] = $no;
			$row[1] = $tkar->kodetrp;
			$row[2] = $tkar->namakar;
			$row[3] = get_level_therapist($levelkomisi);
			$row[4] = format_digit($tabungan);
			
			$data[] = $row;
        }
		
        $output = array(
					"data" => $data,
                );
        echo json_encode($output);
	}
	

	public function data_tabungan_therapist(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		/*
		$list_therapist = $this->M_General->get_therapist_tabungan_count_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		
		if($list_therapist > 0){
			$list_therapist = $this->M_General->get_therapist_tabungan_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		}else{
			$list_therapist = $this->M_General->get_therapist_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		}
		*/
		
		$list_therapist = $this->M_General->get_therapist_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		//var_dump($list_therapist); die();

		$data = array();
		
        $no = 0;
        foreach ($list_therapist as $tkar){		
			
			$kodekar = $tkar->kodekar;

			$tabungan = $this->M_General->get_tabungan_therapist_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
			$levelkomisi 	= $this->M_General->get_level_komisi_by_kodekar($kodekar);

			$no++;
            $row = array();
			$row[0] = $no;
			$row[1] = $tkar->kodetrp;
			$row[2] = $tkar->namakar;
			$row[3] = get_level_therapist($levelkomisi);
			$row[4] = format_digit($tabungan);
			
			$data[] = $row;
        }
		
        $output = array(
					"data" => $data,
                );
        echo json_encode($output);
	}
	
	/* KOMISI FNB */
	public function data_komisi_fnb(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$komisi = $this->M_General->get_komisi_fnb_therapist($dari_tanggal, $sampai_tanggal, $branch_id);
		//var_dump($komisi); die();
		$data = array();
		if($komisi > 0){
			$no = 0;
			foreach ($komisi as $kom){		
				$no++;
				$row = array();
				$row[0] = $no;
				$row[1] = $kom->kodetrp;
				$row[2] = $kom->namakar;
				
				$total_komisi = $this->M_General->get_total_komisi_fnb($dari_tanggal, $sampai_tanggal, $kom->kodekar)*0.05;
				$update_komisi = $this->M_General->update_komisi_fnb_by_id($kom->id_fnb,$total_komisi);

				$row[3] = format_digit($total_komisi);
				$row[4] = '<div class="btn-group" style="display: -webkit-inline-box">
								<button class="btn btn-xs btn-primary btn-view"  data-id=' . $kom->id_fnb . ' dari_tanggal=' . $dari_tanggal . ' sampai_tanggal=' . $sampai_tanggal .  ' kodekar=' . $kom->kodekar . ' namakar=' . $kom->namakar . ' branch_id=' . $branch_id . '><i class="fa fa-search"></i></button>
						</div>';
				
				$data[] = $row;
			}
		}
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}

	public function data_komisi_lain(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$komisi = $this->M_General->get_komisi_lain($dari_tanggal, $sampai_tanggal, $branch_id);
		//var_dump($komisi); die();
		$data = array();
		if($komisi > 0){
			$no = 0;
			foreach ($komisi as $kom){			
				$no++;
				$row = array();
				$row[0] = $no;
				$row[1] = $kom->kodetrp;
				$row[2] = $kom->namakar;
				$row[3] = format_digit($kom->jumlah_komisi);
				$row[4] = '<div class="btn-group" style="display: -webkit-inline-box">
								<button class="btn btn-xs btn-primary btn-edit"  data-id=' . $kom->id_lain . ' kodekar=' . $kom->kodekar . '>Edit</button>
						</div>';
				
				$data[] = $row;
			}
		}
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}

	public function data_komisi_kodes(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$komisi = $this->M_General->get_komisi_lain($dari_tanggal, $sampai_tanggal, $branch_id);
		//var_dump($komisi); die();
		$data = array();
		if($komisi > 0){
			$no = 0;
			foreach ($komisi as $kom){			
				$no++;
				$row = array();
				$row[0] = $no;
				$row[1] = $kom->kodetrp;
				$row[2] = $kom->namakar;
				$row[3] = format_digit($kom->komisi_kodes);
				$row[4] = '<div class="btn-group" style="display: -webkit-inline-box">
								<button class="btn btn-xs btn-primary btn-edit"  data-id=' . $kom->id_lain . ' kodekar=' . $kom->kodekar . '>Edit</button>
						</div>';
				
				$data[] = $row;
			}
		}
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}
	
	public function data_komisi_kuesioner(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$branch_id 		= $this->input->post('branch_id');
		
		$komisi = $this->M_General->get_komisi_kuesioner($dari_tanggal, $sampai_tanggal, $branch_id);
		//var_dump($komisi); die();
		$data = array();
		if($komisi > 0){
			$no = 0;
			foreach ($komisi as $kom){			
				$no++;
				$row = array();
				$row[0] = $no;
				$row[1] = $kom->kodetrp;
				$row[2] = $kom->namakar;
				$row[3] = format_digit($kom->jumlah_komisi);
				$row[4] = '<div class="btn-group" style="display: -webkit-inline-box">
								<button class="btn btn-xs btn-primary btn-edit"  data-id=' . $kom->id_kuesioner . ' kodekar=' . $kom->kodekar . '>Edit</button>
						</div>';
				
				$data[] = $row;
			}
		}
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}	
	
	public function data_edit_komisi_fnb(){
		$id_fnb 	= $this->input->post('id_fnb');
		$kodekar 	= $this->input->post('kodekar');
		
		$data = $this->M_General->get_komisi_fnb_by_id($id_fnb, $kodekar);
		
        $output = array(
				"data" => $data,
				);
				
        echo json_encode($output);
	}

	public function data_view_kar_komisi_fnb(){
		$id_fnb 			= $this->input->post('id_fnb');
		$dari_tanggal 		= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');
		$kodekar 			= $this->input->post('kodekar');
		
		$data = $this->M_General->get_view_kar_komisi_fnb($dari_tanggal, $sampai_tanggal, $kodekar);
		//var_dump($data); die();
		
        echo json_encode($data);
	}

	public function data_view_komisi_fnb(){
		$id_fnb 			= $this->input->post('id_fnb');
		$dari_tanggal 		= $this->input->post('dari_tanggal');
		$sampai_tanggal 	= $this->input->post('sampai_tanggal');
		$kodekar 			= $this->input->post('kodekar');
		$branch_id 			= $this->input->post('branch_id');

		$data = $this->M_General->get_view_komisi_fnb($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
		
        echo json_encode($data);
	}

	public function update_komisi_fnb(){
		$id_fnb 	= $this->input->post('id_fnb');
		$jumlah_komisi 	= $this->input->post('jumlah_komisi');
		
		$data = $this->M_General->update_komisi_fnb_by_id($id_fnb, $jumlah_komisi);
	}
	
	/* End Komisi FNB */

	public function data_edit_komisi_lain(){
		$id_lain 	= $this->input->post('id_lain');
		$kodekar 	= $this->input->post('kodekar');
		
		$data = $this->M_General->get_komisi_lain_by_id($id_lain, $kodekar);
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}

	public function data_edit_komisi_kodes(){
		$id_lain 	= $this->input->post('id_lain');
		$kodekar 	= $this->input->post('kodekar');
		
		$data = $this->M_General->get_komisi_lain_by_id($id_lain, $kodekar);
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}

	public function update_komisi_lain(){
		$id_lain 	= $this->input->post('id_lain');
		$jumlah_komisi 	= $this->input->post('jumlah_komisi');
		
		$data = $this->M_General->update_komisi_lain_by_id($id_lain, $jumlah_komisi);
	}

	public function update_komisi_kodes(){
		$id_lain 	= $this->input->post('id_lain');
		$jumlah_komisi 	= $this->input->post('jumlah_komisi');
		
		$data = $this->M_General->update_komisi_kodes_by_id($id_lain, $jumlah_komisi);
	}

	public function data_edit_komisi_kuesioner(){
		$id_kuesioner 	= $this->input->post('id_kuesioner');
		$kodekar 	= $this->input->post('kodekar');
		
		$data = $this->M_General->get_komisi_kuesioner_by_id($id_kuesioner, $kodekar);
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}

	public function update_komisi_kuesioner(){
		$id_kuesioner 	= $this->input->post('id_kuesioner');
		$jumlah_komisi 	= $this->input->post('jumlah_komisi');
		
		$data = $this->M_General->update_komisi_kuesioner_by_id($id_kuesioner, $jumlah_komisi);
	}

	/* Potongan Therapist */
		
	public function data_edit_potongan_therapist(){
		$id_potongan 	= $this->input->post('id_potongan');
		$kodekar 	= $this->input->post('kodekar');
		
		$data = $this->M_General->get_potongan_therapist_by_id($id_potongan, $kodekar);
		
        $output = array(
				"data" => $data,
                );
        echo json_encode($output);
	}

	public function update_potongan_by_column(){

		$column 	= $this->input->post('column');
		$editval 	= strip_tags($this->input->post('editval'));
		$id_potongan 	= $this->input->post('id_potongan');

		$data = $this->M_General->update_potongan_by_column($column, "'" . $editval . "'", $id_potongan);
	}

	public function update_potongan_therapist(){
		$id_potongan = $this->input->post('id_potongan');
		
		$data = array(			
			'yayasan'		=> str_replace(' ', '', $this->input->post('yayasan')),
			'asuransi' 		=> str_replace(' ', '', $this->input->post('asuransi')),
			'koperasi' 		=> str_replace(' ', '', $this->input->post('koperasi')),
			'mess' 			=> str_replace(' ', '', $this->input->post('mess')),
			'denda' 		=> str_replace(' ', '', $this->input->post('denda')),
			'hutang' 		=> str_replace(' ', '', $this->input->post('hutang')),
			'zion' 			=> str_replace(' ', '', $this->input->post('zion')),
			'lain_lain' 	=> str_replace(' ', '', $this->input->post('lain_lain')),
			'tiket' 		=> str_replace(' ', '', $this->input->post('tiket')),
			'sertifikasi' 	=> str_replace(' ', '', $this->input->post('sertifikasi')),
			'trt_glx' 		=> str_replace(' ', '', $this->input->post('trt_glx')),
			'bpjs_kry' 		=> str_replace(' ', '', $this->input->post('bpjs_kry')),
			'bpjs_prs' 		=> str_replace(' ', '', $this->input->post('bpjs_prs')),
		);
		
		$data = $this->M_General->update_potongan_therapist_by_id($id_potongan, $data);
	}
	
	/* End Potongan therapist */

	public function data_additional_product_detail(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		$branch_id 		= $this->input->post('branch_id');

		//var_dump($branch_id); die();
				
		//$branch = $this->M_General->get_branch_by_branch_id($branch_id);
		$list = $this->M_Additional_Prod->get_additional_product_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
		
		echo json_encode($list);
    }

	public function data_additional_product_detail_kar(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');
		$branch_id 		= $this->input->post('branch_id');
		//var_dump($branch_id); die();
				
		//$branch = $this->M_General->get_branch_by_branch_id($branch_id);
		$list = $this->M_Additional_Prod->get_additional_product_kar_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
		
		echo json_encode($list);
    }

	public function data_komisi_fnb_by_kodekar(){
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		$kodekar 		= $this->input->post('kodekar');

		//var_dump($branch_id); die();
				
		//$branch = $this->M_General->get_branch_by_branch_id($branch_id);
		$list = $this->M_Additional_Prod->get_additional_product_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
		//var_dump($list); die();
        echo json_encode($list);
    }

	public function ajax_detail_penjualan(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_detail_penjualan($dari_tanggal, $sampai_tanggal);
		//var_dump($list); die();
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
			//print_r($list_data);
			$detail 		= $this->M_kasir->get_detail_bill($list_data->Nomor);
			$tabel_guard 	= $this->M_kasir->get_tabel_guard($list_data->Table_Guard)->Nama;
			foreach($detail as $list){
				$Nama 		= $list->Nama;
				$Quantity 	= $list->Quantity;
				$Harga 		= $list->Harga;
				$Disc 		= $list->Disc;
				$Jumlah 	= $Quantity * $Harga;

				$sub_total[] = $Jumlah; 
				$output2[] = array(
								"Nama" => $Nama,
								"Quantity" => $Quantity,
								"Harga" => format_digit($Harga),
								"Disc" => $Disc,
								"Jumlah" => format_digit($Jumlah)
							);
			};
			$Jumlah = array_sum($sub_total);
			
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
			$row[] = $tabel_guard;
            //$row[] = format_digit($list_data->Harga * $list_data->Quantity);
            $row[] = format_digit($Jumlah);
            $row[] = format_digit($list_data->Disc);
            $row[] = format_digit($list_data->Voucher);
			//PPN
            $row[] = format_decimal(($list_data->PPN / 100) * $Jumlah);
			//ServiceCharge
            $row[] = format_digit(round($list_data->ServiceCharge * $Jumlah / 100, 0));
            $row[] = format_digit($list_data->Total);
            $row[] = $list_data->Pembuat;
            $row[] = getHour($list_data->Jam);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_kasir->count_all($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_kasir->count_filtered_detail($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        echo json_encode($output);
    }
	
	public function ajax_harian_all(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_filter_harian($dari_tanggal, $sampai_tanggal);
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
            $row[] = getHour($list_data->Jam);
            $row[] = format_digit($list_data->Total);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_kasir->count_harian($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_kasir->count_filtered_harian($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function kasir_detail(){
		$nomor = $this->input->post('nomor');
        $list_data = $this->M_kasir->get_detail_bill($nomor);
		foreach($list_data as $list){
			$Nama 		= $list->Nama;
			$Quantity 	= $list->Quantity;
			$Harga 		= $list->Harga;
			$Disc 		= $list->Disc;
			$Jumlah 	= $Quantity * $Harga;

			$output[] = array(
							"Nama" => $Nama,
							"Quantity" => $Quantity,
							"Harga" => format_digit($Harga),
							"Disc" => $Disc,
							"Jumlah" => format_digit($Jumlah)
						);
		}
		
		echo json_encode($output);
    }
	
	public function sales_average(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Laporan Transaksi";
		$data['deskripsi'] = "Sales Average";
		
		$this->template->views('report/sales_average', $data);
	}

	public function laporan_gaji_therapist(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Data Gaji Therapist";
		$data['deskripsi'] = "";
		$this->template->views('report/laporan_gaji_therapist', $data);
	}

	public function potongan_therapist(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Potongan Therapist";
		$data['deskripsi'] = "";
		$this->template->views('report/potongan_therapist', $data);
	}

	public function tabungan_therapist(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Tabungan Therapist";
		$data['deskripsi'] = "";
		$this->template->views('report/tabungan_therapist', $data);
	}

	public function komisi_gaji_spv_therapist_mess(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Komisi gaji SPV Therapist & Ibu Mess";
		$data['deskripsi'] = "";
		$this->template->views('report/komisi_gaji_spv_therapist_mess', $data);
	}

	public function komisi_fnb(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Komisi FNB";
		$data['deskripsi'] = "";
		$this->template->views('report/komisi_fnb', $data);
	}

	public function komisi_lain_lain(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Komisi Lain Lain";
		$data['deskripsi'] = "";
		$this->template->views('report/komisi_lain_lain', $data);
	}

	public function komisi_kodes(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Komisi Kode 'S'";
		$data['deskripsi'] = "";
		$this->template->views('report/komisi_kodes', $data);
	}

	public function komisi_kuesioner(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Komisi Kuesioner";
		$data['deskripsi'] = "";
		$this->template->views('report/komisi_kuesioner', $data);
	}

	public function report_category_branch(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Pilih Jenis Laporan";
		$data['deskripsi'] = "";
		$this->template->views('report/category_laporan', $data);
	}
	public function komisi_gro(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Pilih Jenis Laporan Komisi ";
		$data['deskripsi'] = "";
		$this->template->views('report/laporan_komisi_gro', $data);
	}
	public function print_rekapan_gaji_all(){
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Pilih Jenis Laporan";
		$data['deskripsi'] = "";

		$dari_tanggal 	= $_GET['from_date'];
		$sampai_tanggal = $_GET['to_date'];
		$branch_id 		= $_GET['branch_code'];
		
		$status_approve = $this->M_General->get_approval_registry($dari_tanggal, $sampai_tanggal, $branch_id);
		if (count($status_approve) > 0){
			$data['status_approve'] = $status_approve;
			$data['pimpinan_outlet'] = $this->M_General->get_user_by_kodekar($status_approve->users_id);
			//var_dump($status_approve->users_id); die();
		}
		
		$data['branch'] = $this->M_General->get_branch_by_branch_id($branch_id);
		$data['lists'] = $this->M_General->get_therapist_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id);
		
		$data['spv_therapist'] = $this->M_General-> get_data_gaji_spv_therapist($dari_tanggal, $sampai_tanggal, $branch_id);
		//var_dump($data['lists']); die();
		$this->pdf->load_view('report/print_rekapan_gaji', $data);
		$this->pdf->set_paper('F4', 'landscape');
		$this->pdf->render();
		$this->pdf->stream("rekapan_gaji_therapist" . $data['branch']->cname . ".pdf", array("Attachment" => false));		
	}
}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */