<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_admin');
	}

	public function index() {
		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "setting";
		$data['judul'] 			= "Setting";
		$data['deskripsi'] 		= "Setting";
		$this->template->views('setting', $data);
	}

	public function update() {
		$this->form_validation->set_rules('Nama', 'Nama', 'trim|required');

		$UserID = $this->userdata->UserID;
		
		$data = $this->input->post();
		//var_dump($data); die();
		if ($this->form_validation->run() == TRUE) {
			
			$result = $this->M_admin->update($data, $UserID);
			
			if ($result > 0) {
				$this->updateProfil();
				$this->session->set_flashdata('msg', show_succ_msg('Data profile Berhasil diubah'));
				redirect('profile');
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Data profile Gagal diubah'));
				redirect('profile');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('profile');
		}
	}

	public function change_password() {
		$this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
		$this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
		$this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');
		
		$UserID = $this->userdata->UserID;
		
		if ($this->form_validation->run() == TRUE) {
			if (($this->input->post('passLama')) == $this->userdata->Passwd) {
				if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
					$this->session->set_flashdata('msg', show_err_msg('Password Baru dan Konfirmasi Password harus sama'));
					redirect('profile');
				} else {
					$data = [
						'Passwd' => $this->input->post('passBaru')
					];
					
					$result = $this->M_admin->update($data, $UserID);
					
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Password Berhasil diubah'));
						redirect('profile');
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Password Gagal diubah'));
						redirect('profile');
					}
				}
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Password Salah'));
				redirect('profile');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('profile');
		}
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */