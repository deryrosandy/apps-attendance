<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Therapist extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_Data_therapist', 'M_General'));
	}
	
	public function index() {
		$this->data_therapist();
	}

	public function tambah() {
		
		$data['userdata'] 		= $this->userdata;	
		$branch_id = $this->userdata->branch_id;

		$data['branchs'] = $this->M_General->get_all_branch();
		$data['suppliers'] = $this->M_General->get_all_supplier();

		$data['page'] 			= "tambah";
		$data['judul'] 			= "Tambah Therapist";
		$data['deskripsi'] 		= "";
		
		$this->template->views('therapist/tambah_therapist', $data);
	}

	public function save_therapist() {
		
		$data['insert_by'] 		= $this->userdata->id;	

		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag'){

			if (!empty($flag)) { 

				$data = $this->input->post();
				
				$mutasi = $this->M_General->insert_therapist($data);     
			
			}
		}

		redirect('therapist/data_therapist');
	}

	public function update_therapist($therapist_id) {
		
		$data['therapist'] 	= $this->M_General->get_therapist_by_kodekar($therapist_id);
		$data['suppliers'] = $this->M_General->get_all_supplier();
		
		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag'){

			if (!empty($flag)) { 

				$data = $this->input->post();
				
				$update = $this->M_General->update_therapist($data);  

				redirect('therapist/data_therapist');				
			}
			
		}else{

			$data['page'] 			= "tambah";
			$data['judul'] 			= "Tambah Therapist";
			$data['deskripsi'] 		= "";

			$this->template->views('therapist/edit_therapist', $data);
		}	
	}

	public function update_document_therapist($therapist_id) {
		
		$data['therapist'] 	= $this->M_General->get_therapist_by_kodekar($therapist_id);
		$data['suppliers'] = $this->M_General->get_all_supplier();
		
		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag'){

			if (!empty($flag)) { 

				$data = $this->input->post();
				
				$update = $this->M_General->update_document_therapist($data);  

				redirect('therapist/data_therapist');				
			}
			
		}else{

			$data['page'] 			= "tambah";
			$data['judul'] 			= "Tambah Therapist";
			$data['deskripsi'] 		= "";

			$this->template->views('therapist/edit_therapist', $data);
		}	
	}

	public function update_family_therapist($therapist_id) {
		
		$data['therapist'] 	= $this->M_General->get_therapist_by_kodekar($therapist_id);
		$data['suppliers'] = $this->M_General->get_all_supplier();
		
		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag'){

			if (!empty($flag)) { 

				$data = $this->input->post();
				
				$update = $this->M_General->update_document_therapist($data);  

				redirect('therapist/data_therapist');				
			}
			
		}else{

			$data['page'] 			= "tambah";
			$data['judul'] 			= "Tambah Therapist";
			$data['deskripsi'] 		= "";

			$this->template->views('therapist/edit_therapist', $data);
		}	
	}

	public function data_therapist() {
		
		$data['userdata'] 		= $this->userdata;	
		$branch_id = $this->userdata->branch_id;

		$data['therapists'] = $this->M_General->get_therapist_by_branch_id($branch_id);

		$data['page'] 			= "data_therapist";
		$data['judul'] 			= "Data Therapist";
		$data['deskripsi'] 		= "List Therapist";
		
		$this->template->views('therapist/data_therapist', $data);
	}
	
	public function therapist_pelatihan() {

		$data['userdata'] 		= $this->userdata;	

		$data['therapists'] = $this->M_General->get_all_therapist_by_status('pelatihan');

		$data['page'] 			= "therapist_pelatihan";
		$data['judul'] 			= "Therapist Ciawi";
		$data['deskripsi'] 		= "List Therapist Ciawi";
		
		$this->template->views('therapist/data_therapist', $data);
	}
	
	public function therapist_magang() {

		$data['userdata'] 		= $this->userdata;	

		$data['therapists'] = $this->M_General->get_all_therapist_by_status('magang');

		$data['page'] 			= "therapist_magang";
		$data['judul'] 			= "Therapist Pemantapan";
		$data['deskripsi'] 		= "List Therapist Pemantapan";
		
		$this->template->views('therapist/data_therapist', $data);
	}
	
	public function gallery() {

		$data['userdata'] 		= $this->userdata;	
		$branch_id = $this->userdata->branch_id;

		$data['therapists'] = $this->M_General->get_therapist_by_branch_id($branch_id);

		$data['page'] 			= "gallery_therapist";
		$data['judul'] 			= "Gallery Therapist";
		$data['deskripsi'] 		= "";
		
		$this->template->views('therapist/gallery_therapist', $data);
	}
	
	public function detail_gallery() {

		$data['userdata'] 		= $this->userdata;	
		$branch_id = $this->userdata->branch_id;

		$data['therapists'] = $this->M_General->get_therapist_by_branch_id($branch_id);

		$data['page'] 			= "gallery_therapist";
		$data['judul'] 			= "Gallery Therapist";
		$data['deskripsi'] 		= "";
		
		$this->template->views('therapist/detail_gallery_therapist', $data);
	}
	
	public function select_branch(){

		$data['userdata'] = $this->userdata;
		$data['page'] = "Data Therapist";
		$data['judul'] = "Data Therapist";		
	
		$branch_id = $_GET['branch_code'];
		$date1 = date("Y-m-21",strtotime("-1 month"));
		$date2 = date("Y-m-20",strtotime("0 month"));
		$datenow = date("Y-m-20");

		$data['branch'] = $this->M_General->get_branch_by_branch_id($branch_id);

		$data['deskripsi'] = $data['branch']->cname;

		$data['therapist'] = $this->M_General->get_therapist_by_branch_id($date1, $date2, $branch_id);

		$this->template->views('therapist/data_therapist_branch', $data);

	}

	public function mutasi_therapist($kodekar = NULL, $report_month = NULL) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "mutasi_therapist";
		$data['judul'] 			= "Detail Therapist";
		$data['deskripsi'] 		= "Detail Therapist";
		$data['icon'] 			= "icon ion-ios-gear-outline";		

		$data['branchs'] = $this->M_General->get_all_branch();

		
		$data['report_month'] = date('Y-m');
		
		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'mutasi'){

			if (!empty($flag)) { 

				$branch_id = $this->input->post('branch_id');
				$therapist_id = $this->input->post('therapist_id');

				$mutasi = $this->M_General->mutasi_therapist($therapist_id, $branch_id);                     
                   
			}			

			$this->template->views('therapist/mutasi_therapist', $data);

		}else{
			
			$flag = $this->input->post('flag', TRUE);
			
			if (!empty($flag)) { 
				$data['flag'] = 1;
				
                if (!empty($branch_id)) {
                    $data['branch_id'] = $branch_id;
                } else {
                    if($this->input->post('branch_id') == '' || $this->input->post('branch_id') == '0'){
                        $data['branch_id'] = 'all';
                    }else{
                        $data['branch_id'] = $this->input->post('branch_id', TRUE);
                    }
				}

				$data['therapists'] = $this->M_General->get_therapist_by_branch_id($data['branch_id']);                     
                   
			}			
			
			$this->template->views('therapist/mutasi_therapist', $data);

		}
        
	}

	public function ajax_get_detail_therapist() {
		$kodekar 	= $this->input->post('kodekar');
		
		$therapist = $this->M_General->get_therapist_by_kodekar($kodekar);
		
		echo json_encode($therapist);
	}

	public function ajax_mutasi_therapist() {

		$branch_id = $this->input->post('branch_id');
		$kodekar = $this->input->post('kodekar');

		$mutasi = $this->M_General->mutasi_therapist($kodekar, $branch_id);  

		echo json_encode($mutasi);
	}	

	public function ranking_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Ranking Penjualan";
		$this->template->views('kasir/ranking_penjualan', $data);
	}
	
	public function ajax_ranking_penjualan() {
		
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');		
		
		$list = $this->M_kasir->get_ranking_penjualan($dari_tanggal, $sampai_tanggal);
		$data = array();
		$toal_all = 0;
		
		$no = $_POST['start'];
		foreach ($list as $list_data){
			$menu = $this->M_kasir->get_menu_name($list_data->Kode);
			$total = $list_data->Total + $list_data->PPN + $list_data->Service;			
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $menu->Nama;
			$row[] = $list_data->Qty;
			//$row[] = (format_digit($total));
			$row[] = $total;
			$data[] = $row;
			$toal_all += $list_data->Total;
		}
		
		//var_dump($sum); die();
		
		$output = array(
					"draw" => $_POST['draw'],
					"data" => $data,
					"toal_all" => $toal_all,
				);
		//output to json format
		echo json_encode($output);
	}

	public function laba_rugi() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Laba/Rugi";
		$this->template->views('kasir/laba_rugi', $data);
	}

	public function summary_penjualan() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Summary";
		$this->template->views('kasir/summary_penjualan', $data);
	}

	public function rekap_nominal() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "kasir";
		$data['judul'] = "Laporan Transaksi Kasir";
		$data['deskripsi'] = "Rekap Nominal";
		$this->template->views('kasir/rekap_nominal', $data);
	}

	public function ajax_harian(){
		$dari_tanggal 	= $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_filter_harian($dari_tanggal, $sampai_tanggal);
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
            $row[] = getHour($list_data->Jam);
            $row[] = format_digit($list_data->Total);
            $data[] = $row;
        }

		//var_dump($_POST); die();
		
        $output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->M_kasir->count_harian($dari_tanggal, $sampai_tanggal),
					"recordsFiltered" => $this->M_kasir->count_filtered_harian($dari_tanggal, $sampai_tanggal),
					"data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function ajax_detail_penjualan(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_detail_penjualan($dari_tanggal, $sampai_tanggal);
		//var_dump($list); die();
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
			//print_r($list_data);
			$detail 		= $this->M_kasir->get_detail_bill($list_data->Nomor);
			$tabel_guard 	= $this->M_kasir->get_tabel_guard($list_data->Table_Guard)->Nama;
			foreach($detail as $list){
				$Nama 		= $list->Nama;
				$Quantity 	= $list->Quantity;
				$Harga 		= $list->Harga;
				$Disc 		= $list->Disc;
				$Jumlah 	= $Quantity * $Harga;

				$sub_total[] = $Jumlah; 
				$output2[] = array(
								"Nama" => $Nama,
								"Quantity" => $Quantity,
								"Harga" => format_digit($Harga),
								"Disc" => $Disc,
								"Jumlah" => format_digit($Jumlah)
							);
			};
			$Jumlah = array_sum($sub_total);
			
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
			$row[] = $tabel_guard;
            //$row[] = format_digit($list_data->Harga * $list_data->Quantity);
            $row[] = format_digit($Jumlah);
            $row[] = format_digit($list_data->Disc);
            $row[] = format_digit($list_data->Voucher);
			//PPN
            $row[] = format_decimal(($list_data->PPN / 100) * $Jumlah);
			//ServiceCharge
            $row[] = format_digit(round($list_data->ServiceCharge * $Jumlah / 100, 0));
            $row[] = format_digit($list_data->Total);
            $row[] = $list_data->Pembuat;
            $row[] = getHour($list_data->Jam);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_kasir->count_all($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_kasir->count_filtered_detail($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        echo json_encode($output);
    }
	
	public function ajax_harian_all(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_kasir->get_datatables_filter_harian($dari_tanggal, $sampai_tanggal);
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
            $row[] = getHour($list_data->Jam);
            $row[] = format_digit($list_data->Total);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_kasir->count_harian($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_kasir->count_filtered_harian($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function kasir_detail(){
		$nomor = $this->input->post('nomor');
        $list_data = $this->M_kasir->get_detail_bill($nomor);
		foreach($list_data as $list){
			//var_dump($list);
			$Nama 		= $list->Nama;
			$Quantity 	= $list->Quantity;
			$Harga 		= $list->Harga;
			$Disc 		= $list->Disc;
			$Jumlah 	= $Quantity * $Harga;

			$output[] = array(
							"Nama" => $Nama,
							"Quantity" => $Quantity,
							"Harga" => format_digit($Harga),
							"Disc" => $Disc,
							"Jumlah" => format_digit($Jumlah)
						);
		}
		
		 echo json_encode($output);
    }
}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */