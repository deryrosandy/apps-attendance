<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_Data_therapist', 'M_General'));
	}
	
	public function index() {
		$this->voucher_list();
    }
    
    public function list() {
		$this->voucher_list();
	}

	public function voucher_list() {
		
		$data['userdata'] 		= $this->userdata;	
		$data['page'] 			= "data_voucher";
		$data['judul'] 			= "Data Voucher";
		$data['icon'] 			= "ion-ios-briefcase-outline";
		$data['deskripsi'] 		= "Voucher Verification System";

		$this->template->views('voucher/list', $data);
    }

    public function ajax_list_data_voucher() {

		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];

		/*Menghitung total desa didalam database*/
		$total=$this->db->count_all_results("voucher");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();

		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		    $this->db->like("barcode",$search);
		}

		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('barcode','DESC');
		$query=$this->db->get('voucher');

		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
            $this->db->like("id",$search);
            $jum=$this->db->get('voucher');
            $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		$no=$start+1;
		foreach ($query->result_array() as $voucher) {
			$output['data'][]=array(
				$no,
				$voucher['barcode'],
                $voucher['nominal'],
				tgl_indo2($voucher['active_date']),
                tgl_indo2($voucher['expire_date']),
                get_status_voucher($voucher['status']),
				'<div class="btn-group" style="display: -webkit-inline-box">
                    <button class="btn btn-sm btn-primary btn-icon" data_id=' . $voucher["id"] . '>
                        <div><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
                    </button>
				</div>',
			);
		$no++;
		}
		
		echo json_encode($output);
	}

}