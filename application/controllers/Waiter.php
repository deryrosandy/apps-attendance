<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Waiter extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_waiter');
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "waiter";
		$data['judul'] = "Transaksi Kasir";
		$data['deskripsi'] = "";
		$this->template->views('waiter/report_harian', $data);
	}
	
	public function transaksi_perhari() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "waiter";
		$data['judul'] = "Laporan Transaksi Waiter";
		$data['deskripsi'] = "Laporan Harian";
		$this->template->views('waiter/transaksi_perhari', $data);
	}

	public function log_transaksi() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "waiter";
		$data['judul'] = "Laporan Transaksi Waiter";
		$data['deskripsi'] = "Log Transaksi";
		$this->template->views('waiter/log_transaksi', $data);
	}
	
	public function ajax_harian(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_waiter->get_datatables_filter_harian($dari_tanggal, $sampai_tanggal);
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
            $row[] = getHour($list_data->Jam);
            $row[] = format_digit($list_data->Total);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_waiter->count_harian($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_waiter->count_filtered_harian($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function ajax_detail_penjualan(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_waiter->get_datatables_detail_penjualan($dari_tanggal, $sampai_tanggal);
		//var_dump($list); die();
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
			//print_r($list_data);
			$detail 		= $this->M_waiter->get_detail_bill($list_data->Nomor);
			$tabel_guard 	= $this->M_waiter->get_tabel_guard($list_data->Table_Guard)->Nama;
			foreach($detail as $list){
				$Nama 		= $list->Nama;
				$Quantity 	= $list->Quantity;
				$Harga 		= $list->Harga;
				$Disc 		= $list->Disc;
				$Jumlah 	= $Quantity * $Harga;

				$sub_total[] = $Jumlah; 
				$output2[] = array(
								"Nama" => $Nama,
								"Quantity" => $Quantity,
								"Harga" => format_digit($Harga),
								"Disc" => $Disc,
								"Jumlah" => format_digit($Jumlah)
							);
			};
			$Jumlah = array_sum($sub_total);
			
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
			$row[] = $tabel_guard;
            //$row[] = format_digit($list_data->Harga * $list_data->Quantity);
            $row[] = format_digit($Jumlah);
            $row[] = format_digit($list_data->Disc);
            $row[] = format_digit($list_data->Voucher);
			//PPN
            $row[] = format_decimal(($list_data->PPN / 100) * $Jumlah);
			//ServiceCharge
            $row[] = format_digit(round($list_data->ServiceCharge * $Jumlah / 100, 0));
            $row[] = format_digit($list_data->Total);
            $row[] = $list_data->Pembuat;
            $row[] = getHour($list_data->Jam);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_waiter->count_all($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_waiter->count_filtered_detail($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        echo json_encode($output);
    }
	
	public function ajax_harian_all(){
		$dari_tanggal = $this->input->post('dari_tanggal');
		$sampai_tanggal = $this->input->post('sampai_tanggal');
		
        $list = $this->M_waiter->get_datatables_filter_harian($dari_tanggal, $sampai_tanggal);
        $data = array();
		
        $no = $_POST['start'];
        foreach ($list as $list_data) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tgl_indo2($list_data->Tanggal);
            $row[] = $list_data->Nomor;
            $row[] = $list_data->NoMeja;
            $row[] = $list_data->NoPesan;
            $row[] = getHour($list_data->Jam);
            $row[] = format_digit($list_data->Total);
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->M_waiter->count_harian($dari_tanggal, $sampai_tanggal),
                        "recordsFiltered" => $this->M_waiter->count_filtered_harian($dari_tanggal, $sampai_tanggal),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function kasir_detail(){
		$nomor = $this->input->post('nomor');
        $list_data = $this->M_waiter->get_detail_bill($nomor);
		foreach($list_data as $list){
			//var_dump($list);
			$Nama 		= $list->Nama;
			$Quantity 	= $list->Quantity;
			$Harga 		= $list->Harga;
			$Disc 		= $list->Disc;
			$Jumlah 	= $Quantity * $Harga;

			$output[] = array(
							"Nama" => $Nama,
							"Quantity" => $Quantity,
							"Harga" => format_digit($Harga),
							"Disc" => $Disc,
							"Jumlah" => format_digit($Jumlah)
						);
		}
		
		 echo json_encode($output);
    }
}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */