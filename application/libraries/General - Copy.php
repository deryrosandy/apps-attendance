<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class General {

    var $ci;

    function __construct() {
        $this->ci = &get_instance();
//        $this->isLogin();
    }

    function isLogin() {
        if ($this->ci->session->userdata('is_login') == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function cekUserLogin() {
        if ($this->isLogin() != TRUE) {
            $this->ci->session->set_flashdata('message', 'Anda tidak memiliki hak akses');
            redirect('users/login');
        }
    }

    function cekAdminLogin() {
        if ($this->isLogin() == TRUE) {
            if ($this->ci->session->userdata('level') != 1) {
                $this->ci->session->set_flashdata('message', 'Anda tidak memiliki hak akses halaman Administrator');
                redirect('users/login');
            }
        } else {
            $this->ci->session->set_flashdata('message', 'Anda tidak memiliki hak akses halaman Administrator');
            redirect('users/login');
        }
    }

    function getAllNewProducts($limit) {
        $this->ci->load->model('Mproduct');
        $products = $this->ci->Mproduct->getAllNewProducts($limit);
        return $products;
    }
	
    function getProductsFeatured($limit) {
        $this->ci->load->model('Mproduct');
        $products = $this->ci->Mproduct->getProductsFeatured($limit);
        return $products;
    }

    function getProductsRelated($id, $catId, $limit) {
        $this->ci->load->model('Mproduct');
        $products = $this->ci->Mproduct->getProductsRelated($id, $catId, $limit);
        return $products;
    }

    function getProductsByCategoryId($categoryId) {
        $this->ci->load->model('Mproduct');
        $products = $this->ci->Mproduct->getProductsByCategoryId($categoryId);
        return $products;
    }
	
    function getProductsBtmHome($limit) {
        $this->ci->load->model('Mproduct');
        $products = $this->ci->Mproduct->getProductsBtmHome($limit);
        return $products;
    }

    function getCategory() {
        $this->ci->load->model('Category');
        $categories = $this->ci->Category->getCategory();
        return $categories;
    }

    function getDiscountedProducts() {
        $this->ci->load->model('Mproduct');
        $products = $this->ci->Mproduct->getDiscountedProducts();
        return $products;
    }

    function getPagesByPermalink($permalink) {
        $this->ci->load->model('Page');
        $page = $this->ci->Page->getPagesByPermalink($permalink);
        return $page;
    }

    function generateRandomCode($length = 8) {
        // Available characters
        $chars = '0123456789abcdefghjkmnoprstvwxyz';

        $Code = '';
        // Generate code
        for ($i = 0; $i < $length; ++$i) {
            $Code .= substr($chars, (((int) mt_rand(0, strlen($chars))) - 1), 1);
        }
        return strtoupper($Code);
    }

    function getSetting($key) {
        $this->ci->load->model('Setting');
        $setting = $this->ci->Setting->getSettingByKey($key);
		return $setting['value'];
    }

    function getSingleMedia($type, $key) {

        $this->ci->load->model('Product_image');
        $image = $this->ci->Product_image->findSingle($key);
        return $image;
    }
	
    function getMultipleMedia($key) {

        $this->ci->load->model('Product_image');
        $images = $this->ci->Product_image->findMultiple($key);
		return $images;
	}

    public function humanDate($datetime) {
        return date("D, d M Y H:i:s", strtotime($datetime));
    }

    public function humanDate2($date) {
        return date("D, d M Y", strtotime($date));
    }

    function isExistFile($filename) {
//        echo $filename;exit;
        if (file_exists($filename)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function setPaymentDeadline($days) {

        $dueDate = mktime(0, 0, 0, date("m"), date("d") + $days, date("Y"));
        return date("Y-m-d", $dueDate);
    }

    function getBankName() {
        $setting = $this->getSetting('Bank.Name');
        $banks = explode(',', $setting);
        return $banks;
    }

    function getSlides() {
        $this->ci->load->model('Slide');
        $this->ci->load->model('Media');
        $slides = $this->ci->Slide->findActive();
        $data = array();
        $i = 0;
        if (!empty($slides)):
            foreach ($slides as $slide) {
                $image = $this->ci->Media->findSingle('slide', $slide['id']);
                $data[$i]['id'] = $slide['id'];
                $data[$i]['path'] = $image['path'];
                $data[$i]['title'] = $slide['title'];
                $data[$i]['description'] = $slide['description'];
                $i++;
            }
        endif;
        return $data;
    }

    function isExistNextSlide($position) {
        $this->ci->load->model('Slide');
        $slide = $this->ci->Slide->getNextSlide($position);
        if (!empty($slide)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function isExistPrevSlide($position) {
        $this->ci->load->model('Slide');
        $slide = $this->ci->Slide->getPrevSlide($position);
        if (!empty($slide)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	function getPartialBanner($catId) {
        $this->ci->load->model('Mbanner');
        $banner = $this->ci->Mbanner->getBannerActiveByCatId($catId);
		
        return $banner;
    }
	
	function getRestoName() {
        $this->ci->load->model('M_general');
        $restoName = $this->ci->M_general->getRestoName();
        return $restoName;
    }
	
	function getTotaltransaksi() {
        $this->ci->load->model('M_general');
        $data_now = $this->ci->M_general->getTotaltransaksi();
		
		$sum = 0;
		foreach($data_now as $key=>$value){

		if(isset($value->Total))   
			$sum += $value->Total;
		}
		if($sum == 0){
			return '<span style="font-size:28px;">Belum Ada Transaksi</span>';
		}else{
			return format_digit($sum);
		}
	}
	function getMonthlyTransaksi() {
        $this->ci->load->model('M_general');
        $data_now = $this->ci->M_general->getMonthlyTransaksi();
		
		$sum = 0;
		foreach($data_now as $key=>$value){

		if(isset($value->Total))   
			$sum += $value->Total;
		}
		if($sum == 0){
			return '<span style="font-size:28px;">Belum Ada Transaksi</span>';
		}else{
			return format_digit($sum);
		}
	}
}

?>
