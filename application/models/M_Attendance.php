<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Attendance extends CI_Model {

	function __construct()
    {
        parent::__construct();
        $this->load->helper('parse');
        
    }

	public function get_setting(){
    	$data = $this->db->get('pengaturan')->row();
    	return $data;
    }
	public function get_ip_address_from_branch_id($branch_id){
    	$this->db->where('branch_id', $branch_id);
        $data = $this->db->get('fingerspot')->row();
        
        if($data !== null){
            return $data;
        }else{
            return false;
        }
    	
    }

    public function if_exist_check($userid, $DateTime){
        $data = $this->db->get_where('attendance_log', array('userid' => $userid, 'date_time' => $DateTime))->row();
        return $data;
    }

	public function get_status_fingerspot($branch_id){

        $fingerspot = $this->get_ip_address_from_branch_id($branch_id);
       
        if(!empty($fingerspot)){
            $IP = $this->get_ip_address_from_branch_id($branch_id)->ip;
            $Key = $this->get_ip_address_from_branch_id($branch_id)->password;
        }else{
            $IP = 0;
            $Key = 0;
        }        
        
        if($IP!=""){
            $Connect = @fsockopen($IP, "80", $errno, $errstr, 1);

            if($Connect){
                $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
                $newLine="\r\n";
                fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
                fputs($Connect, "Content-Type: text/xml".$newLine);
                fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
                fputs($Connect, $soap_request.$newLine);
                $buffer="";
                while($Response=fgets($Connect, 10240)){
                    $buffer=$buffer.$Response;                    
                }

                $buffer = Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");        

                if($buffer){
                	return true;
                } else {
                	return false;
                }

            }else{
                return false;
            }
        } 
    }

	public function get_attendance_log_by_branch_id($branch_id){

        $IP = $this->get_ip_address_from_branch_id($branch_id)->ip;
        $Key = $this->get_ip_address_from_branch_id($branch_id)->password;
       
        
        if($IP!=""){
            $Connect = @fsockopen($IP, "80", $errno, $errstr, 1);

            if($Connect){

                $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
                $newLine="\r\n";
                fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
                fputs($Connect, "Content-Type: text/xml".$newLine);
                fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
                fputs($Connect, $soap_request.$newLine);
                $buffer="";
                while($Response=fgets($Connect, 10240)){
                    $buffer=$buffer.$Response;                                      
                }

                $buffer = Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>"); 
                
                $buffer = explode("\r\n",$buffer);
                for($a=0;$a<count($buffer);$a++){
                    $data = Parse_Data($buffer[$a],"<Row>","</Row>");
                    $userid = Parse_Data($data,"<PIN>","</PIN>");
                    $DateTime = Parse_Data($data,"<DateTime>","</DateTime>");
                    $Verified = Parse_Data($data,"<Verified>","</Verified>");
                    $Status = Parse_Data($data,"<Status>","</Status>");
                    $ins = array(
                            "userid"       =>  $userid,
                            "date_time" =>  $DateTime,
                            "ver"		=>  $Verified,
                            "status"    =>  $Status,
                            "branch_id" =>  $branch_id
                        );
                   
                    if (!$this->if_exist_check($userid, $DateTime) && $userid && $DateTime) {
                    	$this->db->insert('attendance_log', $ins);
                    }
                }

                return true;

            }else{
                return false;
            }
        } 
    }

    public function get_attendance_info_list($branch_id) {
		
		$this->db->select('a.*, tp.first_name, tp.last_name');
		$this->db->from('attendance_log a');
		$this->db->join('employee tp', 'tp.userid = a.userid', 'left');
		$this->db->join('division d', 'd.division_id = a.branch_id', 'left');
		$this->db->where('tp', "'$branch_id'", 'left');
		$this->db->limit(1000, 0);
		$query = $this->db->get();
		
		//print_r($this->db->last_query()); die();

		return $query->result();
	}

}

/* End of file Absen_model.php */
/* Location: ./application/models/Absen_model.php */