<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Komisi_lulur extends CI_Model {
	
    var $table = 'sesi_therapist';
    var $table_tkar = 'tkar';

	public function get_total_session_by_filter($dari_tanggal, $sampai_tanggal, $branch_id){
        
        $sql="Select b.kodekar, b.kodetrp, b.namakar, 
                (SELECT SUM(jmlsesi) FROM sesi_therapist WHERE kodeservice='SUI' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal ."'AND branch_code=a.branch_code) AS total_suite,
                (SELECT SUM(jmlsesi) FROM sesi_therapist WHERE kodeservice='PRE' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal ."'AND branch_code=a.branch_code) AS total_pre,
                (SELECT SUM(jmlsesi) FROM sesi_therapist WHERE kodeservice='PRS' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'AND branch_code=a.branch_code) AS total_prs,
                (SELECT SUM(jmlsesi) FROM sesi_therapist WHERE kodeservice='STD' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'AND branch_code=a.branch_code) AS total_std,
                (SELECT SUM(jmlext) FROM registrydet WHERE kodeservice IN ('PRE', 'SUI', 'STD', 'PRS') AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'AND branch_code=a.branch_code) AS total_point,                
                (SELECT SUM(jmlsesi) FROM sesi_therapist WHERE kodeservice IN ('SUI', 'PRE', 'PRS') AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'AND branch_code=a.branch_code) AS total_sesi_lulur,
                (SELECT SUM(jmlsesi) FROM sesi_therapist WHERE kodeservice='STD' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'AND branch_code=a.branch_code) AS total_sesi_massage               
                From sesi_therapist a
                JOIN tkar b ON a.kodekar = b.kodekar
                AND tgl>='" . $dari_tanggal ."'
                AND tgl<='" . $sampai_tanggal ."'
                AND a.branch_code='" . $branch_id ."'
                Group by b.[kodekar], b.kodetrp, b.namakar, a.branch_code
                Order By b.kodekar ASC";
       
        /*
        $sql="Select b.kodekar, b.kodetrp,  b.namakar, a.branch_code,
                    (SELECT  CAST(SUM(jmlext) AS INT) FROM registrydet
                                    WHERE kodeservice='SUI' AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
                                    AND noreg In (Select noreg From registry Where tanggal>='" . $dari_tanggal ."' AND kodekar=b.kodekar AND tanggal<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."')) AS total_suite,
                    (SELECT  CAST(SUM(jmlext) AS INT)  FROM registrydet
                                    WHERE kodeservice='PRE' AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
                                    AND noreg In (Select noreg From registry Where tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal ."' AND kodekar=b.kodekar AND branch_code='" . $branch_id ."')) AS total_pre,
                    (SELECT  CAST(SUM(jmlext) AS INT)  FROM registrydet
                                    WHERE kodeservice='PRS'  AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
                                    AND noreg In (Select noreg From registry Where tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal ."' AND kodekar=b.kodekar AND branch_code='" . $branch_id ."')) AS total_prs,
                    (SELECT  CAST(SUM(jmlext) AS INT)  FROM registrydet
                                    WHERE kodeservice='STD'  AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."'  AND branch_code='" . $branch_id ."'
                                    AND noreg In (Select noreg From registry Where tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal ."' AND kodekar=b.kodekar AND branch_code='" . $branch_id ."')) AS total_std,
                    (SELECT  CAST(SUM(jmlext) AS INT) FROM registrydet
                                    WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS') AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
                                    AND noreg In (Select noreg From registry Where tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal ."' AND kodekar=b.kodekar AND branch_code='" . $branch_id ."')) AS total_point,                
                    (SELECT  CAST(SUM(jml) AS INT) FROM registrydet
                                    WHERE kodeservice IN ('SUI', 'PRE', 'PRS') AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
                                    AND noreg In (Select noreg From registry Where tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal ."' AND kodekar=b.kodekar AND branch_code='" . $branch_id ."')) as total_sesi_lulur,
                    (SELECT  CAST(SUM(jml) AS INT) FROM registrydet
                                    WHERE kodeservice IN ('STD') AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
                                    AND noreg In (Select noreg From registry Where tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal ."' AND kodekar=b.kodekar AND branch_code='" . $branch_id ."')) as total_sesi_massage
                    From registrydet a
                    JOIN tkar b ON a.kodekar = b.kodekar
                    AND tgl>='" . $dari_tanggal ."'
                    AND tgl<='" . $sampai_tanggal ."'
                    AND a.branch_code='" . $branch_id ."'
                    AND a.status='Y'
                    Group by b.[kodekar], b.kodetrp, b.namakar, a.branch_code
                    Order By b.kodekar ASC";
        */
        $query=$this->db->query($sql);
        // print_r($this->db->last_query()); 
        
        return $query->result();

    }
    
    public function get_skema_komisi_massage($point, $levelkomisi){        
        $sql =  "Select max(nominal_skema) as nominal
                    From (
                        Select 
                        CASE
                            When point <= " . $point . " Then nominal
                        END as nominal_skema
                        From skema_komisi
                        Where levelkomisi = '" . $levelkomisi . " '
                        AND massage = 1
                    ) as massage
                Where nominal_skema != ''";
        $query = $this->db->query($sql);
        return $query->row()->nominal;        
    }

    public function get_skema_komisi_lulur($point, $levelkomisi){

        //var_dump($point); die();
        $sql =  "Select max(nominal_skema) as nominal
                    From (
                        Select 
                        CASE
                            When point <= " . $point . " Then nominal
                        END as nominal_skema
                        From skema_komisi
                        Where levelkomisi = " . $levelkomisi . "
                        AND lulur = 1
                    ) as lulur
                Where nominal_skema != ''";
        $query = $this->db->query($sql);
        //var_dump($this->db->last_query()); die();
        return $query->row()->nominal;
    }

	public function get_datatables_filter_harian_query($dari_tanggal, $sampai_tanggal){
        $this->db->from($this->table);
		$this->db->where('Tanggal >=', $dari_tanggal);	
		$this->db->where('Tanggal <=', $sampai_tanggal);	
		//return $query->result();
		//$this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])){ // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_by_date)){
            $order = $this->order_by_date;
			//var_dump($order); die();
           // $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
	public function get_datatables_detail_penjualan_query($dari_tanggal, $sampai_tanggal){
        //$this->db->select($this->table . '.*,' . $this->table_waiter . '.Nomor As NomorPesan,' . $this->table_waiter . '.TableGuard As Table_Guard,'. $this->table_detail . '.*'); 
        $this->db->select($this->table . '.*,' . $this->table_waiter . '.Nomor As NomorPesan,' . $this->table_waiter . '.TableGuard As Table_Guard,'); 
		$this->db->from($this->table);
		//$this->db->join ($this->table_detail, $this->table_detail . '.Nomor = ' . $this->table . '.Nomor' );
		$this->db->join ($this->table_waiter, $this->table_waiter . '.Nomor = ' . $this->table . '.NoPesan' );
		$this->db->where($this->table . '.Tanggal >=', $dari_tanggal);	
		$this->db->where($this->table . '.Tanggal <=', $sampai_tanggal);	
		//return $query->result();
		//$this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])){ // here order processing
            $this->db->order_by($this->column_order_detail[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_by_date)){
            $order = $this->order_by_date;
           // $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables_filter_harian($dari_tanggal, $sampai_tanggal){
        $this->get_datatables_filter_harian_query($dari_tanggal, $sampai_tanggal);
        if($_POST['length'] != -1);
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
		//print_r($this->db->last_query()); die();
        return $query->result();
    }
	
    function get_datatables_detail_penjualan($dari_tanggal, $sampai_tanggal){
        $this->get_datatables_detail_penjualan_query($dari_tanggal, $sampai_tanggal);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
		//print_r($this->db->last_query()); die();
        return $query->result();
    }
 
    function get_datatables_harian(){
        $this->get_datatables_harian_query();
		
		//if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();		
		
        return $query->result();
    }
	
    function get_tabel_guard($tabel_guard){
        $query =$this->db->get_where($this->table_user, array('UserID'=>$tabel_guard));
        return $query->row();
    }
 
    function get_detail_bill($nomor){
		$join = array($this->table_menu, $this->table_menu . '.Kode=' . $this->table_detail . '.Kode');
		$query = $this->db->join($join[0], $join[1])->get_where($this->table_detail, array('Nomor'=>$nomor));
		//$query = $this->db->get_where($this->table_detail, array('Nomor'=>$nomor));
        //var_dump($this->db->last_query()); die();
		return $query->result();
    }
 
    function count_filtered(){
        $this->get_datatables_harian_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
	
    function count_filtered_detail($dari_tanggal, $sampai_tanggal){
        $this->get_datatables_filter_harian_query($dari_tanggal, $sampai_tanggal);
       //$query = $this->db->get();
        return $this->db->count_all_results();
    }
    function count_filtered_harian($dari_tanggal, $sampai_tanggal){
		$this->get_datatables_filter_harian_query($dari_tanggal, $sampai_tanggal);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function count_harian($dari_tanggal, $sampai_tanggal){
		$this->get_datatables_filter_harian_query($dari_tanggal, $sampai_tanggal);
        $query = $this->db->get();
        return  $query->num_rows();
    }
    public function count_detail_penjualan($dari_tanggal, $sampai_tanggal){
		$this->get_datatables_filter_harian_query($dari_tanggal, $sampai_tanggal);
        $query = $this->db->get();
		//$this->db->from($this->table);
        return  $query->num_rows();
    }
    public function get_rekap_penjualan($dari_tanggal, $sampai_tanggal){
		
		$this->db->select($this->table . '.*,' . $this->table_detail. '.*'); 
		$this->db->from($this->table_detail);
		
		$this->db->join ($this->table, $this->table . '.Nomor = ' . $this->table_detail . '.Nomor' );

		$this->db->where($this->table . '.Tanggal >=', $dari_tanggal);	
		$this->db->where($this->table . '.Tanggal <=', $sampai_tanggal);		
		
		$query = $this->db->get();
		
		return $query->result();
    }
    public function get_ranking_penjualan($dari_tanggal, $sampai_tanggal){
		
		$this->db->select($this->table_detail . ' . Kode');
		$this->db->select('SUM((' . $this->table_detail . '.Quantity)) as order_qty');  
		$this->db->select('(SUM(' . $this->table_menu . '.HargaJual) / (count(' . $this->table_menu . '.HargaJual))) * (SUM(' . $this->table_detail . '.Quantity)) as Total');
		$this->db->select('(SUM(' . $this->table_menu . '.HargaJual) / (count(' . $this->table_menu . '.HargaJual))) * (SUM(' . $this->table_detail . '.Quantity) * 0.055) as Service');
		$this->db->select('(SUM(' . $this->table_menu . '.HargaJual) / (count(' . $this->table_menu . '.HargaJual))) * (SUM(' . $this->table_detail . '.Quantity) * 0.10) as PPN');
		$this->db->select('SUM(' . $this->table_detail . '.Quantity) as Qty'); 
		$this->db->from($this->table_detail);		
		$this->db->join ($this->table_menu, $this->table_menu . '.Kode = ' . $this->table_detail . '.Kode' );
		$this->db->join ($this->table, $this->table . '.Nomor = ' . $this->table_detail . '.Nomor' );

		$this->db->where($this->table . '.Tanggal >=', $dari_tanggal);	
		$this->db->where($this->table . '.Tanggal <=', $sampai_tanggal);	
		$query = $this->db->group_by($this->table_detail . '.Kode');
		$query = $this->db->order_by('order_qty', 'DESC');
		$query = $this->db->get();
		
		return $query->result();
    }
	
	public function get_menu_name($kode){
		
		$query =$this->db->get_where($this->table_menu, array('Kode'=>$kode));
        return $query->row();
	}
}

/* End of file M_pegawai.php */
/* Location: ./application/models/M_pegawai.php */