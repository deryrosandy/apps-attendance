<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {
	public function update($data, $UserID) {
		
		$this->db->where('id_user', $UserID);
		$this->db->update('users', $data); 
		
		return true;
	}

	public function select($UserID = '') {
		if ($id != '') {
			$this->db->where('UserID', $UserID);
		}

		$data = $this->db->get('users');

		return $data->row();
	}
}

/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */