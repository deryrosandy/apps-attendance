<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_general___ extends CI_Model {
	
	public function getRestoName() {
		$sql = "SELECT Nilai FROM Konstanta Where Nama='1. Nama Resto/Kafe'" ;
		
		$data = $this->db->query($sql);

		return $data->row('Nilai');
	}

	public function getAllBranch() {
		$sql = "SELECT * FROM branch";
		
		$data = $this->db->query($sql);

		return $data->result();
	}
	
	public function getGrossSales($dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("SUM(Total) as TotalGross");
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		
		$query = $this->db->get();
		return $query->row("TotalGross");
	}
	
	public function getSummaryHarian($dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("SUM(Total) as TotalSummary"); 		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		
		$query = $this->db->get();
		return $query->row("TotalSummary");
	}
	
	public function getSalesSummary($dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("SUM(Total) as SalesSummary"); 		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		
		$query = $this->db->get();
		return $query->row();
	}
	
	public function getSalesDiscount($dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("SUM((Quantity*harga)*(Disc/100)) as SalesDiscount"); 		
		$this->db->from("KasirDT");
		$this->db->where("Nomor IN (Select Nomor FROM KasirMS WHERE Tanggal >= '" . $dari_tanggal . "' AND Tanggal <= '" . $sampai_tanggal . "')");		
		
		$query = $this->db->get();
		
		return $query->row("SalesDiscount");
	}
	
	public function getNetSales($dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("SUM(Quantity*harga) as TotalNet"); 		
		$this->db->from("KasirDT");
		$this->db->where("Nomor IN (Select Nomor FROM KasirMS WHERE Tanggal >= '" . $dari_tanggal . "' AND Tanggal <= '" . $sampai_tanggal . "')");
		
		$query = $this->db->get();
		return $query->row("TotalNet");
	}
	
	public function getNumbOfTransaction($dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("COUNT(*) as NumbTransaction"); 		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");$query = $this->db->get();
		return $query->row("NumbTransaction");
	}
	
	public function getAvgSale($dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("(SUM(Total) / COUNT(*)) as AvgSales"); 		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		$query = $this->db->get();
		
		return $query->row("AvgSales");
	}
	
	public function getSalesAverage($dari_tanggal, $sampai_tanggal, $limit,$start) {	
		$this->db->select("Tanggal"); 		
		$this->db->select("(COUNT(*)) as TotalBill"); 		
		$this->db->select("(SUM(Total) / COUNT(*)) as AvgSales"); 		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		$this->db->group_by("Tanggal", "ASC");
		$this->db->limit($limit, $start);	
		//$this->db->order_by($col,$dir);	
	
		$query = $this->db->get();
		
		//print_r($this->db->last_query());
		
		return $query->result();
	}
	
	public function getCountSalesAverage($dari_tanggal, $sampai_tanggal) {		
		
		$this->db->select("Tanggal"); 		
		$this->db->select("(COUNT(*)) as TotalBill"); 		
		$this->db->select("(SUM(Total) / COUNT(*)) as AvgSales"); 		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		$this->db->group_by("Tanggal", "ASC");
		$query = $this->db->get();
		
		return $query->num_rows();  
	}
	
	public function getChartSales($dari_tanggal, $sampai_tanggal) {
		$this->db->select("CONVERT(varchar, Tanggal, 120) as Tanggal");	
		$this->db->select("SUM(Total) as TotalGross"); 
		$this->db->select("COUNT(*) as NumbTransaction");		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		$this->db->group_by("Tanggal", "ASC");
		$query = $this->db->get();
		
		return $query->result();
	}
	
	public function getChartSalesDaily($tanggal) {
		$this->db->select("Nomor"); 
		$this->db->select("CONVERT(varchar, Jam, 108) as Jam, Total");	
		$this->db->from("KasirMS");
		$this->db->where("Tanggal = '" . $tanggal . "'");
		$query = $this->db->get();
		
		return $query->result();
	}
	
	public function getTotaltransaksi() {
		$date_now = date('Y-m-d');
		$this->db->from("KasirMS");
		
		$query = $this->db->get();
		
        return $query->result();
	}
	
	public function getMonthlyTransaksi(){
		$year = date('Y');
		$month = date('m');
		$this->db->from("KasirMS");
		$this->db->where('MONTH(Tanggal) = ' .$month);
		$this->db->where('YEAR(Tanggal) = ' .$year);
		
		$query = $this->db->get();
		
		//print_r($this->db->last_query()); die();
		
        return $query->result();
	}
}

/* End of file M_pegawai.php */
/* Location: ./application/models/M_pegawai.php */