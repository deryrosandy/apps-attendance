<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
			<li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
			<li class="nav-item mr-auto">
				<a class="navbar-brand" href="<?php echo base_url(); ?>">
					<!--<img class="brand-logo" alt="modern admin logo" src="<?php //echo base_url(); ?>assets/images/logo/logo.png"> -->
					<h3 class="brand-logo float-left text-white text-bold-600">Att</h3>
					<h3 class="brand-text"> Management</h3>
				</a>
			</li>
			<li class="nav-item d-none d-md-block float-right"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="toggle-icon ft-toggle-right font-medium-3 white" data-ticon="ft-toggle-right"></i></a></li>
			<li class="nav-item d-md-none">
            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
          </li>
        </ul>
      </div>
      <div class="navbar-container content">
        <div class="collapse navbar-collapse" id="navbar-mobile">
          <ul class="nav navbar-nav mr-auto float-left">
            <li class="nav-item nav-search">
              <h4 class="nav-link nav-link-expand pb-1 pt-2 text-bold-500"><?php echo tgl_indo(date('Y-m-d')); ?></h4>
            </li>
          </ul>
          <ul class="nav navbar-nav float-right">
            <li class="dropdown dropdown-user nav-item">
              <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                <span class="mr-1">Hello,
                  <span class="user-name text-bold-700"><?php echo $this->userdata->first_name; ?> <?php echo $this->userdata->last_name; ?></span>
                </span>
                <span class="avatar avatar-online">
                  <img src="<?php echo base_url(); ?>assets/images/portrait/small/avatar-s-19.png" alt="avatar">
				</span>
              </a>
              	<div class="dropdown-menu dropdown-menu-right">
			  		<a class="dropdown-item" href="<?php echo base_url('profile'); ?>"><i class="ft-user"></i> Edit Profile</a>
				  <a class="dropdown-item" href="<?php echo base_url('auth/logout'); ?>"><i class="ft-power"></i> Logout</a>
              	</div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  