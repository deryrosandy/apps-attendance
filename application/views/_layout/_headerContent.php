<div class="content-header row">
	<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
		<h3 class="content-header-title mb-0 d-inline-block"><?php echo $judul; ?></h3>
		<div class="row breadcrumbs-top d-inline-block">
		<div class="breadcrumb-wrapper col-12">
			<ol class="breadcrumb">

				<?php
					for ($i=0; $i<count($this->session->flashdata('segment')); $i++) { 
						if ($i == 0) {
						?>
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a></li>
						<?php
						} elseif ($i == (count($this->session->flashdata('segment'))-1)) {
						?>
							<span class="breadcrumb-item active"><?php echo ucfirst($this->session->flashdata('segment')[$i]); ?></span>
						<?php
						} else {
						?>
							<li class="breadcrumb-item">
								<a href="<?php echo base_url(); ?><?php echo $this->session->flashdata('segment')[$i]; ?>"><?php echo ucfirst($this->session->flashdata('segment')[$i]); ?></a>
							</li>
						<?php
						}
						if ($i == 0 && $i == (count($this->session->flashdata('segment'))-1) && $this->session->flashdata('segment')[0] !== 'dashboard') {
						?>
							<li class="breadcrumb-item active"><?php echo ucfirst($this->session->flashdata('segment')[$i]); ?></li>
						<?php
						}
					}
				?>

			</ol>
		</div>
		</div>
	</div>
</div>