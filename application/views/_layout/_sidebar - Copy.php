<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
	
	 <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>assets/img/profil1.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $userdata->Nama; ?></p>
        <!-- Status -->
        <a href="<?php echo base_url(); ?>assets/#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
		<li class="header">LIST MENU</li>
		<!-- Optionally, you can add icons to the links -->

		<li <?php if ($page == 'dashboard') {echo 'class="active"';} ?>>
			<a href="<?php echo base_url('dashboard'); ?>">
			<i class="fa fa-dashboard"></i>
			<span>Dashboard</span>
			</a>
		</li>
      
		<li class="treeview <?php if ($page == 'waiter') {echo 'class="active"';} ?>"><a href="<?php echo site_url()?>waiter/"> <i class="fa fa-qrcode"></i>
			<span>Waiter</span> <i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li class="">
					<a href="<?php echo site_url()?>waiter/transaksi_perhari"><i class="fa fa-circle-o"></i>
						Traksaksi PerHari
					</a>
				</li>
				<li class="">
					<a href="<?php echo site_url()?>waiter/log_transaksi"><i class="fa fa-circle-o"></i>
						Log Traksaksi
					</a>
				</li>
			</ul>
		</li>

		<li class="treeview <?php if ($page == 'kasir') {echo 'active';} ?>"><a href="<?php echo site_url()?>kasir/"> <i class="fa fa-pie-chart"></i>
			<span>Kasir</span> <i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li class="">
					<a href="<?php echo site_url()?>kasir/report_harian"><i class="fa fa-circle-o"></i>
						Laporan Harian
					</a>
				</li>
				<li class="">
					<a href="<?php echo site_url()?>kasir/detail_penjualan"><i class="fa fa-circle-o"></i>
						Detail Penjualan
					</a>
				</li>
				<li class="">
					<a href="<?php echo site_url()?>kasir/rekap_penjualan"><i class="fa fa-circle-o"></i>
						Rekap Penjualan
					</a>
				</li>
				<li class="">
					<a href="<?php echo site_url()?>kasir/ranking_penjualan"><i class="fa fa-circle-o"></i>
						Ranking Penjualan
					</a>
				</li>
				<li class="">
					<a href="<?php echo site_url()?>kasir/laba_rugi"><i class="fa fa-circle-o"></i>
						Laba / Rugi
					</a>
				</li>
				<li class="">
					<a href="<?php echo site_url()?>kasir/summary_penjualan"><i class="fa fa-circle-o"></i>
						Summary Penjualan
					</a>
				</li>
				<li class="">
					<a href="<?php echo site_url()?>kasir/rekap_nominal"><i class="fa fa-circle-o"></i>
						Rekap Nominal
					</a>
				</li>
			</ul>
		</li>
		<li <?php if ($page == 'ganti_password') {echo 'class="active"';} ?>>
			<a href="<?php echo base_url('pengaturan'); ?>">
				<i class="fa fa-lock"></i>
				<span>Ganti Password</span>
			</a>
		</li>
		<li <?php if ($page == 'pengaturan') {echo 'class="active"';} ?>>
			<a href="<?php echo base_url('pengaturan'); ?>">
				<i class="fa fa-gear"></i>
				<span>Pengaturan</span>
			</a>
		</li>

    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>