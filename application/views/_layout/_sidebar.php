<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class="active nav-item"><a href="<?php echo base_url('dashboard'); ?>"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a> </li>   
      <li class=" navigation-header">
        <span data-i18n="nav.category.charts_maps">Main</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="main &amp; Maps"></i>
      </li>         
      <li class=" nav-item"><a href="<?php echo base_url('attendance/attendance_list'); ?>"><i class="la la-bar-chart"></i><span class="menu-title" data-i18n="nav.users.main">Attendance</span></a>
      <li class=" nav-item"><a href="<?php echo base_url('attendance/clock_in_out_log'); ?>"><i class="la la-clock-o"></i><span class="menu-title" data-i18n="nav.dash.main">Clock In/Out Log</span></a> </li>
      <li class=" nav-item"><a href="<?php echo base_url('attendance/pull_attendance_log'); ?>"><i class="la la-cloud-download"></i><span class="menu-title" data-i18n="nav.dash.main">Full FP</span></a> </li>  
      <li class=" nav-item"><a href="<?php echo base_url('attendance/leave'); ?>"><i class="la la-location-arrow"></i><span class="menu-title" data-i18n="nav.dash.main">Leave</span></a> </li>          
      <li class=" nav-item"><a href="#"><i class="la la-cubes"></i><span class="menu-title" data-i18n="nav.users.main">Employee</span></a>
        <ul class="menu-content">
          <li>
            <a class="menu-item" href="<?php echo base_url('report/overal-attendance-report'); ?>" data-i18n="nav.users.user_profile">Add New Employee</a>
          </li>
          <li>
            <a class="menu-item" href="<?php echo base_url('report/overal-attendance-report'); ?>" data-i18n="nav.users.user_profile">Employee List</a>
          </li>
        </ul>
      </li>
      <li class=" nav-item"><a href="#"><i class="la la-print"></i><span class="menu-title" data-i18n="nav.users.main">Report</span></a>
        <ul class="menu-content">
          <li>
            <a class="menu-item" href="<?php echo base_url('report/overal-attendance-report'); ?>" data-i18n="nav.users.user_profile">All Attendance Status</a>
          </li>
        </ul>
      </li>
      <li class=" navigation-header">
          <span data-i18n="nav.category.charts_maps">Master</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip"
          data-placement="right" data-original-title="Master"></i>
      </li>
      <li class=" nav-item"><a href="<?php echo base_url('master/company'); ?>"><i class="la la-industry"></i><span class="menu-title" data-i18n="nav.dash.main">Company</span></a> </li>
      <li class=" nav-item"><a href="<?php echo base_url('master/division'); ?>"><i class="la la-map-marker"></i><span class="menu-title" data-i18n="nav.dash.main">Division</span></a> </li>
      <li class=" nav-item"><a href="<?php echo base_url('master/department'); ?>"><i class="la la-institution"></i><span class="menu-title" data-i18n="nav.dash.main">Department</span></a> </li>
      <li class=" nav-item"><a href="<?php echo base_url('master/position'); ?>"><i class="la la-building-o"></i><span class="menu-title" data-i18n="nav.dash.main">Job Title / Position</span></a> </li>
      <li class=" nav-item"><a href="<?php echo base_url('master/fimgerspot'); ?>"><i class="la la-thumbs-o-up"></i><span class="menu-title" data-i18n="nav.dash.main">Solution FP</span></a> </li>
      <li class=" nav-item"><a href="<?php echo base_url('master/users'); ?>"><i class="la la-users"></i><span class="menu-title" data-i18n="nav.dash.main">Users</span></a> </li>
      
      <li class=" nav-item"><a href="<?php echo base_url('#'); ?>"><i class="la la-cog"></i><span class="menu-title" data-i18n="nav.users.main">Setting</span></a>
        <ul class="menu-content">
          <li>
            <a class="menu-item" href="<?php echo base_url('profile'); ?>" data-i18n="nav.users.user_profile">Profile</a>
          </li>
          <li>
            <a class="menu-item" href="<?php echo base_url('change_password'); ?>" data-i18n="nav.users.users_contacts">Change Password</a>
          </li>
        </ul>
      </li>

    </ul>
  </div>
</div>
