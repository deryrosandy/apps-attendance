<?php /*
<div class="br-pagebody">
  <div class="br-section-wrapper pd-30">

    <div class="row">

      <div class="col-xl-8 offset-xl-2 mg-t-0 mg-b-0">
        <div class="form-layout form-layout-5  border-0 pd-y-0">
          <form id="form" role="form" enctype="multipart/form-data" action="<?php echo base_url() ?>penilaian/kinerja_therapist" method="post" class="form-horizontal form-groups-bordered">
            <div class="row mg-t-10">
                <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span><strong> Select Month:</strong></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="text" value="<?php
                      if (!empty($report_month)) {
                          echo $report_month;
                      }
                      ?>" class="form-control form-control-1 input-sm fc-datepicker" placeholder="MM-YYYY" name="report_month" data-format="yyyy/mm/dd">
                </div>
              </div>
              <div class="row mg-t-10">
                <div class="col-sm-8 mg-l-auto">
                  <div class="form-layout-footer">
                    <button class="btn btn-info" name="flag" value="1">Submit</button>
                  </div><!-- form-layout-footer -->
                </div><!-- col-8 -->
              </div>
          </form>
        </div><!-- form-layout -->
      </div>
    
    </div>
  </div>
</div>
*/ ?>

<div class="br-profile-page">
    
  <form id="form" role="form" enctype="multipart/form-data" action="<?php echo base_url() ?>penilaian/update_therapist/<?php echo $therapist->therapist_id; ?>" method="post" class="form-horizontal form-groups-bordered">

      <div class="col-xl-12 pd-30 mg-b-0">
          
      <div class="ht-70 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-between shadow-base">
        <ul class="nav nav-outline active-info align-items-center flex-row" role="tablist">
          <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#posts" role="tab">Performance</a></li>
          <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#massage" role="tab">Massage</a></li>
          <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#services" role="tab">Services</a></li>
          <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#kedisiplinan" role="tab">Kedisiplinan</a></li>
          <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#kesehatan" role="tab">Kesehatan</a></li>
          <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#attitude" role="tab">Attitude</a></li>
          <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#grade" role="tab">Grade</a></li>
          <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#photos" role="tab">Photo</a></li>
        </ul>
        <h4 class="tx-14 tx-right tx-uppercase tx-bold tx-inverse mg-t-10 mg-b-10 justify-content-right d-flex flex-row"><?php echo $therapist->nit; ?>  /  <?php  echo (!$therapist_performance) ? bulan_tahun(date('Y-m-d')) : bulan_tahun($therapist_performance->created_at); ?></h4>
      </div>

        <div class="tab-content mg-t-30">
          <div class="tab-pane fade active show" id="posts">
            <div class="row">
              <div class="col-lg-12">
                <div class="card ht-100p shadow-base widget-9">
                    <div class="row no-gutters">
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-y-25 ">
                          
                          <div class="d-flex align-items-center justify-content-between">
                            <div class="">
                              <span class="tx-11 tx-uppercase tx-gray-600 tx-mont tx-semibold d-block mg-b-5"><?php echo get_level_therapist($therapist->level); ?></span>
                              <h4 class="tx-inverse tx-normal tx-roboto mg-b-5"><?php echo $therapist->fullname; ?></h4>
                              <p class="tx-12">
                                <span class="mg-r-10">NIT : <?php echo $therapist->nit; ?></span>
                                <span class="mg-r-10">Supplier : <?php echo get_supplier_name($therapist->supplier_id); ?> </span>
                              </p>
                            </div>
                            <h4 class="tx-inverse tx-normal tx-roboto mg-b-30">Branch : <?php echo get_branch_name($therapist->branch_id); ?></h4>
                          </div>

                          <div class="mg-t-10 mg-b-30">
                            <h5 class="tx-inverse tx-normal tx-roboto">Detail Information</h5>
                          </div>

                          <input name="therapist_id" type="hidden" value="<?php  echo $therapist->therapist_id; ?>" />

                          <div class="row">
                            <div class="col-lg-6">

                              <label class="tx-12 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Berat Badan</label>
                              <input  name="berat_badan" class="form-control mg-b-20" placeholder="Berat Badan (Kg)" type="number">
                 
                              <label class="tx-12 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Jerawat</label>
                              
                              <?php if(!$therapist_performance){ ?>
                                <div class="row">
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="jerawat" >
                                      <span>Ya</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="jerawat" checked >
                                      <span>Tidak</span>
                                    </label>
                                  </div>
                                </div>
                              <?php }else{ ?>
                                <div class="row">
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="jerawat" <?php  echo (($therapist_performance->jerawat==1) ? "checked" : ""); ?> >
                                      <span>Ya</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="jerawat" <?php  echo (($therapist_performance->jerawat==0) ? "checked" : ""); ?> >
                                      <span>Tidak</span>
                                    </label>
                                  </div>
                                </div>
                              <?php } ?>
                              
                              <label class="tx-12 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Lingkar Badan</label>
                              <input  name="lingkar_badan" class="form-control mg-b-20" placeholder="Lingkar Badan (cm)"  value="<?php  echo (!$therapist_performance) ? "" : $therapist_performance->lingkar_badan; ?>" type="number">
                              
                              <label class="tx-12 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Lingkar Perut</label>
                              <input  name="lingkar_perut" class="form-control mg-b-20" placeholder="Lingkar Perut (cm)"  value="<?php  echo (!$therapist_performance) ? "" : $therapist_performance->lingkar_perut; ?>" type="number">
                              
                              <label class="tx-12 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Lingkar Paha</label>
                              <input  name="lingkar_paha" class="form-control mg-b-20" placeholder="Lingkar Paha (cm)" value="<?php  echo (!$therapist_performance) ? "" : $therapist_performance->lingkar_paha; ?>" type="number">
                             
                            </div>
                            <div class="col-lg-6">

                              <label class="tx-12 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Lingkar Lengan</label>
                              <textarea name="lingkar_lengan" rows="3" class="form-control mg-b-20" placeholder="Lingkar Lengan"><?php  echo (!$therapist_performance) ? "" : $therapist_performance->lingkar_lengan; ?></textarea>
                              
                              <label class="tx-12 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Warna Kulit</label>
                              
                              <?php if(!$therapist_performance){ ?>
                                <div class="row">
                                  <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="warna_kulit" checked>
                                      <span>Kuning Langsat</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="2" name="warna_kulit">
                                      <span>Cokelat</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="3" name="warna_kulit">
                                      <span>Gelap</span>
                                    </label>
                                  </div>
                                </div>
                              <?php }else{ ?>
                                <div class="row">
                                  <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="warna_kulit" <?php  echo (($therapist_performance->warna_kulit==1) ? "checked" : ""); ?>>
                                      <span>Kuning Langsat</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="2" name="warna_kulit" <?php  echo (($therapist_performance->warna_kulit==2) ? "checked" : ""); ?>>
                                      <span>Cokelat</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="3" name="warna_kulit" <?php  echo (($therapist_performance->warna_kulit==3) ? "checked" : ""); ?>>
                                      <span>Gelap</span>
                                    </label>
                                  </div>
                                </div>

                              <?php } ?>

                              <label class="tx-12 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Kesehatan Rambut</label>
                              
                              <?php if(!$therapist_performance){ ?>
                                <div class="row">
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="kes_rambut" checked>
                                      <span>Sehat</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="2" name="kes_rambut">
                                      <span>Ketombe</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="3" name="kes_rambut">
                                      <span>Pecah</span>
                                    </label>
                                  </div>
                                </div>
                              <?php }else{ ?>
                                <div class="row">
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="kes_rambut" <?php  echo (($therapist_performance->kes_rambut==1) ? "checked" : ""); ?>>
                                      <span>Sehat</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="2" name="kes_rambut" <?php  echo (($therapist_performance->kes_rambut==2) ? "checked" : ""); ?>>
                                      <span>Ketombe</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="3" name="kes_rambut" <?php  echo (($therapist_performance->kes_rambut==3) ? "checked" : ""); ?>>
                                      <span>Pecah</span>
                                    </label>
                                  </div>
                                </div>
                              <?php } ?>

                              <label class="tx-12 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Penampilan Secara keseluruhan</label>
                              
                              <?php if(!$therapist_performance){ ?>
                                <div class="row">
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="penampilan" checked>
                                      <span>Ideal</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="penampilan">
                                      <span>OW</span>
                                    </label>
                                  </div>
                                </div>
                              <?php }else{ ?>
                                <div class="row">
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="penampilan" <?php  echo (($therapist_performance->penampilan==1) ? "checked" : ""); ?>>
                                      <span>Ideal</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-3 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="penampilan" <?php  echo (($therapist_performance->penampilan==0) ? "checked" : ""); ?>>
                                      <span>OW</span>
                                    </label>
                                  </div>
                                </div>
                              <?php } ?>

                            </div>
                          </div>
                        </div><!-- pd-30 -->
                      </div><!-- col-5 -->
                    </div><!-- row -->
                  </div>
                </div><!-- col-lg-8 -->
            </div><!-- row -->
          </div><!-- tab-pane -->

          <div class="tab-pane fade show" id="massage">
            <div class="row">
              <div class="col-lg-12">
                <div class="card ht-100p shadow-base widget-9">
                    <div class="row no-gutters">
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-t-25">
                          <h5 class="tx-inverse tx-normal tx-roboto">MASSAGE</h5>
                          <hr class="mg-b-5" />
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="pd-x-30 pd-b-25">

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Ketelatenan</h6>
                            <input  name="ketelatenan" class="form-control mg-b-20" placeholder="Ketelatenan" value="<?php  echo (!$therapist_massage) ? "" : $therapist_massage->ketelatenan; ?>" type="number">
                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Tenaga</h6>
                            <input  name="tenaga" class="form-control mg-b-20" placeholder="Tenaga" value="<?php  echo (!$therapist_massage) ? "" : $therapist_massage->tenaga; ?>" type="number">
                           </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Kelengkapan</h6>
                            <input  name="kelengkapan" class="form-control mg-b-20" placeholder="Kelengkapan" value="<?php  echo (!$therapist_massage) ? "" : $therapist_massage->kelengkapan; ?>" type="number">
                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Relaxasi</h6>
                            <input  name="relaxasi" class="form-control mg-b-20" placeholder="Relaxasi" value="<?php  echo (!$therapist_massage) ? "" : $therapist_massage->relaxasi; ?>" type="number">
                          </div>

                        </div><!-- pd-30 -->
                      </div><!-- col-6 -->

                      <div class="col-lg-6">
                        <div class="pd-x-30 pd-b-25">
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Hot Stone</h6>
                            <input  name="hotstone" class="form-control mg-b-20" placeholder="Hot Stone" value="<?php  echo (!$therapist_massage) ? "" : $therapist_massage->hotstone; ?>" type="number">
                          </div>

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Wet Massage</h6>
                            <input  name="wet_massage" class="form-control mg-b-20" placeholder="Wet Massage" value="<?php  echo (!$therapist_massage) ? "" : $therapist_massage->wet_massage; ?>" type="number">
                          </div>

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Ear Candle</h6>
                            <input  name="ear_candle" class="form-control mg-b-20" placeholder="Ear Candle" value="<?php  echo (!$therapist_massage) ? "" : $therapist_massage->ear_candle; ?>" type="number">
                          </div>

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Mud Therapy</h6>
                            <input  name="mud" class="form-control mg-b-20" placeholder="Mud Therapy" value="<?php  echo (!$therapist_massage) ? "" : $therapist_massage->mud; ?>" type="number">
                          </div>

                        </div><!-- pd-30 -->
                      </div><!-- col-6 -->

                    </div><!-- row -->
                  </div>
                </div><!-- col-lg-8 -->
            </div><!-- row -->
          </div><!-- tab-pane -->

          <div class="tab-pane fade show" id="services">
            <div class="row">
              <div class="col-lg-12">
                <div class="card ht-100p shadow-base widget-9">
                    <div class="row no-gutters">
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-t-25">
                          <h5 class="tx-inverse tx-normal tx-roboto">SERVICES</h5>
                          <hr class="mg-b-5" />
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="pd-x-30 pd-b-25">

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Attitude Ke Tamu</h6>
                            <?php if(!$therapist_services){ ?>
                              <div class="row">
                                <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                  <label class="rdiobox">
                                    <input type="radio" value="1" name="attitude_tamu" checked>
                                    <span>Menyenangkan</span>
                                  </label>
                                </div>
                                <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                  <label class="rdiobox">
                                    <input type="radio" value="0" name="attitude_tamu">
                                    <span>Jutek</span>
                                  </label>
                                </div>
                              </div>
                            <?php }else{ ?>
                              <div class="row">
                                <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                  <label class="rdiobox">
                                    <input type="radio" value="1" name="attitude_tamu" <?php  echo (($therapist_services->attitude_tamu==1) ? "checked" : ""); ?>>
                                    <span>Menyenangkan</span>
                                  </label>
                                </div>
                                <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                  <label class="rdiobox">
                                    <input type="radio" value="0" name="attitude_tamu" <?php  echo (($therapist_services->attitude_tamu==0) ? "checked" : ""); ?>>
                                    <span>Jutek</span>
                                  </label>
                                </div>
                              </div>
                            <?php } ?>
                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Attitude Ke Rekan Kerja</h6>
                            <?php if(!$therapist_services){ ?>
                              <div class="row">
                                <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                  <label class="rdiobox">
                                    <input type="radio" value="1" name="attitude_rekan" checked>
                                    <span>Bekerjasama</span>
                                  </label>
                                </div>
                                <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                  <label class="rdiobox">
                                    <input type="radio" value="0" name="attitude_rekan">
                                    <span>Cuek</span>
                                  </label>
                                </div>
                              </div>
                            <?php }else{ ?>
                              <div class="row">
                                <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                  <label class="rdiobox">
                                    <input type="radio" value="1" name="attitude_rekan" <?php  echo (($therapist_services->attitude_rekan==1) ? "checked" : ""); ?>>
                                    <span>Bekerjasama</span>
                                  </label>
                                </div>
                                <div class="col-lg-4 mg-b-20 mg-lg-t-0">
                                  <label class="rdiobox">
                                    <input type="radio" value="0" name="attitude_rekan" <?php  echo (($therapist_services->attitude_rekan==0) ? "checked" : ""); ?>>
                                    <span>Cuek</span>
                                  </label>
                                </div>
                              </div>
                            <?php } ?>
                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Meninggalkan Tamu</h6>
                            <input  name="men_tamu" class="form-control mg-b-20" placeholder="Meninggalkan Tamu" value="<?php  echo (!$therapist_services) ? "" : $therapist_services->men_tamu; ?>" type="number">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="pd-x-30 pd-b-25">
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Mengantarkan Tamu</h6>
                            <input  name="meng_tamu" class="form-control mg-b-20" placeholder="Mengantarkan Tamu" value="<?php  echo (!$therapist_services) ? "" : $therapist_services->meng_tamu; ?>" type="number">
                          </div>

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Menolak Tamu (Pemilih)</h6>
                            <input  name="menolak_tamu" class="form-control mg-b-20" placeholder="Menolak Tamu" value="<?php  echo (!$therapist_services) ? "" : $therapist_services->menolak_tamu; ?>" type="number">
                          </div>

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Di Cancel Tamu</h6>
                            <input  name="dicancel_tamu" class="form-control mg-b-20" placeholder="Di Cancel Tamu" value="<?php  echo (!$therapist_services) ? "" : $therapist_services->dicancel_tamu; ?>" type="number">
                          </div>

                        </div><!-- pd-30 -->
                      </div><!-- col-12 -->

                    </div><!-- row -->
                  </div>
                </div><!-- col-lg-8 -->
            </div><!-- row -->
          </div><!-- tab-pane -->

          <div class="tab-pane fade show" id="kedisiplinan">
            <div class="row">
              <div class="col-lg-12">
                <div class="card ht-100p shadow-base widget-9">
                    <div class="row no-gutters">
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-t-25">
                          <h5 class="tx-inverse tx-normal tx-roboto">KEDISIPLINAN</h5>
                          <hr class="mg-b-5" />
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-b-25">

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Total Masuk Kerja (1-30)</h6>
                            <input  name="total_kerja" class="form-control mg-b-20" placeholder="Total Masuk Kerja" value="<?php  echo (!$therapist_kedisiplinan) ? "" : $therapist_kedisiplinan->total_kerja; ?>" type="number">
                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Terlambat Masuk Kerja (1-30)</h6>
                            <input  name="terlambat_kerja" class="form-control mg-b-20" placeholder="Terlambat Masuk Kerja" value="<?php  echo (!$therapist_kedisiplinan) ? "" : $therapist_kedisiplinan->terlambat_kerja; ?>" type="number">
                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Sisa Waktu Sesi Massage (1-30)</h6>
                            <input  name="sisa_waktu_massage" class="form-control mg-b-20" placeholder="Sisa Waktu Sesi Massage" value="<?php  echo (!$therapist_kedisiplinan) ? "" : $therapist_kedisiplinan->sisa_waktu_massage; ?>" type="number">
                          </div>
                          
                        </div><!-- pd-30 -->
                      </div><!-- col-12 -->

                    </div><!-- row -->
                  </div>
                </div><!-- col-lg-8 -->
            </div><!-- row -->
          </div><!-- tab-pane -->

          <div class="tab-pane fade show" id="kesehatan">
            <div class="row">
              <div class="col-lg-12">
                <div class="card ht-100p shadow-base widget-9">
                    <div class="row no-gutters">
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-t-25">
                          <h5 class="tx-inverse tx-normal tx-roboto">KESEHATAN</h5>
                          <hr class="mg-b-5" />
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-b-25">

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Sakit Tidak Kerja (1-30)</h6>
                            <input  name="sakit_tidak_kerja" class="form-control mg-b-20" placeholder="Sakit Tidak Kerja" value="<?php  echo (!$therapist_kesehatan) ? "" : $therapist_kesehatan->sakit_tidak_kerja; ?>" type="number">
                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Sakit Tetap Kerja (1-30)</h6>
                            <input  name="sakit_tetap_kerja" class="form-control mg-b-20" placeholder="Sakit Tetap Kerja" value="<?php  echo (!$therapist_kesehatan) ? "" : $therapist_kesehatan->sakit_tetap_kerja; ?>" type="number">
                          </div>
                          
                        </div><!-- pd-30 -->
                      </div><!-- col-12 -->

                    </div><!-- row -->
                  </div>
                </div><!-- col-lg-8 -->
            </div><!-- row -->
          </div><!-- tab-pane -->

          <div class="tab-pane fade show" id="attitude">
            <div class="row">
              <div class="col-lg-12">
                <div class="card ht-100p shadow-base widget-9">
                    <div class="row no-gutters">
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-t-25">
                          <h5 class="tx-inverse tx-normal tx-roboto">ATTITUDE</h5>
                          <hr class="mg-b-5" />
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-b-25">

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Taat Peraturan Kerja</h6>
                            
                            <?php if(!$therapist_sikap){ ?>
                                <div class="row">
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="taat_peraturan" >
                                      <span>Ya</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="taat_peraturan" checked >
                                      <span>Tidak</span>
                                    </label>
                                  </div>
                                </div>
                              <?php }else{ ?>
                                <div class="row">
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="taat_peraturan" <?php  echo (($therapist_sikap->taat_peraturan==1) ? "checked" : ""); ?> >
                                      <span>Ya</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="taat_peraturan" <?php  echo (($therapist_sikap->taat_peraturan==0) ? "checked" : ""); ?> >
                                      <span>Tidak</span>
                                    </label>
                                  </div>
                                </div>
                              <?php } ?>
                          
                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Pembangkang</h6>
                            
                            <?php if(!$therapist_sikap){ ?>
                                <div class="row">
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="pembangkang" >
                                      <span>Ya</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="pembangkang" checked >
                                      <span>Tidak</span>
                                    </label>
                                  </div>
                                </div>
                              <?php }else{ ?>
                                <div class="row">
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="pembangkang" <?php  echo (($therapist_sikap->pembangkang==1) ? "checked" : ""); ?> >
                                      <span>Ya</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="pembangkang" <?php  echo (($therapist_sikap->pembangkang==0) ? "checked" : ""); ?> >
                                      <span>Tidak</span>
                                    </label>
                                  </div>
                                </div>
                              <?php } ?>

                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Provokator</h6>
                            
                            <?php if(!$therapist_sikap){ ?>
                                <div class="row">
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="provokator" >
                                      <span>Ya</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="provokator" checked >
                                      <span>Tidak</span>
                                    </label>
                                  </div>
                                </div>
                              <?php }else{ ?>
                                <div class="row">
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="provokator" <?php  echo (($therapist_sikap->provokator==1) ? "checked" : ""); ?> >
                                      <span>Ya</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="provokator" <?php  echo (($therapist_sikap->provokator==0) ? "checked" : ""); ?> >
                                      <span>Tidak</span>
                                    </label>
                                  </div>
                                </div>
                            <?php } ?>

                          </div>

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Penurut</h6>
                            
                            <?php if(!$therapist_sikap){ ?>
                                <div class="row">
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="penurut" >
                                      <span>Ya</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="penurut" checked >
                                      <span>Tidak</span>
                                    </label>
                                  </div>
                                </div>
                              <?php }else{ ?>
                                <div class="row">
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="1" name="penurut" <?php  echo (($therapist_sikap->penurut==1) ? "checked" : ""); ?> >
                                      <span>Ya</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="0" name="penurut" <?php  echo (($therapist_sikap->penurut==0) ? "checked" : ""); ?> >
                                      <span>Tidak</span>
                                    </label>
                                  </div>
                                </div>
                            <?php } ?>
                          
                          </div>
                          
                        </div><!-- pd-30 -->
                      </div><!-- col-12 -->

                    </div><!-- row -->
                  </div>
                </div><!-- col-lg-8 -->
            </div><!-- row -->
          </div><!-- tab-pane -->
          
          <div class="tab-pane fade show" id="grade">
            <div class="row">
              <div class="col-lg-12">
                <div class="card ht-100p shadow-base widget-9">
                    <div class="row no-gutters">
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-t-25">
                          <h5 class="tx-inverse tx-normal tx-roboto">GRADE</h5>
                          <hr class="mg-b-5" />
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="pd-x-30 pd-b-25">

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">3G</h6>
                            
                            <?php if(!$therapist_grade){ ?>
                                <div class="row">
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="A" name="grade" checked>
                                      <span>A</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="B" name="grade" >
                                      <span>B</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="C" name="grade" >
                                      <span>C</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-2 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="D" name="grade" >
                                      <span>D</span>
                                    </label>
                                  </div>
                                </div>
                              <?php }else{ ?>
                                <div class="row">
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="A" name="grade" <?php  echo (($therapist_grade->grade=='A') ? "checked" : ""); ?> >
                                      <span>A</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="B" name="grade" <?php  echo (($therapist_grade->grade=='B') ? "checked" : ""); ?> >
                                      <span>B</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="C" name="grade" <?php  echo (($therapist_grade->grade=='C') ? "checked" : ""); ?> >
                                      <span>C</span>
                                    </label>
                                  </div>
                                  <div class="col-lg-1 mg-b-20 mg-lg-t-0">
                                    <label class="rdiobox">
                                      <input type="radio" value="D" name="grade" <?php  echo (($therapist_grade->grade=='D') ? "checked" : ""); ?> >
                                      <span>D</span>
                                    </label>
                                  </div>
                                </div>
                            <?php } ?>

                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Bad Comment</h6>
                            <input  name="bad_comment" class="form-control mg-b-20" placeholder="Bad Comment" value="<?php  echo (!$therapist_grade) ? "" : $therapist_grade->bad_comment; ?>" type="number">
                          </div>
                          
                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">TS</h6>
                            <input  name="ts" class="form-control mg-b-20" placeholder="TS" value="<?php  echo (!$therapist_grade) ? "" : $therapist_grade->ts; ?>" type="number">
                          </div>

                          <div class="mg-r-20">
                            <h6 class="tx-gray-800 tx-uppercase tx-mont tx-semibold tx-13 mg-t-30 mg-b-10">Handle Tamu</h6>
                            <input  name="handle_tamu" class="form-control mg-b-20" placeholder="TS" value="<?php  echo (!$therapist_grade) ? "" : $therapist_grade->handle_tamu; ?>" type="number">
                          </div>

                        </div><!-- pd-30 -->
                      </div><!-- col-12 -->

                    </div><!-- row -->
                  </div>
                </div><!-- col-lg-8 -->
            </div><!-- row -->
          </div><!-- tab-pane -->

          <div class="tab-pane fade" id="photos">
            <div class="row">
              <div class="col-lg-12">
                <div class="card pd-20 pd-xs-30 shadow-base bd-0 mg-t-30">
                  <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-14 mg-b-30">Upload Photos</h6>
                  
                  <div class="row row-xs">
                    <div class="col-6 col-sm-6 col-md-6 mg-b-10">
                      <div class="form-group">
                          <div class="controls">
                              
                            <div class="entry input-group">
                              <input class="form-control" name="therapist_image" type="file">
                            </div>
                          </div>
                          <br>
                          <!-- <input type="submit" class="btn btn-primary" value="UPLOAD"> -->
                        </div>
                    </div>
                  </div>

                  <div class="row row-xs">
                    
                    <?php if($therapist_photo){ ?>
                      <?php foreach($therapist_photo as $photo): ?>
                        <div class="col-6 col-sm-4 col-md-3 mg-b-10"><img src="<?php echo base_url() . $photo->source; ?>" class="img-fluid" alt=""></div>
                      <?php endforeach; ?>
                    <?php } ?>

                  </div><!-- row -->

                </div><!-- card -->
              </div><!-- col-lg-12 -->
            </div><!-- row -->
          </div><!-- tab-pane -->
        </div><!-- br-pagebody -->
        
      <div class="br-section-wrapper pd-30">

        <div class="row">

            <div class="col-xl-12 mg-t-0 mg-b-0">

              <?php echo $this->session->flashdata('msg'); ?>

              <div class="form-layout-footer pull-right">
                <a href="<?php echo base_url('penilaian/update_therapist'); ?>" class="btn btn-secondary">Cancel</a>
                <button class="btn btn-info" name="flag" value="update">Submit</button>
              </div><!-- form-layout-footer -->

            </div>

          </div>
        </div>

      </div>
  </form>

</div>