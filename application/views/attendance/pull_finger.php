<div class="content-body">

  <!-- Basic Select2 start -->
  <section class="basic-select2">
    <div class="row">
      <div class="col-xl-12 col-lg-12">
        <div class="card">

            <div class="card-content pt-1 collapse show">
              <div class="card-body">
                <div class="form-group">

                  <div class="col-xl-8 offset-xl-2 mt-0 mb-0">
                    <div class="form-layout form-layout-5  border-0 pd-y-0">
                      <form id="form" role="form" enctype="multipart/form-data" action="<?php echo base_url() ?>attendance/pull_attendance_log" method="post" class="form-horizontal form-groups-bordered">                            
                        <div class="row mt-1 mb-1">
                          <label class="col-sm-4 text-right label-control"><strong> Select Branch:</strong></label>
                          <div class="col-sm-7 mg-t-10 mg-sm-t-0">
                            <select class="form-control select2" name="branch_id" data-placeholder="Choose Branch">
                              <option value="0">- Choose Branch -</option>
                              <?php foreach ($branchs as $branch){ ?> 
                                <option value="<?php echo $branch->branch_code; ?>"
                                    <?php
                                    if (!empty($branch)) {
                                        echo $branch->branch_code == $branch_id ? 'selected' : '';
                                    }
                                    ?>><?php echo $branch->branch_name ?>
                                </option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>

                        <div class="row mt-1">
                          <label class="col-sm-4 text-right form-control-label"><strong></strong></label>                              
                          <div class="col-sm-7 ml-0">
                            <div class="form-layout">
                              <button class="btn btn-primary" name="flag" value="1">Submit</button>
                            </div>
                          </div>
                        </div>

                      </form>
                    </div><!-- form-layout -->
                  </div>
                
                </div>
              </div>
            </div>

            <div class="card-content pt-1 collapse show">
              <div class="card-body">
                    
                <div class="msg" style="">
                  <?php echo $this->session->flashdata('msg'); ?>
                </div>

              </div>
            </div>

        </div>
      </div>
    </div>
  </section>
  <!-- Basic Select2 end -->
  
  
  <!-- `new` constructor table -->
  <section id="constructor">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                <li><a data-action="close"><i class="ft-x"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="card-content collapse show">
            <div class="card-body card-dashboard">
              <table class="table table-striped table-bordered dataex-res-constructor">
                <thead>
                  <tr>
                    <th>ID #</th>
                    <th>Date</th>
                    <th>Status (In/Out)</th>
                    <th>Branch</th>
                  </tr>
                </thead>
                <tbody>
                    
                  <?php if (!empty($flag)): ?>
                    <?php foreach ($attendance as $att): ?>
                      <tr>
                        <td><?php echo $att->userid; ?></td>
                        <td><?php echo $att->datetime; ?></td>
                        <td><?php echo $att->status; ?></td>
                        <td><?php echo $att->branch_id; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- `new` constructor table -->

</div>