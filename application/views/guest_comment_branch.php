<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Dari Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Sampai Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<?php if(@$_GET['branch_code']!==null){ ?>
					<input type="hidden" name="branch_id" id="branch_id" value="<?php echo $_GET['branch_code']; ?>">
				<?php }else{ ?>
				<?php //var_dump($data['userdata']->level); die(); ?>
					<?php if($data['userdata']->level !== '5'){ ?>
						<div class="col-md-3">
							<div class="form-group">
								<label>Outlet :</label>

								<div class="">
									<select class="form-control pull-right" name="branch_id" id="branch_id">
										<?php $branchs = $this->general->getBranchList(); ?>
										<option value="">- Pilih Outlet -</option>
										<?php foreach ($branchs as $branch){ ?>
											<option value="<?php echo $branch->csname; ?>"><?php echo $branch->cname; ?></option>
										<?php } ?>
									</select>
								</div>
								<!-- /.input group -->
							</div>
						</div>
					<?php } ?>
				<?php } ?>

				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_guest_comment" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
				
				<?php if($this->userdata->level != '5'){ ?>
					<div class="col-md-3">
						<div class="form-group pull-right">
							<label>&nbsp;</label>
							<div class="input-group">
								<a href="<?php echo base_url(); ?>guest_comment/branch_list" class="btn btn-md btn-primary" style="margin-bottom: 10px;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Change Branch</a>
							</div>
						</div>
					</div>
				<?php } ?>

			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="guest_comment" class="tabel-report table  table-striped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>T. Tamu</th>
								<th>TS Req</th>
								<th>% Req</th>
								<th>Nilai Req</th>
								<th>Point Ratio</th>
								<th>Nilai Ratio</th>
								<th>GC</th>
								<th>% GC</th>
								<th>Nilai GC</th>								
								<th>Hasil</th>
                                <th>Grade</th>
                                <th>Perform</th>
								<th>Massage</th>
								<th>Service</th>
								<th>Jml GC</th>
							</tr>
						</thead>
						<tbody id="data_guest_comment">

						</tbody>
					</table>
				</div>
				<a href="" id="print_guest_comment" class="btn btn-md btn-warning pull-right" style="margin-bottom: 10px;"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Cetak</a>
			</div>			
		</div>
	</div>
	
</div>