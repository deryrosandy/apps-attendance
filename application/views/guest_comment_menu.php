<?php if($userdata->level == '1' || $userdata->level == '3'){ ?>

    <div class="row  gridlist-dashboard">
        
        <div class="col-lg-3 col-sm-3 col-xs-6" style="margin-bottom: 15px;">
            <div clas="small-box"  title="Guest Comment">
                <a href="<?php base_url(); ?>guest_comment/guest_comment_filter" class="btn btn-block  btn-lg btn-primary">
                    <h5 class="text-bold word-break">GUEST COMMENT</h4>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-6" style="margin-bottom: 15px;">
            <div clas="small-box"  title="Grade Therapist">
                <a href="<?php base_url(); ?>guest_comment/branch_list" class="btn btn-block  btn-lg btn-primary">
                    <h5 class="text-bold word-break">GRADE THERAPIST</h4>
                </a>
            </div>
        </div>

    </div>

<?php }else{ ?>

    <div class="row  gridlist-dashboard">
        
        <div class="col-lg-3 col-sm-3 col-xs-6" style="margin-bottom: 15px;">
            <div clas="small-box"  title="Guest Comment">
                <a href="<?php base_url(); ?>guest_comment/select_branch_filter?branch_code=<?php echo $branch_id; ?>" class="btn btn-block  btn-lg btn-primary">
                    <h5 class="text-bold word-break">GUEST COMMENT</h4>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-6" style="margin-bottom: 15px;">
            <div clas="small-box"  title="Guest Comment">
                <a href="<?php echo base_url(); ?>guest_comment/select_branch?branch_code=<?php echo $branch_id; ?>" class="btn btn-block  btn-lg btn-primary">
                    <h5 class="text-bold word-break">GRADE THERAPIST</h4>
                </a>
            </div>
        </div>

    </div>

<?php } ?>