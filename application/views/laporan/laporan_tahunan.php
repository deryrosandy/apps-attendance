<div class="br-pagebody">
  
  <div class="br-section-wrapper pd-30">

    <div class="row">

      <div class="col-xl-8 offset-xl-2 mg-t-0 mg-b-0">
        <div class="form-layout form-layout-5  border-0 pd-y-0">
          <form id="form" role="form" enctype="multipart/form-data" action="<?php echo base_url() ?>laporan/laporan_tahunan" method="post" class="form-horizontal form-groups-bordered">
            <?php if(($this->session->userdata('userdata')->user_type) =='admin') { ?>
              <div class="row mg-t-10">
                <label class="col-sm-4 form-control-label"><strong> Select Branch:</strong></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <select class="form-control select2" name="branch_id" data-placeholder="Choose Branch">
                  <option value="0">- Choose Branch -</option>
                  <?php foreach ($branchs as $branch){ ?> 
                    <option value="<?php echo $branch->id; ?>"
                        <?php
                        if (!empty($branch_id)) {
                            echo $branch->id == $branch_id ? 'selected' : '';
                        }
                        ?>><?php echo $branch->name ?>
                    </option>
                  <?php } ?>
                </select>
                </div>
              </div>
            <?php }else{ ?>
              <input type="hidden" value="<?php echo $this->session->userdata('userdata')->branch_id; ?>" name="branch_id">
            <?php } ?>
            
            <div class="row mg-t-10">
              <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span><strong> Select Year:</strong></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="text" value="<?php
                      if (!empty($year)) {
                          echo $year;
                      }
                      ?>" class="form-control form-control-1 input-sm formatyears" placeholder="YYYY" name="year">
              </div>
            </div>
            <div class="row mg-t-10">
              <div class="col-sm-8 mg-l-auto">
                <div class="form-layout">
                  <button class="btn btn-info btn-sm" name="flag" value="1">Submit</button>
                </div><!-- form-layout-footer -->
              </div><!-- col-8 -->
            </div>
          </form>
        </div><!-- form-layout -->
      </div>
    
    </div>
  </div>
</div>
<div class="br-pagebody">
  
  <div class="br-section-wrapper pd-20">

    <?php if (!empty($flag)): ?>
            
      <div class="row">
          
        <div class="col-xl-12 mg-t-0 mg-b-0">
          <div class="form-layout form-layout-5  border-0 pd-0">
            <h4 class="tx-inverse tx-normal tx-roboto mg-b-20">LAPORAN TAHUNAN THERAPIST</h4>
            <div class="table-wrapper">
              <table id="datatable1" class="table display responsive nowrap">
                <thead>
                  <tr>
                    <th class="wd-5p">No.</th>
                    <th class="wd-5p">NIT</th>
                    <th class="wd-15p">Nama Lengkap</th>
                    <th class="wd-20p">Level</th>
                    <th class="wd-15p">Supplier</th>
                    <th class="wd-10p">Kontrak Ke</th>
                    <th class="wd-10p">Akhir Kontrak</th>
                    <th class="wd-10p">Status</th>
                    <th class="wd-25p">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  <?php foreach ($therapists as $therapist): ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $therapist->nit; ?></td>
                      <td><?php echo $therapist->fullname; ?></td>
                      <td><?php echo get_level_therapist_short($therapist->level); ?></td>
                      <td><?php echo get_supplier_name($therapist->supplier_id); ?></td>
                      <td><?php echo $therapist->kontrak_ke; ?></td>
                      <td><?php echo tgl_indo2($therapist->akhir_kontrak); ?></td>
                      <td><?php echo get_status_therapist($therapist->status); ?></td>
                      <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                          <a href="<?php echo base_url(); ?>laporan/laporan_tahunan/<?php echo $therapist->therapist_id; ?>/<?php echo $year; ?>" target="_blank" data-toggle="tooltip-danger" data-placement="top" title="Print" class="badge  badge-primary pd-x-10 pd-y-5 tx-white tx-11 tx-roboto"><i class="fa fa-print"></i></a>
                        </div>
                      </td>
                    </tr>
                    <?php $no++; ?>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div><!-- table-wrapper -->
          </div><!-- form-layout -->
        </div>
      
      </div>

    <?php endif; ?>

  </div>
</div>