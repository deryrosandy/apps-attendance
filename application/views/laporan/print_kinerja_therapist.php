<style type="text/css">
	html { margin: 15px}
	.tg  {
		width: 100%;
		border-collapse:collapse;
		border-spacing:0;
		margin-bottom: 10px;
	}
	.tg1  {
		width: 400px;
		border-collapse:collapse;
		border-spacing:0;
		float: left;
		display: inline-table;
		margin-bottom:5px;
	}
	.tg td{font-family:Arial, sans-serif;font-size:7px;padding:1px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg th{font-family:Arial, sans-serif;font-size:7px;font-weight:700;border-style:solid;padding:1px 3px;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg .tg-yw4l{vertical-align:middle}
	.tg1 td{font-family:Arial, sans-serif;font-size:7px;padding:1px 3px;border-style:solid;border-width:0;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 th{font-family:Arial, sans-serif;font-size:7px;font-weight:700;padding:1px 3px;border-style:solid;border-width:0;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 .tg-yw4l{vertical-align:middle}
	.tfoot {font-weight:700}
	.tc {text-align:center}
	.tl {text-align:left}
	.tr {text-align:right}
	.tg .tg-yw4l.tt {vertical-align:top}
	h4 {font-size:12px;padding-top:0;margin-top:0;margin-bottom:5px;}
	.subject_footer {
		font-family:Arial, sans-serif;
		position: relative;
		left: 0;
		bottom: 115px;
		font-size: 12px;
		font-weight: 500;
	}
	.subject_footer div {
		font-family:Arial, sans-serif;
		width: 120px;
		float: left;
		margin: 0 60px;
		font-weight: 500;
	}
	.subject_footer .name {
		font-family:Arial, sans-serif;
		margin-top: 50px;
		font-weight: 500;
	}
</style>

<h4 class="tc">ANEV YEARLY REPORT <br/><?php echo $year; ?></h4>

<table width="100%" class="tg1">
	<tr>
		<td width="30" class="tg-yw4l tl">Nama</td>
		<td width="70" class="tg-yw4l tl">:   <?php echo $therapist->fullname; ?></td>	
	</tr>
	<tr>
		<td width="30" class="tg-yw4l tl">NIT</td>
		<td width="30" class="tg-yw4l tl">:   <?php echo $therapist->nit; ?></td>	
	</tr>
	<tr>
		<td width="30" class="tg-yw4l tl">Tgl Lahir</td>
		<td width="30" class="tg-yw4l tl">:   <?php echo tgl_indo2($therapist->tgl_lahir); ?></td>	
	</tr>
	<tr>
		<td width="30" class="tg-yw4l tl">Tinggi Badan</td>
		<td width="30" class="tg-yw4l tl">:   <?php echo $therapist->tinggi_badan; ?> cm</td>	
	</tr>
	<tr>
		<td width="30" class="tg-yw4l tl">Wajah</td>
		<td width="30" class="tg-yw4l tl">:   <?php echo $therapist->wajah; ?></td>	
	</tr>
	<tr>
		<td width="30" class="tg-yw4l tl">Kontrak Ke</td>
		<td width="30" class="tg-yw4l tl">:   <?php echo $therapist->kontrak_ke; ?></td>	
	</tr>
	<tr>
		<td width="30" class="tg-yw4l tl">Akhir Kontrak</td>
		<td width="30" class="tg-yw4l tl">:   <?php echo tgl_indo2($therapist->akhir_kontrak); ?></td>	
	</tr>
	<tr>
		<td width="30" class="tg-yw4l tl">Status</td>
		<td width="30" class="tg-yw4l tl">:   <?php echo $therapist->status; ?></td>	
	</tr>
</table>

<table width="100%" class="tg1">
	<tr>
		<td width="30" class="tg-yw4l tl">HISTORY ROTASI</td>
		<td width="70" class="tg-yw4l tl">: </td>	
	</tr>
	<tr>
		<td width="30" class="tg-yw4l tl">SUPPLIER</td>
		<td width="30" class="tg-yw4l tl">:   <?php echo get_supplier_name($therapist->supplier_id); ?></td>	
	</tr>
	<tr>
		<td width="30" class="tg-yw4l tl">RIWAYAT PENYAKIT</td>
		<td width="30" class="tg-yw4l tl">:   <?php echo $therapist->riwayat_penyakit; ?></td>	
	</tr>
</table>

<div style="clear:both;"></div>

<table width="100%" class="tg">
    <tr>
        <th rowspan="2" width="12" class="tg-yw4l tc">NO.</th>
        <th rowspan="2" width="80" class="tg-yw4l tc">KPI</th>
        <th colspan="12" width="40" class="tg-yw4l tc">BULAN</th>
        <th rowspan="2" width="30" class="tg-yw4l tc">TOTAL POINT</th>
    </tr>
	
    <tr>
		<?php foreach($therapist_massage as $massage){ ?>
			<th width="30" class="tg-yw4l tc"><?php echo get_month_name($massage->month); ?></th>
		<?php } ?>	
	</tr>

	<tr>
		<td rowspan="9" class="tg-yw4l tc tt">1</td>
		<td colspan="14" bgcolor="#f4bf42" class="tg-yw4l tl">MASSAGE</td>		
	</tr>

	<tr>
		<td class="tg-yw4l tl">- KETELATENAN</td>	

		<?php foreach($therapist_massage as $massage){ ?>
			<td class="tg-yw4l tc"><?php echo $massage->ketelatenan; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- TENAGA</td>	

		<?php foreach($therapist_massage as $massage){ ?>
			<td class="tg-yw4l tc"><?php echo $massage->tenaga; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- KELENGKAPAN</td>	

		<?php foreach($therapist_massage as $massage){ ?>
			<td class="tg-yw4l tc"><?php echo $massage->kelengkapan; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- RELAXASI</td>	

		<?php foreach($therapist_massage as $massage){ ?>
			<td class="tg-yw4l tc"><?php echo $massage->relaxasi; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- HOTSTONE</td>	

		<?php foreach($therapist_massage as $massage){ ?>
			<td class="tg-yw4l tc"><?php echo $massage->hotstone; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- WET MASSAGE</td>	

		<?php foreach($therapist_massage as $massage){ ?>
			<td class="tg-yw4l tc"><?php echo $massage->wet_massage; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- EAR CANDLE</td>	

		<?php foreach($therapist_massage as $massage){ ?>
			<td class="tg-yw4l tc"><?php echo $massage->ear_candle; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- MUD THERAPI</td>	

		<?php foreach($therapist_massage as $massage){ ?>
			<td class="tg-yw4l tc"><?php echo $massage->mud; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td rowspan="5" class="tg-yw4l tc tt">2</td>
		<td colspan="14" bgcolor="#f4bf42" class="tg-yw4l tl">GRADE</td>		
	</tr>

	<tr>
		<td class="tg-yw4l tl">- GRADE</td>	

		<?php foreach($therapist_grade as $grade){ ?>
			<td class="tg-yw4l tc"><?php echo $grade->grade; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- BAD COMMENT</td>	

		<?php foreach($therapist_grade as $grade){ ?>
			<td class="tg-yw4l tc"><?php echo $grade->bad_comment; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- HANDLE TAMU</td>	

		<?php foreach($therapist_grade as $grade){ ?>
			<td class="tg-yw4l tc"><?php echo $grade->handle_tamu; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- TS</td>	

		<?php foreach($therapist_grade as $grade){ ?>
			<td class="tg-yw4l tc"><?php echo $grade->ts; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td rowspan="10" class="tg-yw4l tc tt">3</td>
		<td colspan="14" bgcolor="#f4bf42" class="tg-yw4l tl">PERFORMANCE</td>		
	</tr>

	<tr>
		<td class="tg-yw4l tl">- BERAT BADAN</td>	

		<?php foreach($therapist_performance as $performance){ ?>
			<td class="tg-yw4l tc"><?php echo $performance->berat_badan; ?> kg</td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- JERAWAT</td>	

		<?php foreach($therapist_performance as $performance){ ?>
			<td class="tg-yw4l tc"><?php echo get_status_yesno($performance->jerawat); ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- LINGKAR BADAN</td>	

		<?php foreach($therapist_performance as $performance){ ?>
			<td class="tg-yw4l tc"><?php echo $performance->lingkar_badan; ?> cm</td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- LINGKAR PERUT</td>	

		<?php foreach($therapist_performance as $performance){ ?>
			<td class="tg-yw4l tc"><?php echo $performance->lingkar_perut; ?> cm</td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- LINGKAR PAHA</td>	

		<?php foreach($therapist_performance as $performance){ ?>
			<td class="tg-yw4l tc"><?php echo $performance->lingkar_paha; ?> cm</td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- LINGKAR LENGAN</td>	

		<?php foreach($therapist_performance as $performance){ ?>
			<td class="tg-yw4l tc"><?php echo $performance->lingkar_lengan; ?> cm</td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- WARNA KULIT</td>	

		<?php foreach($therapist_performance as $performance){ ?>
			<td class="tg-yw4l tc"><?php echo get_status_kulit($performance->warna_kulit); ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- KESEHATAN RAMBUT</td>	

		<?php foreach($therapist_performance as $performance){ ?>
			<td class="tg-yw4l tc"><?php echo get_status_rambut($performance->kes_rambut); ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- PENAMPILAN KESELURUHAN</td>	

		<?php foreach($therapist_performance as $performance){ ?>
			<td class="tg-yw4l tc"><?php echo get_status_penampilan($performance->penampilan); ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td rowspan="7" class="tg-yw4l tc tt">4</td>
		<td colspan="14" bgcolor="#f4bf42" class="tg-yw4l tl">SERVICES</td>		
	</tr>
	
	<tr>
		<td class="tg-yw4l tl">- ATTITUDE KE TAMU</td>	

		<?php foreach($therapist_services as $services){ ?>
			<td class="tg-yw4l tc"><?php echo get_status_ketamu($services->attitude_tamu); ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>
	
	<tr>
		<td class="tg-yw4l tl">- ATTITUDE KE REKAN KERJA</td>	

		<?php foreach($therapist_services as $services){ ?>
			<td class="tg-yw4l tc"><?php echo get_status_ketamu($services->attitude_rekan); ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- MENINGGALAN TAMU</td>	

		<?php foreach($therapist_services as $services){ ?>
			<td class="tg-yw4l tc"><?php echo $services->men_tamu; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- MENGANTARKAN TAMU</td>	

		<?php foreach($therapist_services as $services){ ?>
			<td class="tg-yw4l tc"><?php echo $services->meng_tamu; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- MENOLAK TAMU (PEMILIH)</td>	

		<?php foreach($therapist_services as $services){ ?>
			<td class="tg-yw4l tc"><?php echo $services->menolak_tamu; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- DI CANCEL TAMU</td>	

		<?php foreach($therapist_services as $services){ ?>
			<td class="tg-yw4l tc"><?php echo $services->dicancel_tamu; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td rowspan="4" class="tg-yw4l tc tt">5</td>
		<td colspan="14" bgcolor="#f4bf42" class="tg-yw4l tl">KEDISIPLINAN</td>		
	</tr>

	<tr>
		<td class="tg-yw4l tl">- TOTAL MASUK KERJA</td>	

		<?php foreach($therapist_kedisiplinan as $kedisiplinan){ ?>
			<td class="tg-yw4l tc"><?php echo $kedisiplinan->total_kerja; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- TERLAMBAT MASUK KERJA</td>	

		<?php foreach($therapist_kedisiplinan as $kedisiplinan){ ?>
			<td class="tg-yw4l tc"><?php echo $kedisiplinan->terlambat_kerja; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- SISA WAKTU SESI MASSAGE</td>	

		<?php foreach($therapist_kedisiplinan as $kedisiplinan){ ?>
			<td class="tg-yw4l tc"><?php echo $kedisiplinan->sisa_waktu_massage; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td rowspan="3" class="tg-yw4l tc tt">6</td>
		<td colspan="14" bgcolor="#f4bf42" class="tg-yw4l tl">KESEHATAN</td>		
	</tr>

  <tr>
		<td class="tg-yw4l tl">- SAKIT TIDAK KERJA</td>	

		<?php foreach($therapist_kesehatan as $kesehatan){ ?>
			<td class="tg-yw4l tc"><?php echo $kesehatan->sakit_tidak_kerja; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

  <tr>
		<td class="tg-yw4l tl">- SAKIT TETAP KERJA</td>	

		<?php foreach($therapist_kesehatan as $kesehatan){ ?>
			<td class="tg-yw4l tc"><?php echo $kesehatan->sakit_tetap_kerja; ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td rowspan="5" class="tg-yw4l tc tt">7</td>
		<td colspan="14" bgcolor="#f4bf42" class="tg-yw4l tl">SIKAP / ATTITUDE</td>		
	</tr>

  <tr>
		<td class="tg-yw4l tl">- TAAT PERATURAN</td>	

		<?php foreach($therapist_sikap as $sikap){ ?>
			<td class="tg-yw4l tc"><?php echo get_status_yesno($sikap->taat_peraturan); ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- PEMBANGKANG</td>	

		<?php foreach($therapist_sikap as $sikap){ ?>
			<td class="tg-yw4l tc"><?php echo get_status_yesno($sikap->pembangkang); ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

	<tr>
		<td class="tg-yw4l tl">- PROVOKATOR</td>	

		<?php foreach($therapist_sikap as $sikap){ ?>
			<td class="tg-yw4l tc"><?php echo get_status_yesno($sikap->provokator); ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

  <tr>
		<td class="tg-yw4l tl">- PENURUT</td>	

		<?php foreach($therapist_sikap as $sikap){ ?>
			<td class="tg-yw4l tc"><?php echo get_status_yesno($sikap->penurut); ?></td>		
		<?php } ?>

		<td class="tg-yw4l tc"></td>
	</tr>

    <tr>
		<td class="tg-yw4l tc tt">8</td>
		<td class="tg-yw4l tl">FOTO</td>	

		<?php foreach($therapist_photo as $photo){ ?>
		<?php //var_dump($photo->source); ?>
			<td class="tg-yw4l tc" style="padding:0;">
				<img src="<?php echo ($photo->source); ?>" width="45"  style="margin:0;" class=""/>
			</td>		
		<?php } ?>	

		<td class="tg-yw4l tc"></td>

	</tr>

  	<tr>
		<td colspan="2" class="tg-yw4l tc">TOTAL POINT</td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
		<td class="tg-yw4l tr"></td>
	</tr>

</table>

</div>