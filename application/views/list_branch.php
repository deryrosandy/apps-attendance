<div class="row  gridlist-dashboard">
	
	<?php foreach($list_branch as $branch){ ?>
		<div class="col-lg-3 col-sm-3 col-xs-6" style="margin-bottom: 15px;">
			<div clas="small-box"  title="<?php echo $branch->cname; ?>">
				<a href="<?php base_url(); ?>report/report_category_branch?branch_code=<?php echo $branch->csname; ?>" class="btn btn-block  btn-lg btn-primary">
					<h5 class="text-bold word-break"><?php echo $branch->cname; ?></h4>
				</a>
			</div>
		</div>
	<?php } ?>

</div>