<div class="content-body">

  <section id="row-separator-form-layouts">
    <div class="row">
      <div class="col-md-9">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title" id="row-separator-basic-form">Tambah Penerimaan</h4>
            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                <li><a data-action="close"><i class="ft-x"></i></a></li>
              </ul>
            </div>
          </div>

          <div class="card-content collapse show">
            <div class="card-body">
              <form class="form form-horizontal row-separator">
                <div class="form-body">
                  <h4 class="form-section"><i class="la la-clipboard"></i> Requirements</h4>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="projectinput5">Company</label>
                    <div class="col-md-9">
                      <input type="text" id="projectinput5" class="form-control" placeholder="Company Name" name="company">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="projectinput6">Interested in</label>
                    <div class="col-md-9">
                      <select id="projectinput6" name="interested" class="form-control">
                        <option value="none" selected="" disabled="">Interested in</option>
                        <option value="design">design</option>
                        <option value="development">development</option>
                        <option value="illustration">illustration</option>
                        <option value="branding">branding</option>
                        <option value="video">video</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="projectinput7">Budget</label>
                    <div class="col-md-9">
                      <select id="projectinput7" name="budget" class="form-control">
                        <option value="0" selected="" disabled="">Budget</option>
                        <option value="less than 5000$">less than 5000$</option>
                        <option value="5000$ - 10000$">5000$ - 10000$</option>
                        <option value="10000$ - 20000$">10000$ - 20000$</option>
                        <option value="more than 20000$">more than 20000$</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control">Select File</label>
                    <div class="col-md-9">
                      <label id="projectinput8" class="file center-block">
                        <input type="file" id="file">
                        <span class="file-custom"></span>
                      </label>
                    </div>
                  </div>
                  <div class="form-group row last">
                    <label class="col-md-3 label-control" for="projectinput9">About Project</label>
                    <div class="col-md-9">
                      <textarea id="projectinput9" rows="5" class="form-control" name="comment" placeholder="About Project"></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-actions">
                  <button type="button" class="btn btn-warning mr-1">
                    <i class="la la-remove"></i> Cancel
                  </button>
                  <button type="submit" class="btn btn-primary">
                    <i class="la la-check"></i> Save
                  </button>
                </div>
              </form>
            </div>
          </div>

        </div>
      </div>

      <div class="col-md-3">
        <div class="card alert bg-success ">
            <div class="card-header">
              <h4 class="card-title">Project Overview</h4>
              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="close"><i class="ft-x"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="card-content">
              <div class="card-body">
                <p>
                  <strong>Pellentesque habitant morbi tristique</strong> senectus et netus
                  et malesuada fames ac turpis egestas. Vestibulum tortor quam,
                  feugiat vitae.
                  <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend
                  leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum
                  erat wisi, condimentum sed, <code>commodo vitae</code>, ornare
                  sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum,
                  eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.
                  <a href="#">Donec non enim</a>.</p>
                <p>
                  <strong>Lorem ipsum dolor sit</strong>
                </p>
                <ol>
                  <li>Consectetuer adipiscing</li>
                  <li>Aliquam tincidunt mauris</li>
                  <li>Consectetur adipiscing</li>
                  <li>Vivamus pretium ornare</li>
                  <li>Curabitur massa</li>
                </ol>
              </div>
            </div>
          </div>
      </div>

    </div>
  </section>

</div>