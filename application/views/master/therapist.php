<div class="msg" style="">
  <?php echo $this->session->flashdata('msg'); ?>
</div>

<div class="br-pagebody">
  
  <div class="br-section-wrapper pd-20">
            
      <div class="row">
          
        <div class="col-xl-12 mg-t-0 mg-b-0">
          <div class="form-layout form-layout-5  border-0 pd-0">
            <div class="d-flex align-items-center justify-content-between">
              <h4 class="tx-inverse tx-normal tx-roboto mg-b-20">Data Therapist</h4>
              <button class="btn btn-info btn-sm tx-roboto tx-normal"><i class="fa fa-plus"></i> Add Therapist</button>
            </div>
            <div class="table-wrapper">
              <table id="datatables_therapist" class="table display responsive nowrap">
                <thead>
                  <tr>
                    <th class="wd-5p">No.</th>
                    <th class="wd-15p">Nama</th>
                    <th class="wd-10p">NIT</th>
                    <th class="wd-10p">Level</th>
                    <th class="wd-10p">Tgl Lahir</th>
                    <th class="wd-10p">No. Telp</th>

                    <?php if(($this->session->userdata('userdata')->user_type) =='superadmin'){ ?>
                      <th class="wd-10p">Akhir Kontrak</th>
                    <?php } ?>

                    <th class="wd-10p">Supplier</th>
                    <th class="wd-10p">Status</th>
                    <th class="wd-10p">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  <?php foreach ($therapists as $therapist): ?>
                    <tr>
                    <td><?php echo $no; ?></td>
                      <td><?php echo $therapist->fullname; ?></td>
                      <td><?php echo $therapist->nit; ?></td>
											<td><?php echo get_level_therapist_short($therapist->level); ?></td>
											<td><?php echo tgl_indo2($therapist->tgl_lahir); ?></td>                   
                      <td><?php echo $therapist->phone_number; ?></td> 
                      
                      <?php if(($this->session->userdata('userdata')->user_type) =='superadmin'){ ?>
                        <td><?php echo tgl_indo2($therapist->akhir_kontrak); ?></td>
                      <?php } ?>

                      <td><?php echo get_supplier_name($therapist->supplier_id); ?></td>
                      <td><?php echo get_status_therapist($therapist->status); ?></td>
                      <td>
                        <button data-toggle="modal" data-target="#modal_therapist" data-id="<?php echo $therapist->therapist_id; ?>" data-toggle="tooltip-danger" data-placement="top"  title="Edit Therapist" class="btn btn-info btn-sm">Edit</button>
                      </td>
                    </tr>
                    <?php $no++; ?>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div><!-- table-wrapper -->
          </div><!-- form-layout -->
        </div>
      
      </div>

  </div>
</div>

<div id="modal_mutasi" class="modal fade" aria-hidden="true">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content bd-0">
    <div class="modal-header pd-y-20 pd-x-25">
      <h4 class="mg-b-5 tx-inverse lh-2 tx-uppercase">MUTASI THERAPIST</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      
      <div class="modal-body pd-0">
        <div class="row no-gutters">          
          <div class="col-lg-12 bg-white">
            <div class="pd-30">
              
            <div class="form-layout border-0 pd-y-0">
              
                <h4 class="tx-inverse tx-normal tx-roboto mg-b-20 fullname"></h4>
                <input type="hidden" name="therapist_id" class="kodekar" />

                <h5 class="tx-inverse tx-normal tx-roboto mg-b-5">From Branch :</h5>

                <div class="alert alert-danger" role="alert">
                  <strong class="d-block d-sm-inline-block-force"><?php echo get_branch_name($branch_id); ?></strong>
                </div><!-- alert -->
                
                <h5 class="tx-inverse tx-normal tx-roboto mg-b-5">To Branch :</h5>

                <div class="form-group mg-b-20">
                  <select class="form-control select" name="branch_id" data-placeholder="Choose Branch">
                    <option value="0">- Choose Branch -</option>
                    <?php foreach ($branchs as $branch){ ?> 
                      <option value="<?php echo $branch->id; ?>"
                          <?php
                          if (!empty($branch_id)) {
                              echo $branch->id == $branch_id ? 'selected' : '';
                          }
                          ?>><?php echo $branch->name ?>
                      </option>
                    <?php } ?>
                  </select>
                </div><!-- form-group -->

                <button type="submit" name="flag" value="mutasi" class="btn btn-info pd-y-12 btn-block  tx-18">Mutasi <i class="menu-item-icon ion-ios-redo-outline"></i></button>

            </div><!-- pd-20 -->

          </div><!-- col-6 -->
        </div><!-- row -->
      </div><!-- modal-body -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div>