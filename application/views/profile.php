<div class="br-pagebody">
  
  <div class="br-section-wrapper">

    <div class="row">

      <div class="col-xl-6 mg-t-20 mg-b-40 mg-xl-t-0">
        <div class="form-layout form-layout-5">
          <h6 class="br-section-label pd-b-30">Update Profile</h6>
          <div class="row">
            <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Firstname:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="text" class="form-control" placeholder="Enter firstname">
            </div>
          </div><!-- row -->
          <div class="row mg-t-20">
            <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Lastname:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="text" class="form-control" placeholder="Enter lastname">
            </div>
          </div>
          <div class="row mg-t-20">
            <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> Email:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="email" class="form-control" placeholder="Enter email address">
            </div>
          </div>
          <div class="row mg-t-20">
            <label class="col-sm-4 form-control-label"> Phone Number:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
            <input type="text" class="form-control" placeholder="Enter Phone Number">
            </div>
          </div><!-- row -->
          <div class="row mg-t-30">
            <div class="col-sm-8 mg-l-auto">
              <div class="form-layout-footer">
                <button class="btn btn-info">Update Profile</button>
              </div><!-- form-layout-footer -->
            </div><!-- col-8 -->
          </div>
        </div><!-- form-layout -->
      </div>

      <div class="col-xl-6 mg-t-20 mg-b-40 mg-xl-t-0">
        <div class="form-layout form-layout-5">
          <h6 class="br-section-label pd-b-30">Change Password</h6>
          <div class="row">
            <label class="col-sm-4 form-control-label"> Old Password:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="password" class="form-control" placeholder="Enter Your Old Password">
            </div>
          </div><!-- row -->
          <div class="row">
            <label class="col-sm-4 form-control-label"> New Password:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="password" class="form-control" placeholder="Enter Your New Password">
            </div>
          </div><!-- row -->
          <div class="row">
            <label class="col-sm-4 form-control-label"> Confirm Password:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="password" class="form-control" placeholder="Enter Your Confirm Password">
            </div>
          </div><!-- row -->
          <div class="row mg-t-30">
            <div class="col-sm-8 mg-l-auto">
              <div class="form-layout-footer">
                <button class="btn btn-info">Change Password</button>
              </div><!-- form-layout-footer -->
            </div><!-- col-8 -->
          </div>
        </div><!-- form-layout -->
      </div>

      <div class="col-xl-6 mg-t-20 mg-b-40 mg-xl-t-0">
        <div class="form-layout form-layout-5">
          <h6 class="br-section-label pd-b-30">Change Email</h6>
          <div class="row">
            <label class="col-sm-4 form-control-label"> Password:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="password" class="form-control" placeholder="Password Current Password">
            </div>
          </div><!-- row -->
          <div class="row mg-t-20">
            <label class="col-sm-4 form-control-label"><span class="tx-danger">*</span> New Email:</label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="text" class="form-control" placeholder="new Email">
            </div>
          </div>
          <div class="row mg-t-30">
            <div class="col-sm-8 mg-l-auto">
              <div class="form-layout-footer">
                <button class="btn btn-info">Change Email</button>
              </div><!-- form-layout-footer -->
            </div><!-- col-8 -->
          </div>
        </div><!-- form-layout -->
      </div>
    
    </div>
  </div>
</div>