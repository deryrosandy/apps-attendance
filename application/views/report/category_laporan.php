<div class="row  gridlist-dashboard">
	
	<div class="col-lg-2 col-sm-3 col-xs-12">
		<div class="small-box"  title="Komisi Additional">
			<a href="<?php echo base_url(); ?>report/komisi_additional?branch_code=<?php echo ($_GET['branch_code']); ?>" class="btn btn-block  btn-lg btn-primary">
				<h4 class="">Komisi Additional</h4>
			</a>
		</div>
	</div>

	<div class="col-lg-2 col-sm-3 col-xs-12">
		<div class="small-box"  title="Komisi Lulur">
			<a href="<?php echo base_url(); ?>report/komisi_lulur?branch_code=<?php echo ($_GET['branch_code']); ?>" class="btn btn-block  btn-lg btn-primary">
				<h4 class="">Komisi Lulur</h4>
			</a>
		</div>
	</div>

	<div class="col-lg-2 col-sm-3 col-xs-12">
		<div class="small-box"  title="Potongan">
			<a href="<?php echo base_url(); ?>report/potongan_therapist?branch_code=<?php echo ($_GET['branch_code']); ?>" class="btn btn-block  btn-lg btn-primary">
				<h4 class="">Potongan</h4>
			</a>
		</div>
	</div>

	<div class="col-lg-2 col-sm-3 col-xs-12">
		<div class="small-box"  title="Komisi F&B">
			<a href="<?php echo base_url(); ?>report/komisi_fnb?branch_code=<?php echo ($_GET['branch_code']); ?>" class="btn btn-block  btn-lg btn-primary">
				<h4 class="">Komisi F&B</h4>
			</a>
		</div>
	</div>

	<div class="col-lg-2 col-sm-3 col-xs-12">
		<div class="small-box"  title="Komisi Kuesioner">
			<a href="<?php echo base_url(); ?>report/komisi_kuesioner?branch_code=<?php echo ($_GET['branch_code']); ?>" class="btn btn-block  btn-lg btn-primary">
				<h4 class="">Kuesioner</h4>
			</a>
		</div>
	</div>

	<div class="col-lg-2 col-sm-3 col-xs-12">
		<div class="small-box"  title="Komisi Kode 'S'">
			<a href="<?php echo base_url(); ?>report/komisi_kodes?branch_code=<?php echo ($_GET['branch_code']); ?>" class="btn btn-block  btn-lg btn-primary">
				<h4 class="">Komisi Kode 'S'</h4>
			</a>
		</div>
	</div>

	<div class="col-lg-2 col-sm-3 col-xs-12">
		<div class="small-box"  title="Komisi Lain Lain">
			<a href="<?php echo base_url(); ?>report/komisi_lain_lain?branch_code=<?php echo ($_GET['branch_code']); ?>" class="btn btn-block  btn-lg btn-primary">
				<h4 class="">Komisi Lain-lain</h4>
			</a>
		</div>
	</div>

	<div class="col-lg-2 col-sm-3 col-xs-12">
		<div class="small-box"  title="Potongan">
			<a href="<?php echo base_url(); ?>report/tabungan_therapist?branch_code=<?php echo ($_GET['branch_code']); ?>" class="btn btn-block  btn-lg btn-primary">
				<h4 class="">Tabungan</h4>
			</a>
		</div>
	</div>


	<div class="col-lg-2 col-sm-3 col-xs-12">
		<div class="small-box"  title="Spv Therapist & Ibu Mess">
			<a href="<?php echo base_url(); ?>report/komisi_gaji_spv_therapist_mess?branch_code=<?php echo ($_GET['branch_code']); ?>" class="btn btn-block  btn-lg btn-primary">
				<h4 class="">Spv Trp & Ibu Mess</h4>
			</a>
		</div>
	</div>

	<div class="col-lg-2 col-sm-3 col-xs-12">
		<div class="small-box"  title="Laporan Gaji">
			<a href="<?php echo base_url(); ?>report/laporan_gaji_therapist?branch_code=<?php echo ($_GET['branch_code']); ?>" class="btn btn-block  btn-lg btn-primary">
				<h4 class="">Laporan Gaji</h4>
			</a>
		</div>
	</div>

</div>