<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-7">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Dari Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Sampai Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				
				<input type="hidden" name="branch_id" id="branch_id" value="<?php echo $_GET['branch_code']; ?>">

				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_additional" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
				<?php /*
				<div class="col-md-2 pull-right">
					<a href="<?php echo base_url('kasir/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Excel</a>
				</div>
				*/ ?>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="additional_product" class="tabel-report table  table-striped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Kode TRP</th>
								<th>Nama Therapist</th>
								<th>Tgl Masuk</th>
							</tr>
						</thead>
						<tbody id="data_additional_product">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-5">
		
		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Rekap Gaji Therapist</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12">
						<ul class="list-group list-group-bordered">
							<li class="list-group-item">
								<b>Outlet</b><span class="text-bold pull-right" id="branch_name"></span>
							</li>
							<li class="list-group-item">
								<b>Periode</b><span class="text-bold pull-right" id="periode"></span>
							</li>
							<li class="list-group-item">
								<b>Nama Therapist</b><span class="text-bold pull-right" id="namakar"></span>
							</li>
							<li class="list-group-item">
								<b>Kode Therapist</b><span class="text-bold pull-right" id="kodekar"></span>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		
		<div class="box">
			<div class="box box-default box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Komisi Additional</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12 table-responsive ">

						<table id="data-harian-detail" class="tabel-report  table  table-striped">
							<thead>
								<tr>
									<th class='text-center'>Kode</th>
									<th class='text-center'>Nama Product</th>
									<th class='text-center'>Jumlah</th>
									<th class='text-center'>Komisi</th>
									<th class='text-center'>Total Komisi</th>
								</tr>
							</thead>
							<tbody id="detail_additional" class="list-detail-kasir">
								
							</tbody>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
	</div>
	
</div>