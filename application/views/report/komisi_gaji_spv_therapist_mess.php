<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-8">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Dari Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Sampai Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				
				<input type="hidden" name="branch_id" id="branch_id" value="<?php echo $_GET['branch_code']; ?>">

				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_gaji_spv" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
				<?php /*
				<div class="col-md-2 pull-right">
					<a href="<?php echo base_url('kasir/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Excel</a>
				</div>
				*/ ?>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="gaji_spv_therapist" class="tabel-report table  table-striped">
						<thead>
							<tr>
								<th class='text-center'>No.</th>
								<th class='text-center'>Nama Lengkap</th>
								<th class='text-center'>Posisi</th>
								<th class='text-center'>Gaji</th>
								<th class='text-center'>Kuesioner</th>
								<th class='text-center'>Total Gaji</th>
								<th class='text-center'>Action</th>
							</tr>
						</thead>
						<tbody id="data_gaji_spv_therapist">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-4">	

		<div class="box">
			<div class="box box-default box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Data SPV Therapist & Ibu Mess</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12 table-responsive ">

						<table id="data-harian-detail" class="tabel-report  table  table-striped">
							<thead>
								<tr>
									<th class='text-center'>No</th>
									<th class='text-center'>Nama Lengkap</th>
									<th class='text-center'>Jabatan</th>
									<th class='text-center'>Action</th>
								</tr>
							</thead>
							<tbody id="data_spv_therapist" class="list-detail-kasir">
								
							</tbody>
						</table>
					</div>
					<a style="margin-right: 10px;" id="hitung_gaji_spv_therapist" class="pull-left btn btn-md btn-primary"><i class="fa fa-money"></i> Hitung gaji</a>
					<a style="" id="tambah_spv_therapist" class="pull-left btn btn-md btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
	</div>
	
</div>

  <!-- Bootstrap modal Komisi FNB -->
  <div class="modal fade" id="modal_edit_spv_therapist" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi Lulur</h3>
				<!--<h4 class="username">Therapist : <span class="therapist_name"></span></h4>-->
			</div>
			<div class="modal-body">
				<form  method="post" class="form-horizontal" id="form_spv_therapist">
					<input value="" type="hidden" name="id_spv" class="id_spv" id="id_spv">				
					<input value="" type="hidden" name="branch_code" class="branch_code" id="branch_code">				
					<div class="box-body">
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Nama Lengkap</label>
							<div class="col-sm-9">
								<input type="text" val="" class="form-control" name="nama_spv" id="nama_spv" placeholder="Nama Lengkap">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Jabatan</label>
								<div class="col-sm-9">
								<select name="is_mess" id="jabatan" class="form-control" >
								</select>
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-3 control-label">No. Rekening</label>
							<div class="col-sm-9">
								<input type="text" val="" class="form-control" name="no_rek" id="no_rek" placeholder="No. Rekening">
							</div>
						</div>
						
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">No. Telp</label>
							<div class="col-sm-9">
								<input type="text" val="" class="form-control" name="no_telp" id="no_telp" placeholder="No. Telp">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Branch</label>
								<div class="col-sm-9">
								<select name="branch_code"  id="branch_id_list" class="form-control select2" style="width: 100%;" >
							
								</select>
							</div>
						</div>

					</div>
				</form>
            </div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

  <!-- Bootstrap modal Komisi FNB -->
  <div class="modal fade" id="modal_edit_gaji_spv_therapist" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi Lulur</h3>
				<!--<h4 class="username">Therapist : <span class="therapist_name"></span></h4>-->
			</div>
			<div class="modal-body">
				<form  method="post" class="form-horizontal" id="form_gaji_spv_therapist">
					<input value="" type="hidden" name="id_gaji" class="id_gaji" id="id_gaji">				
					<div class="box-body">
						<div class="form-group">
							<label for="" class="col-sm-4 control-label">Nama Lengkap</label>
							<div class="col-sm-8">
								<input type="text" val="" class="form-control" disabled name="nama_spv" id="nama_spv" placeholder="Nama Lengkap">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-4 control-label">Gaji Pokok</label>
							<div class="col-sm-8">
								<input type="number" val="" class="form-control" name="gaji" id="gajipokok" placeholder="Gaji Pokok">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-4 control-label">Kuesioner</label>
							<div class="col-sm-8">
								<input type="number" val="" class="form-control" name="kuesioner" id="kuesioner" placeholder="Kuesioner">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-4 control-label">Tunjangan Transport</label>
							<div class="col-sm-8">
								<input type="number" val="" class="form-control" name="tunj_transport" id="tunj_transport" placeholder="Tunjangan Transport">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-4 control-label">Tunjangan Luar Kota</label>
							<div class="col-sm-8">
								<input type="number" val="" class="form-control" name="tunj_luarkota" id="tunj_luarkota" placeholder="Tunjangan Luar Kota">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-4 control-label">BPJS Perusahaan</label>
							<div class="col-sm-8">
								<input type="number" val="" class="form-control" name="bpjs_prs" id="bpjs_prs" placeholder="BPJS Perusahaan">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-4 control-label">BPJS Karyawan</label>
							<div class="col-sm-8">
								<input type="number" val="" class="form-control" name="bpjs_kry" id="bpjs_kry" placeholder="Gaji Karyawan">
							</div>
						</div>	
											
					</div>
				</form>
            </div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->