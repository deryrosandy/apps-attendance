<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-10 col-lg-10">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Dari Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Sampai Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				
				<input type="hidden" name="branch_id" id="branch_id" value="<?php echo $_GET['branch_code']; ?>">

				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_komisi_lain" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="komisi_lain" class="tabel-report table  table-striped  center-all">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>Jumlah Komisi</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="data_komisi_lain">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>

  <!-- Bootstrap modal Komisi Additional -->
  <div class="modal fade" id="modal_edit_komisi_lain" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi Lain</h3>
				<h4 class="username">Therapist : <span class="therapist_name"></span></h4>
			</div>
			<div class="modal-body">
			<form class="form_komisi_lain">
				<fieldset>
					<div class="modal-body">
						<input value="" type="hidden" name="id_lain" class="id_lain">
						<ul class="nav nav-list col-lg-6">
							<li class="nav-header">Jumlah Komisi</li>
							<li>
								<input type="numeric" class="form-control jumlah_komisi" name="jumlah_komisi"/>
							</li>
						</ul> 
					</div>
				</fieldset>
			</form>
            </div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->