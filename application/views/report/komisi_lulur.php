<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Dari Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Sampai Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<?php if(@$_GET['branch_code']!==null){ ?>
					<input type="hidden" name="branch_id" id="branch_id" value="<?php echo $_GET['branch_code']; ?>">
				<?php }else{ ?>
					<div class="col-md-3">
						<div class="form-group">
							<label>Outlet :</label>

							<div class="">
								<select class="form-control pull-right" name="branch_id" id="branch_id">
									<?php $branchs = $this->general->getBranchList(); ?>
									<option value="">- Pilih Outlet -</option>
									<?php foreach ($branchs as $branch){ ?>
										<option value="<?php echo $branch->csname; ?>"><?php echo $branch->cname; ?></option>
									<?php } ?>
								</select>
							</div>
							<!-- /.input group -->
						</div>
					</div>
				<?php } ?>

				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_lulur" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="komisi_lulur" class="tabel-report table  table-striped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>SUI</th>
								<th>PRE</th>
								<th>PRS</th>
								<th>STD</th>
								<th>Level</th>
								<th>Sesi Massage</th>
								<th>Sesi Lulur</th>
								<th>Point</th>
								<th>Komisi Massage</th>
								<th>Komisi Lulur</th>
								<th>Total Komisi</th>
							</tr>
						</thead>
						<tbody id="data_premium_room">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>