<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-7">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Dari Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Sampai Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				
				<?php if($userdata->level==2 || $userdata->level==5){ ?>
					<input type="hidden" name="branch_id" id="branch_id" value="<?php echo $branch_id ?>">
				<?php }else{ ?>
					<input type="hidden" name="branch_id" id="branch_id" value="<?php echo @$_GET['branch_code']; ?>">
				<?php } ?>

				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_rekap_gaji_therapist" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
				<?php /*
				<div class="col-md-2 pull-right">
					<a href="<?php echo base_url('kasir/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Excel</a>
				</div>
				*/ ?>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="rekap_gaji_therapist" class="tabel-report table  table-striped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Nama Therapist</th>
								<th>Level</th>
								<th>Tgl Masuk</th>
							</tr>
						</thead>
						<tbody id="data_rekap_gaji_therapist">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-5">

		<div class="row">
			<div class="col-lg-12">
				<div class="input-group date">
				<?php if($userdata->level==2){ ?>
					<button  style="margin-right: 10px;" class="pull-left insert_note btn btn-md btn-info"><i class="fa fa-pencil-square-o"></i> Insert Note</button>
					
					<div id="status_approve" class="pull-left"></div>
					
					<?php $status_approve = 0; ?>
					<?php /*
					<?php if($status_approve==1){ ?>
						<div id="status_approve"></div>
						<button style="margin-right: 10px;" class="btn btn-md btn-success"><i class="fa fa-check"></i> Approved</button>
					<?php }else{ ?>
						<button style="margin-right: 10px;" id="button_approval"  title="Set Approved" class="btn btn-md btn-danger"><i class="fa fa-close"></i> Set Approve</button>
					<?php } ?>
					*/
					?>

				<?php }elseif($userdata->level==1){ ?>
					<a style="margin-right: 10px;" href="<?php echo base_url(); ?>report/print_rekapan_gaji_all?branch_code=<?php echo $_GET['branch_code']; ?>?from=" target="_blank" id="btn_print_rekapan_gaji" class="pull-left btn btn-md btn-warning"><i class="fa fa-print"></i> Rekapan Gaji</a>
					<a style="margin-right: 10px;" href="<?php echo base_url(); ?>report/print_rekapan_gaji_all?branch_code=<?php echo $_GET['branch_code']; ?>?from=" target="_blank" id="btn_print_rekapan_gaji" class="pull-left btn btn-md btn-info"><i class="fa fa-print"></i> Slip Gaji</a>
					
					<div id="status_approve" class="pull-left"></div>
					

				<?php }elseif($userdata->level==5){ ?>
					
					<div id="status_approve" class="pull-left"></div>

				<?php }else{ ?>

					<a style="margin-right: 10px;" href="<?php echo base_url(); ?>report/print_rekapan_gaji_all?branch_code=<?php echo $_GET['branch_code']; ?>?from=" target="_blank" id="btn_print_rekapan_gaji" class="pull-left btn btn-md btn-success"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
					<a style="margin-right: 10px;" href="<?php echo base_url(); ?>report/print_excel_rekapan_gaji_all?branch_code=<?php echo $_GET['branch_code']; ?>?from=" target="_blank" id="btn_print_rekapan_gaji" class="pull-left btn btn-md btn-warning"><i class="fa fa-file-excel-o"></i> Export Excel</a>
					<a href="<?php echo base_url(); ?>report/print_rekapan_gaji_all?branch_code=<?php echo $_GET['branch_code']; ?>?from=" target="_blank" id="btn_print_rekapan_gaji" class="pull-left btn btn-md btn-info"><i class="pull-leftfa fa-print"></i> Cetak Slip Gaji</a>
				
				<?php } ?>
				</div>
				<p></p>
			</div>
		</div>

		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Rekap Gaji Therapist</h3>
					<span class="pull-right-container">
						<span class="pull-right label" id="label_notes"></span>
					</span>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12">
						<input type="hidden" id="branch_code" />
						<ul class="list-group list-group-bordered">
							<li class="list-group-item">
								<b>Outlet</b><span class="text-bold pull-right" id="branch_name"></span>
							</li>
							<li class="list-group-item">
								<b>Periode</b><span class="text-bold pull-right" id="periode"></span>
							</li>
							<li class="list-group-item">
								<b>Nama Therapist</b><span class="text-bold pull-right" id="namakar"></span>
							</li>
							<li class="list-group-item">
								<b>Kode Therapist</b><span class="text-bold pull-right" id="kodetrp"></span>
							</li>
							<span class="text-bold hidden pull-right" id="kodekar"></span>
						</ul>
					</div>
					
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		
		<div class="box">
			<div class="box box-primary  collapsed-box box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Penghasilan</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="col-lg-12">

						<div class="table-responsive">
							<table class="table">
								<tbody>
									<tr>
										<td style="width:50%">Total Sesi</td>
										<td style="width:40%" class='text-bold text-right'><span id="total_sesi"></span></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary" id="modal_total_sesi"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									<tr>
										<td>Total Point</td>
										<td class='text-bold text-right'><span id="total_point"></span></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary" id="modal_total_point"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									<tr>
										<td>Komisi Lulur Plus</td>
										<td class='text-bold text-right'><span id="total_komisi_lulur"></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary"  id="modal_komisi_lulur"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									<tr>
										<td>Komisi Additional</td>
										<td class='text-bold text-right'><span id="total_additional"></span></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary" id="modal_komisi_additional"><i class="fa fa-search"></i></button>
										</td>
									</tr>								
									<tr>
										<td>Komisi F&B</td>
										<td class='text-bold text-right'><span id=total_komisi_fnb></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary" id="modal_komisi_fnb"><i class="fa fa-search"></i></button>
										</td>
									</tr>
									<tr>
										<td>Kuesioner</td>
										<td class='text-bold text-right'><span id="total_kuesioner"></td>
									</tr>
									<tr>
										<td>Lain-lain</td>
										<td class='text-bold text-right'><span id=total_komisi_lain></td>
									</tr
									<tr>
										<th class='text-bold'>Total Penghasilan</th>
										<td class='text-bold text-right'><span id="total_penghasilan"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		
		<div class="box">
			<div class="box box-primary collapsed-box box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Potongan</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="col-lg-12">

						<div class="table-responsive">
							<input type="hidden" value="" id="id_potongan"/>
							<table class="table">
								<tbody>
									<tr>
										<td style="width:50%">Management Fee</td>
										<td class='text-bold text-right' id="potongan_yayasan" ondblclick="return editPotonganYayasan(kodekar);"></td>
									</tr>
									<tr>
										<td>Asuransi</td>
										<td class='text-bold text-right' id="potongan_asuransi"></span></td>
									</tr>
									<tr>
										<td>Koperasi</td>
										<td class='text-bold text-right' id="potongan_koperasi"></td>
									</tr>
									<tr>
										<td>Denda</td>
										<td class='text-bold text-right' id="potongan_denda"></td>
									</tr>
									<tr>
										<td>Hutang</td>
										<td class='text-bold text-right' id="potongan_hutang"></td>
									</tr>
									<tr>
										<td>Tiket</td>
										<td class='text-bold text-right' id="potongan_tiket"></td>
									</tr>
									<tr>
										<td>Sertifikasi</td>
										<td class='text-bold text-right' id="potongan_sertifikasi"></td>
									</tr>
									<tr>
										<td>Treatment Zion</td>
										<td class='text-bold text-right' id="potongan_zion"></td>
									</tr>
									<tr>
										<td>Treatment Galaxy</td>
										<td class='text-bold text-right' id="potongan_trt_glx"></td>
									</tr>
									<tr>
										<td>BPJS Karyawan</td>
										<td class='text-bold text-right' id="potongan_bpjs_kry"></td>
									</tr>
									<tr>
										<td>BPJS Perusahaan</td>
										<td class='text-bold text-right' id="potongan_bpjs_prs"></td>
									</tr>
									<tr>
										<th class='text-bold'>Total Potongan</th>
										<td class='text-bold text-right' id="total_potongan"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- /.box-body -->
			</div>
		</div>

		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Summary</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="col-lg-12">

						<div class="table-responsive">
							<input type="hidden" value="" id="id_potongan"/>
							<table class="table tabel-report table-striped">
								<tbody>
									<tr>
										<td class="text-bold" style="width:60%">Total Penghasilan</td>
										<td class='text-bold text-right' id="total_penghasilan_summary"></td>
									</tr>
									<tr>
										<td class="text-bold" style="width:60%">Total Potongan</td>
										<td class='text-bold text-right' id="total_potongan_summary"></td>
									</tr>
									<tr>
										<td class="text-bold">Sisa Penghasilan</td>
										<td class='text-bold text-right' id="sisa_penghasilan"></td>
									</tr>
									<tr>
										<td class="text-bold">Tabungan</td>
										<td class='text-bold text-right' id="jumlah_tabungan"></td>
									</tr>
									<tr>
										<th class='text-bold'>Total Gaji Di Terima</th>
										<td  id="take_homepay" colspan="2" class='text-bold text-right'></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- /.box-body -->				
			</div>
		</div>
	</div>
	
</div>

<!-- Bootstrap Modal Notes -->
<div class="modal fade" id="modal_insert_notes" role="dialog">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Tambah Catatan</h3>
				<h4 class="username">THERAPIST : <span class="therapist_name"></span></h4>
			</div>
			<div class="modal-body">
				<form class="form_insert_notes">
					<fieldset>
						<div class="modal-body">
							<input value="" type="hidden" name="kodekar" class="id_catatan"/>
							<input value="" type="hidden" name="awal" class="date_awal"/>
							<input value="" type="hidden" name="akhir" class="date_akhir"/>
							<ul class="nav nav-list col-lg-12">
								<li class="nav-header">Masukkan Catatan</li>
								<li>
									<textarea name="deskripsi" class="form-control col-xs-12" style="resize: vertical;" rows="4"></textarea>
								</li>
							</ul> 
						</div>
					</fieldset>
				</form>
            </div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<!-- Bootstrap modal Komisi Lulur -->
<div class="modal fade" id="modal_data_komisi_lulur_plus" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi Lulur</h3>
				<h4 class="username">Therapist : <span class="therapist_name"></span></h4>
			</div>
			<div class="modal-body">
                <div class="fetched-data">
                    <table id="data_komisi_lulur_plus" class="table table-responsive table-striped  table-modal" width="100%">
						<thead>
							<tr>
								<th class="text-center">No.</th>
								<th class="text-center">Tanggal</th>
								<th class="text-center">No Kamar</th>
								<th class="text-center">Room</th>
								<th class="text-center">Sesi</th>
								<th class="text-center">Point</th>
							</tr>
						</thead>
                    </table>   
                </div>
            </div>
			<div class="modal-footer">
				<div class="col-md-8" id="modal_footer_info">
					
				</div>
				<div class="col-md-4">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  <!-- Bootstrap modal Komisi Additional -->
<div class="modal fade" id="modal_data_komisi_additional" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi Lulur</h3>
				<h4 class="username">Therapist : <span class="therapist_name"></span></h4>
			</div>
			<div class="modal-body">
                <div class="fetched-data">
                    <table id="data_komisi_additional" class="table table-responsive table-striped  table-modal" width="100%">
						<thead>
							<tr>
								<th class="text-center">Kode</th>
								<th class="text-center">Nama Product</th>
								<th class="text-center">Jumlah</th>
								<th class="text-center">Komisi</th>
								<th class="text-center">Total Komisi</th>
							</tr>
						</thead>
                    </table>   
                </div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  <!-- Bootstrap modal Komisi Additional -->
<div class="modal fade" id="modal_view_notes" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Catatan</h3>
			</div>
			<div class="modal-body">
                <div id="body_notes">
					  
                </div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->

<!-- Bootstrap modal Komisi Additional -->
<div class="modal fade" id="modal_view_komisi_fnb" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi FNB</h3>
				<!--<h4 class="username">Therapist : <span class="therapist_name"></span></h4>-->
			</div>
			<div class="modal-body">
				<div class="fetched-data">
					<table id="data_komisi_additional" class="table table-responsive table-striped  table-modal" width="100%">
						<thead>
							<tr>
								<th class="text-center">No.</th>
								<th class="text-center">Kode</th>
								<th class="text-center">Nama Product</th>
								<th class="text-center">Jumlah</th>
								<th class="text-center">Harga</th>
								<th class="text-center">Total Komisi</th>
							</tr>
						</thead>
						<tbody id="data_modal_videw_komisi_fnb">
						</tbody>
					</table>   
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->