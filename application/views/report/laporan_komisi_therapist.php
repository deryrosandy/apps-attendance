<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">

	<div class="col-lg-12">
					
		<div class="row">
			<div class="col-lg-8">
				<div class="row" id="filter_dashboard">
					<div class="col-md-3">
						<div class="form-group">
							<label>Periode :</label>

							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control pull-right datepicker" id="bulan_periode" placeholder="Periode" data-date="2013-02" data-date-format="yyyy-mm">
							</div>
							<!-- /.input group -->
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>&nbsp;</label>
							<div class="input-group date">
								<button id="button_filter_dashboard" class="btn btn-md btn-primary">Filter</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		
		<div class="box box-primary">
			<div class="box-body box-profile">
				<img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>assets/dist/img/female-user.jpg" alt="User profile picture">

				<h3 class="profile-username text-center"><?php echo $data_therapist->namakar; ?></h3>

				<!--<p class="text-muted text-center">&nbsp;</p>-->
				<input type="hidden" value="<?php echo $kodekar; ?>" id="kodekar" name="kodekar" />
				<input type="hidden" value="<?php echo $data_therapist->kodetrp; ?>" id="kodetrp" name="kodetrp" />
				<input type="hidden" value="<?php echo $data_therapist->namakar; ?>" id="namakar" name="namakar" />
				<input type="hidden" value="<?php echo $data_therapist->branch_code; ?>" id="branch_id" name="branch_id" />

				<ul class="list-group list-group-unbordered">
					<li class="list-group-item">
						<b>Kode Therapist <span class="pull-right"><?php echo $data_therapist->kodetrp; ?></span></b>
					</li>
					<li class="list-group-item">
						<b>Level Therapist <span class="pull-right" id="level_komisi" data-level="<?php echo get_level_therapist($data_therapist->levelkomisi); ?>"><?php echo get_level_therapist($data_therapist->levelkomisi); ?></span></b>
					</li>
					<li class="list-group-item">
						<b>Saldo Tabungan <span class="pull-right">Rp. <?php echo format_digit($data_therapist->saldo_terakhir); ?></span></b>
					</li>
					<li class="list-group-item">
						<b>No. Rekening <span class="pull-right"><?php echo $data_therapist->bank; ?>  <?php echo $data_therapist->no_rek; ?></span></b>
					</li>
				</ul>
			</div>
			<!-- /.box-body -->
		</div>

		<?php /*
		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Data Therapist</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12">
						<input type="hidden" id="branch_code" />
						<ul class="list-group list-group-bordered">							
							<li class="list-group-item">
								<b>Nama Therapist</b><span class="text-bold pull-right" id="namakar"><?php echo $data_therapist->namakar; ?></span>
							</li>
							<li class="list-group-item">
								<b>Kode Therapist</b><span class="text-bold pull-right" id="kodekar"><?php echo $data_therapist->kodetrp; ?></span>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		*/ ?>
		
		<div class="box">
			<div id="status_data" class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
                Data Tidak Tersedia
			</div>
		</div>

		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Laporan Penghasilan</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="col-lg-12">

						<div class="table-responsive">
							<table class="tabel-report table table-striped" id="laporan_pendapatan_therapist" branch-id="<?php echo @$branch_id; ?>">
								<tbody>
									<tr id="total_sesi_kar" data-from="" data-to="" kodekar="<?php echo $kodekar; ?>">
										<td style="width:50%"><strong>Total Sesi</strong></td>
										<td id="data_total_sesi" style="width:40%" class='text-bold text-right'></td>
									</tr>
									<tr id="total_point_kar" title="Klik Untuk Melihat Detail Transaksi">
										<td><strong>Total Point</strong></td>
										<td id="data_total_point" class='text-bold text-right'></td>
									</tr>
									<tr id="total_komisi_lulur_kar">
										<td><strong>Komisi Lulur Plus</strong></td>
										<td id="data_total_komisi_lulur" class='text-bold text-right'></td>
									</tr>
									<tr id="total_additional_kar">
										<td><strong>Komisi Additional</strong></td>
										<td id="data_total_komisi_additional" class='text-bold text-right'></td>
									</tr>
									<tr id="total_fnb_kar">
										<td><strong>Komisi F&B</strong></td>
										<td class='text-bold text-right' id="data_total_komisi_fnb" ></td>
									</tr>
									<tr id="total_kuesioner_kar">
										<td><strong>Kuesioner</strong></td>
										<td class='text-bold text-right' id="data_total_komisi_kuesioner" ></td>
									</tr>
									<tr id="total_lain_lain_kar">
										<td><strong>Lain-lain</strong></td>
										<td class='text-bold text-right' id="data_total_komisi_lain" ></td>
									</tr>
									<tr id="total_penghasilan_kar">
										<th class='text-bold lead'>Total Penghasilan</th>
										<td colspan="2" class='text-bold text-right lead'  id="data_total_penghasilan" ></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		
		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Potongan</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="col-lg-12">

						<div class="table-responsive">
							<input type="hidden" value="" id="id_potongan"/>
							<table class="table tabel-report table-striped">
								<tbody>
									<tr>
										<td class="text-bold" style="width:60%">Management Fee</td>
										<td class='text-bold text-right' id="potongan_yayasan"></td>
									</tr>
									<tr>
										<td class="text-bold">Asuransi</td>
										<td class='text-bold text-right' id="potongan_asuransi"></td>
									</tr>
									<tr>
										<td class="text-bold">Koperasi</td>
										<td class='text-bold text-right' id="potongan_koperasi"></td>
									</tr>									
									<tr>
										<td class="text-bold">Denda</td>
										<td class='text-bold text-right' id="potongan_denda"></td>
									</tr>									
									<tr>
										<td class="text-bold">Hutang</td>
										<td class='text-bold text-right' id="potongan_hutang"></td>
									</tr>									
									<tr>
										<td class="text-bold">Tiket</td>
										<td class='text-bold text-right' id="potongan_tiket"></td>
									</tr>									
									<tr>
										<td class="text-bold">Sertifikasi</td>
										<td class='text-bold text-right' id="potongan_sertifikasi"></td>
									</tr>									
									<tr>
										<td class="text-bold">Treatment Zion</td>
										<td class='text-bold text-right' id="potongan_trt"></td>
									</tr>									
									<tr>
										<td class="text-bold">Treatment Galaxy</td>
										<td class='text-bold text-right' id="potongan_glx"></td>
									</tr>
									<tr>
										<td class="text-bold">BPJS Karyawan</td>
										<td class='text-bold text-right' id="potongan_bpjs_kry"></td>
									</tr>
									<tr>
										<td class="text-bold">BPJS Perusahaan</td>
										<td class='text-bold text-right' id="potongan_bpjs_prs"></td>
									</tr>
									<tr>
										<th class='text-bold lead'>Total Potongan</th>
										<td  id="total_potongan" colspan="2" class='text-bold text-right lead'></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- /.box-body -->
			</div>
		</div>

		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Summary</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="col-lg-12">

						<div class="table-responsive">
							<input type="hidden" value="" id="id_potongan"/>
							<table class="table tabel-report table-striped">
								<tbody>
									<tr>
										<td class="text-bold" style="width:60%">Total Penghasilan</td>
										<td class='text-bold text-right' id="total_penghasilan_summary"></td>
									</tr>
									<tr>
										<td class="text-bold" style="width:60%">Total Potongan</td>
										<td class='text-bold text-right' id="total_potongan_summary"></td>
									</tr>
									<tr>
										<td class="text-bold">Sisa Penghasilan</td>
										<td class='text-bold text-right' id="sisa_penghasilan"></td>
									</tr>
									<tr>
										<td class="text-bold">Tabungan</td>
										<td class='text-bold text-right' id="tabungan">(&nbsp;&nbsp;&nbsp;)</td>
									</tr>
									<tr>
										<th class='text-bold lead'>Total Gaji Di Terima</th>
										<td  id="data_take_homepay" colspan="2" class='text-bold text-right lead'></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- /.box-body -->
			</div>
		</div>

	</div>
	
	<div class="col-md-6">

		<div class="box">
			<div class="box-header row">
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="total_sesi_therapist" class="tabel-report table  table-striped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Tanggal</th>
								<th>Loker</th>
								<th>Room</th>
								<th>Outlet</th>
								<th>Sesi</th>
								<th>Point</th>
							</tr>
						</thead>
						<tbody id="data_total_sesi_therapist">

						</tbody>
						<tfoot>
							<tr>
								<th colspan="5" style="text-align:right">Total:</th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>

					<div id="footer_info_sesi"></div>

					<table id="komisi_add_product" class="tabel-report table  table-striped">
						<thead>
							<tr>
								<th>Kode</th>
								<th>Nama Service</th>
								<th>Jumlah</th>
								<th>Komisi</th>
								<th>Total Komisi</th>
							</tr>
						</thead>
						<tbody id="detail_additional">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>

<!-- Bootstrap modal Komisi Lulur -->
<div class="modal fade" id="modal_data_komisi_lulur_plus" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi Lulur</h3>
				<h4 class="username">Therapist : <span class="therapist_name"></span></h4>
			</div>
			<div class="modal-body">
                <div class="fetched-data">
                    <table id="data_komisi_lulur_plus" class="table table-responsive table-striped  table-modal" width="100%">
						<thead>
							<tr>
								<th class="text-center">No.</th>
								<th class="text-center">Tanggal</th>
								<th class="text-center">Loker</th>
								<th class="text-center">Room</th>
								<th class="text-center">Sesi</th>
								<th class="text-center">Point</th>
							</tr>
						</thead>
                    </table>   
                </div>
            </div>
			<div class="modal-footer">				
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  <!-- Bootstrap modal Komisi Additional -->
<div class="modal fade" id="modal_data_komisi_additional" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi Lulur</h3>
				<h4 class="username">Therapist : <span class="therapist_name"></span></h4>
			</div>
			<div class="modal-body">
                <div class="fetched-data">
                    <table id="data_komisi_additional" class="table table-responsive table-striped  table-modal" width="100%">
						<thead>
							<tr>
								<th class="text-center">Kode</th>
								<th class="text-center">Nama Product</th>
								<th class="text-center">Jumlah</th>
								<th class="text-center">Komisi</th>
								<th class="text-center">Total Komisi</th>
							</tr>
						</thead>
                    </table>   
                </div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->