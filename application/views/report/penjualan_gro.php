<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-4">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Dari Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal_gro"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Sampai Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal_gro">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				
				<input type="hidden" name="branch_id" id="branch_id" value="<?php echo $_GET['branch_code']; ?>">

				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_penjualan_gro" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
				<?php /*
				<div class="col-md-2 pull-right">
					<a href="<?php echo base_url('kasir/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Excel</a>
				</div>
				*/ ?>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="penjualan_gro" class="tabel-report table  table-striped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode GRO</th>
								<th>Nama GRO</th>
							</tr>
						</thead>
						<tbody id="data_penjualan_gro">

						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="box">
			<div class="box box-default box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Data Penjualan GRO</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12 table-responsive ">

						<table id="data-harian-detail" class="tabel-report  table  table-striped">
							<thead>
								<tr>
									<th class='text-center'>Nama Product</th>
									<th class='text-center'>Jumlah</th>
									<!--<th class='text-center'>Komisi</th>-->
									<!--<th class='text-center'>Total</th>-->
								</tr>
							</thead>
							<tbody id="detail_penjualan_gro" class="list-detail-kasir">
								
							</tbody>
						</table>

					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>

		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Target Point</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12">
						<table id="penjualan_gro" class="tabel-report table  table-striped">
							<thead>
								<tr>
									<th>Nama Product</th>
									<th>Point</th>
								</tr>
							</thead>
							<tbody id="">
								<tr>
									<td>Suite</td>
									<td id="base_point_suite">10</td>
								</tr>
								<tr>
									<td>Hot Stone</td>
									<td id="base_point_hotstone">10</td>
								</tr>
								<tr>
									<td>Aroma</td>
									<td id="base_point_aroma">5</td>
								</tr>
								<tr>
									<td>MUD</td>
									<td id="base_point_mud">50</td>
								</tr>
								<tr>
									<td>Lulur</td>
									<td id="base_point_lulur">10</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>

	</div>
	
	<div class="col-md-4">

		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Point Penjualan GRO</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12 table-responsive ">

						<table id="data-harian-detail" class="tabel-report  table  table-striped">
							<thead>
								<tr>
									<th class='text-center'>Nama GRO</th>
									<th class='text-center'>Suite</th>
									<th class='text-center'>Hot Stone</th>
									<th class='text-center'>Aroma</th>
									<th class='text-center'>Mud</th>
									<th class='text-center'>Lulur</th>
									<th class='text-center'>Total Point</th>
								</tr>
							</thead>
							<tbody id="data_point_detail_gro" class="list-detail-kasir">
								
							</tbody>
						</table>

					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>

		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Perhitungan MUD</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12">
						<ul class="list-group list-group-bordered">
							<li class="list-group-item">
								<b>Target Perhari</b><span class="text-bold pull-right" id="target_day">7</span>
							</li>
							<li class="list-group-item">
								<b>Jumlah periode hari</b><span class="text-bold pull-right" id="total_hari_target"></span>
							</li>
							<li class="list-group-item">
								<b>Target Penjualan MUD</b><span class="text-bold pull-right" id="target_mud"></span>
							</li>
							<li class="list-group-item">
								<b>Total Penjualan MUD</b><span class="text-bold pull-right" id="total_mud"></span>
							</li>
							<li class="list-group-item">
								<b>Status</b><span class="text-bold pull-right" id="status_target_mud"></span>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>

		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Total Penerimaan Gaji GRO</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12 table-responsive ">
						<input type="hidden" id="data_value_gaji_gro" />
						<table id="data-harian-detail" class="tabel-report  table  table-striped">
							<thead>
								<tr>
									<th class='text-center'>Nama GRO</th>
									<th class='text-center'>Total Point</th>
									<th class='text-center'>Total gaji</th>
								</tr>
							</thead>
							<tbody id="data_penerimaan_gaji_gro" class="list-detail-kasir">
								
							</tbody>
						</table>

					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>

	</div>

	<div class="col-md-4">
		
		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Komisi GRO</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12">
						<ul class="list-group list-group-bordered">
							<li class="list-group-item">
								<b>Outlet</b><span class="text-bold pull-right" id="branch_name"></span>
							</li>
							<li class="list-group-item">
								<b>Periode</b><span class="text-bold pull-right" id="periode"></span>
							</li>
							<li class="list-group-item">
								<b>Total Tamu Treatment</b><span class="text-bold pull-right" id="total_treatment"></span>
							</li>
							<li class="list-group-item">
								<b>Total Hot Stone</b><span class="text-bold pull-right" id="total_hotstone"></span>
							</li>
							<li class="list-group-item">
								<b>Persentase Komisi Hot Stone</b><span class="text-bold pull-right" id="persen_hotstone"></span>
							</li>
							<li class="list-group-item">
								<b>Komisi Hot Stone</b><span class="text-bold pull-right" id="komisi_hotstone"></span>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.box-body -->

				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12">
						<table id="penjualan_gro" class="tabel-report table  table-striped">
							<thead>
								<tr>
									<th>No.</th>
									<th>Nama Product</th>
									<th>Total</th>
									<th>Komisi</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody id="total_kommisi_addproduct">
								<tr>
									<td>1</td>
									<td>Hot Stone</td>
									<td id="total_hot_tbl"></td>
									<td class="text-right"  id="komisi_hot_tbl"></td>
									<td class="text-right"  id="total_komisi_hot_tbl"></td>
								</tr>
								<tr>
									<td>2</td>
									<td>Aroma</td>
									<td id="total_aroma_tbl"></td>
									<td class="text-right" id="komisi_aroma_tbl"></td>
									<td class="text-right"  id="total_komisi_aroma_tbl"></td>
								</tr>
								<tr>
									<td>3</td>
									<td>MUD</td>
									<td id="total_mud_tbl"></td>
									<td class="text-right"  id="komisi_mud_tbl"></td>
									<td class="text-right"  id="total_komisi_mud_tbl"></td>
								</tr>
								<tr>
									<td>4</td>
									<td>Lulur</td>
									<td id="total_lulur_tbl"></td>
									<td class="text-right"  id="komisi_lulur_tbl"></td>
									<td class="text-right"  id="total_komisi_lulur_tbl"></td>
								</tr>
								<tr>
									<th class="text-right" colspan="4">Total Komisi</th>
									<td class="text-right text-bold"  id="total_komisi_gro"></td>
								</tr>
								<tr>
									<th class="text-right" colspan="4">Potongan (10%)</th>
									<td class="text-right text-bold"  id="persen_potongan_komisi"></td>
								</tr>
								<tr>
									<th class="text-right" colspan="4">Sisa Komisi</th>
									<td class="text-right text-bold"  id="total_komisi_gro_net"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- /.box-body -->

				<!-- .box-body -->
				<div class="box-body">
					<div class="col-lg-12">
						<ul class="list-group list-group-bordered">
							<li class="list-group-item">
								<b>Total Komisi Treatment</b><span class="text-bold pull-right" id="komisi_trt_gro"></span>
							</li>
							<li class="list-group-item">
								<b>MUD HK</b><span class="text-bold pull-right" id="mud_hk"></span>
							</li>
							<li class="list-group-item">
								<b>Sisa (Komisi Treatment - MUD HK)</b><span class="text-bold pull-right" id="sisa_komisi_treatment"></span>
							</li>
							<li class="list-group-item">
								<b>Komisi GRO (80.5%)</b><span class="text-bold pull-right" id="komisi_gro_after"></span>
							</li>
							<li class="list-group-item">
								<b>Total Point</b><span class="text-bold pull-right" id="total_point_gro"></span>
							</li>
							<li class="list-group-item">
								<b>Point Per GRO</b><span class="text-bold pull-right" id="point_per_gro"></span>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.box-body -->

			</div>
		</div>

						
	</div>
	
</div>