<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Dari Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Sampai Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				
				<input type="hidden" name="branch_id" id="branch_id" value="<?php echo $_GET['branch_code']; ?>">

				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_potongan" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="potongan_therapist" class="tabel-report table  table-striped  center-all">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>Yayasan</th>
								<th>Asuransi</th>
								<th>BPJS KRY</th>
								<th>BPJS PRS</th>
								<th>Tiket</th>
								<th>Sertifikasi</th>
								<th>Koperasi</th>
								<th>Hutang</th>	
								<th>Denda</th>	
								<th>Trt Glx</th>	
								<th>Zion</th>		
								<th>Lain2</th>		
								<th>Total</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="data_potongan_therapist">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>


  <!-- Bootstrap modal Komisi FNB -->
  <div class="modal fade" id="modal_edit_potongan_therapist" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi Lulur</h3>
				<!--<h4 class="username">Therapist : <span class="therapist_name"></span></h4>-->
			</div>
			<div class="modal-body">
				<form  method="post" class="form-horizontal form_potongan_therapist" id="form_potongan_therapist">
				<input value="" type="hidden" name="id_potongan" class="id_potongan" id="id_potongan">				
					<div class="box-body">
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Management Fee</label>
							<div class="col-sm-9">
								<input type="text" val="" class="form-control" name="yayasan" id="yayasan" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Asuransi</label>
							<div class="col-sm-9">
								<input type="numeric" val="" class="form-control" name="asuransi" id="asuransi" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Koperasi</label>
							<div class="col-sm-9">
								<input type="numeric" val="" class="form-control" name="koperasi" id="koperasi" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Denda</label>
							<div class="col-sm-9">
								<input type="numeric" val="" class="form-control" name="denda" id="denda" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Hutang</label>
							<div class="col-sm-9">
								<input type="numeric" val="" class="form-control" name="hutang" id="hutang" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Treatment Zion</label>
							<div class="col-sm-9">
								<input type="numeric" val=""  class="form-control" name="zion" id="zion" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Treatment Galaxy</label>
							<div class="col-sm-9">
								<input type="numeric" val=""  class="form-control" name="trt_glx" id="trt_glx" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Tiket</label>
							<div class="col-sm-9">
								<input type="numeric" val=""  class="form-control" name="tiket" id="tiket" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Sertifikasi</label>
							<div class="col-sm-9">
								<input type="numeric" val=""  class="form-control" name="sertifikasi" id="sertifikasi" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">BPJS Karyawan</label>
							<div class="col-sm-9">
								<input type="numeric" val=""  class="form-control" name="bpjs_kry" id="bpjs_kry" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">BPJS Perusahaan</label>
							<div class="col-sm-9">
								<input type="numeric" val=""  class="form-control" name="bpjs_prs" id="bpjs_prs" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-3 control-label">Lain-lain</label>
							<div class="col-sm-9">
								<input type="numeric" val="" class="form-control" name="lain_lain" id="lain_lain" placeholder="">
							</div>
						</div>
					</div>
				</form>
            </div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->