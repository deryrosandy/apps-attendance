<style type="text/css">
	html { margin: 10px}
	.tg  {
		width: 100%;
		border-collapse:collapse;
		border-spacing:0;
		margin-bottom: 10px;
	}
	.tg1  {
		width: 200px;
		border-collapse:collapse;
		border-spacing:0;
		position: absolute;
		right: 0;
		display: inline-table;
	}
	.tg td{font-family:Arial, sans-serif;font-size:8px;padding:5px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg th{font-family:Arial, sans-serif;font-size:8px;font-weight:700;padding:10px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg .tg-yw4l{vertical-align:middle}
	.tg1 td{font-family:Arial, sans-serif;font-size:8px;padding:5px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 th{font-family:Arial, sans-serif;font-size:8px;font-weight:700;padding:5px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 .tg-yw4l{vertical-align:middle}
	tfoot {font-weight:700}
	.tc {text-align:center}
	.tl {text-align:left}
	.tr {text-align:right}
	h4 {font-size:16px;padding-top:0;}
	.subject_footer {
		font-family:Arial, sans-serif;
		position: relative;
		left: 0;
		bottom: 115px;
		font-size: 12px;
		font-weight: 500;
	}
	.subject_footer div {
		font-family:Arial, sans-serif;
		width: 120px;
		float: left;
		margin: 0 60px;
		font-weight: 500;
	}
	.subject_footer .name {
		font-family:Arial, sans-serif;
		margin-top: 50px;
		font-weight: 500;
	}
</style>
<?php $from_date = $_GET['from_date']; ?>
<?php $to_date = $_GET['to_date']; ?>

<h4 class="tc"><?php echo $branch->cname; ?><br/>GAJI THERAPIST PERIODE <?php echo strtoupper(tgl_indo($from_date)); ?> - <?php echo strtoupper(tgl_indo($to_date)); ?></h4>
<table width="100%" class="tg">
  <tr>
    <th class="tg-yw4l tc">NO.</th>
    <th class="tg-yw4l tc">NIT</th>
    <th width="80px" class="tg-yw4l tc">NAMA</th>
    <th width="60px" class="tg-yw4l tc">TGL MASUK</th>
    <th class="tg-yw4l tc">KODE</th>
    <th width="60px" class="tg-yw4l tc">GAJI</th>
    <th class="tg-yw4l tc">QUESIONER</th>
    <th class="tg-yw4l tc">YAYASAN</th>
    <th class="tg-yw4l tc">ASURANSI</th>
    <th class="tg-yw4l tc">KOPERASI</th>
    <th class="tg-yw4l tc">TABUNGAN</th>
	<th class="tg-yw4l tc">TRT. GLX</th>
	<th class="tg-yw4l tc">ZION</th>
    <th class="tg-yw4l tc">DENDA</th>
    <th class="tg-yw4l tc">HUTANG</th>
    <th class="tg-yw4l tc">BPJS KRYWN</th>
    <th class="tg-yw4l tc">BPJS PRSHN</th>
    <th class="tg-yw4l tc">Sertifikasi</th>
    <th class="tg-yw4l tc">Tiket</th>
    <th width="60px" class="tg-yw4l tc">TOTAL GAJI</th>
    <th class="tg-yw4l tc">KODE "S"</th>
    <th width="60px" class="tg-yw4l tc">THP</th>
  </tr>

  <?php $no = 1; ?>
  <?php $sum_gross_gaji = 0; ?>
  <?php $sum_kuesioner = 0; ?>
  <?php $sum_yayasan = 0; ?>
  <?php $sum_asuransi = 0; ?>
  <?php $sum_koperasi = 0; ?>
  <?php $sum_tabungan = 0; ?>
  <?php $sum_zion = 0; ?>
  <?php $sum_denda = 0; ?>
  <?php $sum_hutang = 0; ?>
  <?php $sum_total_gaji = 0; ?>
  <?php $sum_thp = 0; ?>
  <?php $sum_trt_glx = 0; ?>
  <?php $sum_bpjs_kry = 0; ?>
  <?php $sum_bpjs_prs = 0; ?>
  <?php $sum_sertifikasi = 0; ?>
  <?php $sum_tiket = 0; ?>
  <?php $sum_kodes = 0; ?>
	
  <?php foreach($lists as $list){ ?>

	<?php @$potongan = get_potongan_gaji($list->kodekar, $from_date, $to_date,  $branch->csname); ?>
	<?php @$tabungan_temp =get_tabungan($list->kodekar, $from_date, $to_date, $branch->csname); ?>
	<?php @$gross_gaji = get_gross_total_gaji($list->kodekar, $from_date, $to_date, $branch->csname); ?>	
	<?php //var_dump($gross_gaji); die();?>
	<?php @$tabungan = ($tabungan_temp !== null ? (intVal($tabungan_temp->setoran)) : '-'); ?>
	<?php @$komisi_kodes = get_komisi_kodes($list->kodekar, $from_date, $to_date, $branch->csname); ?>
	<?php //var_dump($tabungan); ?>
	<?php @$tabungan_new = ($tabungan !== '-' ? $tabungan_temp->setoran : 0); ?>
	<?php @$total_kuesioner = get_kuesioner($from_date, $to_date, $list->kodekar, $branch->csname); ?>
	<?php //var_dump($total_kuesioner); ?>
	<?php //$total_gaji = get_total_gaji($list->kodekar, $from_date, $to_date)-$tabungan_new+$total_kuesioner; ?>
	<?php @$total_gaji = get_total_gaji($list->kodekar, $from_date, $to_date,  $branch->csname); ?>
	<?php @$total_thp = get_takehomepay($list->kodekar, $from_date, $to_date,  $branch->csname); ?>	
	
	<tr>
		<td class="tg-yw4l tc"><?php echo $no; ?></td>
		<td class="tg-yw4l tc"><?php echo $list->kodetrp; ?></td>
		<td class="tg-yw4l"><?php echo $list->namalengkap; ?></td>
		<td class="tg-yw4l tc"><?php echo tgl_indo2($list->tglmasuk); ?></td>
		<td class="tg-yw4l tc"><?php echo get_level_therapist($list->levelkomisi); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($gross_gaji); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($total_kuesioner); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->yayasan); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->asuransi); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->koperasi); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($tabungan); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->trt_glx); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->zion); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->denda); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->hutang); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->bpjs_kry); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->bpjs_prs); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->sertifikasi); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit(@$potongan->tiket); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($total_gaji); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($komisi_kodes); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($total_thp); ?></td>
		
		<?php  	@$sum_gross_gaji+= @$gross_gaji; ?>
		<?php 	@$sum_kuesioner+= @$total_kuesioner; ?>
		<?php  	@$sum_yayasan+= @$potongan->yayasan; ?>
		<?php  @$sum_asuransi+= @$potongan->asuransi; ?>
		<?php  @$sum_koperasi+= @$potongan->koperasi; ?>
		<?php  @$sum_tabungan+= @$tabungan; ?>
		<?php  @$sum_trt_glx+= @$potongan->trt_glx; ?>
		<?php  @$sum_zion+= @$potongan->zion; ?>
		<?php  @$sum_denda+= @$potongan->denda; ?>
		<?php  @$sum_hutang+= @$potongan->hutang; ?>
		<?php  @$sum_total_gaji+= @$total_gaji; ?>
		<?php  @$sum_thp+= @$total_thp; ?>
		<?php @$sum_bpjs_kry+= @$potongan->bpjs_kry; ?>
		<?php  @$sum_bpjs_prs+= @$potongan->bpjs_prs; ?>
		<?php  @$sum_sertifikasi+= @$potongan->sertifikasi; ?>
		<?php  @$sum_tiket+= @$potongan->tiket; ?>
		<?php  @$sum_kodes+= @$komisi_kodes; ?>

		<?php //print_r($gross_gaji); ?>
		
	</tr>
	<?php $no++; ?>

  	<?php } ?>
	  <?php //print_r($sum_gross_gaji);  ?>
	  <?php // die(); ?>
	<?php $sum_total_gaji_spv = 0; ?>
	<?php $sum_gaji_spv = 0; ?>
	<?php $sum_kuesioner_spv = 0; ?>	

	<?php foreach($spv_therapist as $spv){ ?>

		<?php @$potongan = get_potongan_gaji($list->kodekar, $from_date, $to_date,  $branch->csname); ?>
		<?php @$thp_spv = $spv->total_gaji+$spv->bpjs_prs; ?>

		<tr>
			<td class="tg-yw4l tc"><?php echo $no; ?></td>
			<td class="tg-yw4l tc"><?php echo ($spv->is_mess==0) ? 'SPV Trp':'Ibu Mess'; ?></td>
			<td class="tg-yw4l"><?php echo $spv->nama_spv; ?></td>
			<td class="tg-yw4l tc"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tc"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit($spv->gaji); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit($spv->kuesioner); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit($spv->bpjs_kry); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit($spv->bpjs_prs); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit($spv->total_gaji); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit('-'); ?></td>
			<td class="tg-yw4l tr"><?php echo format_digit($thp_spv); ?></td>
		</tr>

		<?php  $sum_gaji_spv+= $spv->gaji; ?>
		<?php  $sum_total_gaji_spv+= $spv->total_gaji; ?>
		<?php  $sum_kuesioner_spv+= $spv->kuesioner; ?>
		
		<?php $no++; ?>
		
		<?php // $sum_gross_gaji+= $gross_gaji; ?>
		<?php  //$sum_kuesioner+= $total_kuesioner; ?>
		<?php // $sum_yayasan+= @$potongan->yayasan; ?>
		<?php  //$sum_mess+= @$potongan->mess; ?>
		<?php  //$sum_asuransi+= @$potongan->asuransi; ?>
		<?php  //$sum_koperasi+= @$potongan->koperasi; ?>
		<?php  //$sum_tabungan+= $tabungan; ?>
		<?php  //$sum_trt_glx+= @$potongan->trt_glx; ?>
		<?php  //$sum_zion+= @$potongan->zion; ?>
		<?php  //$sum_denda+= @$potongan->denda; ?>
		<?php  //$sum_hutang+= @$potongan->hutang; ?>
		<?php  //$sum_total_gaji+= $total_gaji; ?>
		<?php  //$sum_thp+= $total_thp; ?>
		<?php  //$sum_bpjs_kry+= @$potongan->bpjs_kry; ?>
		<?php // $sum_bpjs_prs+= @$potongan->bpjs_prs; ?>
		<?php //$sum_sertifikasi+= @$potongan->sertifikasi; ?>
		<?php  //$sum_tiket+= @$potongan->tiket; ?>
	
	<?php } ?>

	<?php $sum_thp = $sum_thp+$sum_total_gaji_spv; ?>
	<?php //var_dump($sum_gaji_spv); die(); ?>
	<?php $sum_gross_gaji = $sum_gross_gaji+$sum_gaji_spv; ?>
	<?php $sum_kuesioner = $sum_kuesioner+$sum_kuesioner_spv; ?>

  	<tfoot>
		<td colspan="5" class="tg-yw4l tc">TOTAL</td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_gross_gaji); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_kuesioner); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_yayasan); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_asuransi); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_koperasi); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_tabungan); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_trt_glx); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_zion); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_denda); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_hutang); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_bpjs_kry); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_bpjs_prs); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_sertifikasi); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_tiket); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_total_gaji); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_kodes); ?></td>
		<td class="tg-yw4l tr"><?php echo format_digit($sum_thp); ?></td>
	</tfoot>

</table>

<div class="subject_footer">
	<div class="tc">
		<p>Dibuat Oleh,</p>
		<p class="name"><?php echo $this->userdata->fullname ?></p>
  	</div>
	  <div class="tc">
		<p>Disetujui,</p>
		<p class="name" style="margin-bottom: 0; padding-botttom:0;">Ibu Rumanti S</p>
		<hr style="padding:0;margin:0;"/>
		<span style="margin-top:0px;">HRD</span>
  	</div>
	
	<?php if(@$status_approve){ ?>
		<div class="tc">
			<p>Disetujui,</p>
			<p class="name" style="margin-bottom: 0; padding-botttom:0;"><?php echo $pimpinan_outlet->fullname; ?></p>
			<hr style="padding:0;margin:0;"/>
			<span style="margin-top:0px;">Pimpinan Outlet</span>
		</div>
	
	<?php } ?>

</div>

<?php  $sum_fee = $sum_thp * 0.175; ?>
<?php  $tagihan = $sum_thp + $sum_fee; ?>

<table width="" class="tg1">
	<tr>
		<th class="tg-yw4l tl">Total Gaji</th>
		<th class="tg-yw4l tr"><?php echo format_digit($sum_thp); ?></th>
	</tr>
	<tr>
		<th class="tg-yw4l tl">Fee 17,5%</th>
		<th class="tg-yw4l tr"><?php echo format_digit($sum_fee); ?></th>
	</tr>
	<tr>
		<th class="tg-yw4l tl">Tagihan</th>
		<th class="tg-yw4l tr"><?php echo format_digit($tagihan); ?></th>
	</tr>
</table>