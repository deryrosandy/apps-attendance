<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-7">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Dari Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Sampai Tgl :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="form-group">
						<label>Outlet :</label>
						
						<select class="form-control select2 pull-right" name="branch_id" id="branch_id">
							<?php $branchs = $this->general->getBranchList(); ?>
							<option value="">- Pilih Outlet -</option>
							<?php foreach ($branchs as $branch){ ?>
								<option value="<?php echo $branch->csname; ?>"><?php echo $branch->cname; ?></option>
							<?php } ?>
						</select>
						<!-- /.input group -->
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_rekap_gaji_therapist" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="rekap_gaji_therapist" class="tabel-report table table-bordered table-striped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Nama Therapist</th>
								<th>Kode Honor</th>
								<th>Tgl Masuk</th>
							</tr>
						</thead>
						<tbody id="data_rekap_gaji_therapist">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-5">
		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Rekap Gaji Therapist</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12">
						<input type="hidden" id="branch_code" />
						<ul class="list-group list-group-bordered">
							<li class="list-group-item">
								<b>Outlet</b><span class="text-bold pull-right" id="branch_name"></span>
							</li>
							<li class="list-group-item">
								<b>Periode</b><span class="text-bold pull-right" id="periode"></span>
							</li>
							<li class="list-group-item">
								<b>Nama Therapist</b><span class="text-bold pull-right" id="namakar"></span>
							</li>
							<li class="list-group-item">
								<b>Kode Therapist</b><span class="text-bold pull-right" id="kodekar"></span>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		
		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Penghasilan</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="col-lg-12">

						<div class="table-responsive">
							<table class="table">
								<tbody>
									<tr>
										<td style="width:50%">Total Sesi</td>
										<td style="width:40%" class='text-bold text-right'><span id="total_sesi"></span></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary" id="modal_total_sesi">Detail</button>
										</td>
									</tr>
									<tr>
										<td>Total Point</td>
										<td class='text-bold text-right'><span id="total_point"></span></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary" id="modal_total_point">Detail</button>
										</td>
									</tr>
									<tr>
										<td>Komisi Lulur Plus</td>
										<td class='text-bold text-right'><span id="total_komisi_lulur"></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary" id="modal_komisi_lulur">Detail</button>
										</td>
									</tr>
									<tr>
										<td>Komisi Additional</td>
										<td class='text-bold text-right'><span id="total_additional"></span></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary" id="modal_komisi_additional">Detail</button>
										</td>
									</tr>								
									<tr>
										<td>Komisi F&B</td>
										<td class='text-bold text-right'><span id=total_fnb></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary">Detail</button>
										</td>
									</tr>
									<tr>
										<td>Kuesioner</td>
										<td class='text-bold text-right'><span id="total_kuesioner"></td>
										<td  style="width:60px;" class='text-bold text-center pull-right'>
											<button class="btn btn-xs btn-primary">Detail</button>
										</td>
									</tr>
									<tr>
										<td>Lain-lain</td>
										<td class='text-bold text-right'><span id=total_lain_lain></td>
										<td class='text-bold text-center pull-right'>
										</td>
									</tr
									<tr>
										<th class='text-bold'>Total Penghasilan</th>
										<td colspan="2" class='text-bold text-right'><span id="total_penghasilan"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		
		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Potongan</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="col-lg-12">

						<div class="table-responsive">
							<input type="hidden" value="" id="id_potongan"/>
							<table class="table">
								<tbody>
									<tr>
										<td style="width:50%">Management Fee</td>
										<td class='text-bold text-right' id="potongan_yayasan" ondblclick="return editPotonganYayasan(kodekar);"></td>
									</tr>
									<tr>
										<td>Asuransi</td>
										<td class='text-bold text-right' id="potongan_asuransi"></span></td>
									</tr>
									<tr>
										<td>Koperasi</td>
										<td class='text-bold text-right' id="potongan_koperasi"></td>
									</tr>
									<tr>
										<td>Mess</td>
										<td class='text-bold text-right' id="potongan_mess"></td>
									</tr>
									<tr>
										<td>Tabungan</td>
										<td class='text-bold text-right' id="potongan_tabungan"></td>
									</tr>
									<tr>
										<td>Zion</td>
										<td class='text-bold text-right' id="potongan_zion"></td>
									</tr>
									<tr>
										<td>Denda</td>
										<td class='text-bold text-right' id="potongan_denda"></td>
									</tr>
									<tr>
										<th class='text-bold'>Total Potongan</th>
										<td class='text-bold text-right' id="total_potongan"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- /.box-body -->
			</div>
		</div>

	</div>
	
</div>

<!-- Bootstrap modal Komisi Lulur -->
<div class="modal fade" id="modal_data_komisi_lulur_plus" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi Lulur</h3>
				<h4 class="username">Therapist : <span class="therapist_name"></span></h4>
			</div>
			<div class="modal-body">
                <div class="fetched-data">
                    <table id="data_komisi_lulur_plus" class="table table-responsive table-striped table-bordered table-modal" width="100%">
						<thead>
							<tr>
								<th class="text-center">No.</th>
								<th class="text-center">Tanggal</th>
								<th class="text-center">Loker</th>
								<th class="text-center">Room</th>
								<th class="text-center">Sesi</th>
								<th class="text-center">Point</th>
							</tr>
						</thead>
                    </table>   
                </div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  <!-- Bootstrap modal Komisi Additional -->
<div class="modal fade" id="modal_data_komisi_additional" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Komisi Additional</h3>
				<h4 class="username">Therapist : <span class="therapist_name"></span></h4>
			</div>
			<div class="modal-body">
                <div class="fetched-data">
                    <table id="data_komisi_additional" class="table table-responsive table-striped table-bordered table-modal" width="100%">
						<thead>
							<tr>
								<th class="text-center">Kode</th>
								<th class="text-center">Nama Product</th>
								<th class="text-center">Jumlah</th>
								<th class="text-center">Komisi</th>
								<th class="text-center">Total Komisi</th>
							</tr>
						</thead>
                    </table>   
                </div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->