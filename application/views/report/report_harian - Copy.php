<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-8">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Dari Tanggal :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Sampai Tanggal :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_harian" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
				<?php /*
				<div class="col-md-2 pull-right">
					<a href="<?php echo base_url('kasir/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Excel</a>
				</div>
				*/ ?>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive col-lg-12">
					<table id="data-penjualan-kasir" class="tabel-report table table-bordered table-striped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Tanggal</th>
								<th>Nomor</th>
								<th>No. Meja</th>
								<th>No. Pesanan</th>
								<th>Jam</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody id="">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="box">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Summary Transaksi</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body text-center">
					<h2>Total : <span id="summary_harian"></span></h2>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		
		<div class="box">
			<div class="box box-default box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Item Transaksi</h3>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-lg-12 table-responsive">
						<div class="col-md-12 pull-right">
							<h3>Nomor : <span id="nomor_bill"></span></h3>
						</div>
						<table id="data-harian-detail" class="tabel-report  table table-responsive table-bordered table-striped">
							<thead>
								<tr>
									<th>Menu</th>
									<th>Qty</th>
									<th>Harga</th>
									<th>Disc</th>
									<th>Jumlah</th>
								</tr>
							</thead>
							<tbody id="detail-data-penjualan" class="list-detail-kasir">
								
							</tbody>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
	</div>
	
</div>