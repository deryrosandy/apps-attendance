<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-7">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Dari Tanggal :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Sampai Tanggal :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_summary" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Sales Summary</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<table class="table table-striped">
					<tbody>
						<tr>
							<th><b>Bruto</b></th>
							<th class="text-right" id="sales_bruto">-</th>
						</tr>
						<tr>
							<th><b>Discount</b></th>
							<th class="text-right" id="sales_disc">-</th>
						</tr>
						<tr>
							<th><b>PPN</b></th>
							<th class="text-right" id="sales_ppn">-</th>
						</tr>
						<tr>
							<th><b>Service</b></th>
							<th class="text-right" id="sales_service">-</th>
						</tr>
						<tr>
							<th><b>Grand Total</b></th>
							<th class="text-right" id="grand_total">-</th>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		
	</div>
	
</div>