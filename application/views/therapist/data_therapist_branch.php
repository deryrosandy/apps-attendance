<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>data_therapist" class="btn btn-md btn-primary pull-right" style="margin-bottom: 10px;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Change Branch</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-body">
				<div class="col-lg-12 table-responsive ">
					<table id="" class="tabel-report table table-striped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>Nama Lengkap</th>
								<th>Tgl Masuk</th>
								<th>level</th>
								<th>No. Rekening</th>
								<th>Supplier</th>
								<th>Telp. Supplier</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="list_data_therapist">
							<?php $no = 1; ?>
							<?php foreach($therapist as $trp): ?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $trp->kodetrp; ?></td>
									<td><?php echo $trp->namakar; ?></td>
									<td><?php echo $trp->namalengkap; ?></td>
									<td><?php echo tgl_indo2($trp->tglmasuk); ?></td>
									<td><?php echo get_level_therapist($trp->levelkomisi); ?></td>
									<td><?php echo $trp->bank . ' ' . $trp->no_rek; ?></td>
									<td><?php echo $trp->supplier_name; ?></td>
									<td><?php echo $trp->supplier_phone; ?></td>
									<td>
										<div class="btn-group" style="display: -webkit-inline-box">
											<button class="btn btn-xs btn-primary view_therapist" kodekar="<?php echo $trp->kodekar; ?>"  branch_id="<?php echo $_GET['branch_code']; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View</button>
										</div>
									</td>
								</tr>
								<?php $no++; ?>
							<?php endforeach; ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>

<div id="therapistModal" class="modal fade">
	<div class="modal-dialog">
		<form action="<?php echo base_url(); ?>data_therapist/update_therapist" class="form-horizontal" id="therapist_form" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Therapist</h4>
					<input type="hidden" id="branch_id" class="form-control" name="branch_code" value=""/>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label col-xs-3" for="firstName">Kode:</label>
						<div class="col-xs-9">
							<input type="text"  class="form-control kodekar" id="" placeholder="Kode" readonly required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="kodetrp">Kode Therapist:</label>
						<div class="col-xs-9">
							<input type="text" name="kodetrp"  class="form-control" maxlength="4" size="4" id="kodetrp" placeholder="Kode TRP" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="namakar">Nama Therapist:</label>
						<div class="col-xs-9">
							<input type="text"  name="namakar" class="form-control" maxlength="30" id="namakar" placeholder="Nama Therapist" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">Nama Lengkap:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" name="namalengkap" maxlength="30" id="namalengkap" placeholder="Nama Lengkap">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">No. KTP:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" name="no_ktp"  maxlength="16" id="no_ktp" placeholder="No. KTP">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">Tgl Masuk:</label>
						<div class="col-xs-3">
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control pull-right datepicker" data-date="2013-02-28" data-date-format="yyyy-mm-dd" maxlength="10" name="tglmasuk"  placeholder="Tanggal Masuk" id="tglmasuk">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">Nama Supplier:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" name="supplier_name" id="supplier_name" maxlength="20" placeholder="Nama Supplier">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">Telp Supplier:</label>
						<div class="col-xs-9">
							<input type="tel" name="supplier_phone" class="form-control" id="supplier_phone" maxlength="15" placeholder="Telp Supplier">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">Status:</label>
						<div class="col-xs-9">
							<select name="status" class="form-control" id="status">
								<option value="SINGLE">SINGLE</option>
								<option value="MENIKAH">MENIKAH</option>
								<option value="CERAI">CERAI</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">Tempat Lahir:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" name="tmplahir" maxlength="30" id="tmplahir" placeholder="Tempat Lahir">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Tanggal Lahir:</label>
						<div class="col-xs-3">
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control pull-right datepicker" maxlength="10" name="tgllahir" id="tgllahir">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">Level:</label>
						<div class="col-xs-9">
							<select name="levelkomisi" class="form-control" id="levelkomisi">
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="postalAddress">Alamat:</label>
						<div class="col-xs-9">
							<textarea rows="3" name="alamat1" class="form-control" id="alamat1" placeholder="ALamat"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3" for="phoneNumber">Bank/No. Rekening:</label>
						<div class="col-xs-9">
							<div class="row">
								<div class="col-xs-3">
									<input type="text"  maxlength="10"  name="bank" class="form-control" id="bank" placeholder="BANK">	
								</div>
								<div class="col-xs-9">
									<input type="text"  maxlength="20" name="no_rek" class="form-control" id="no_rek" placeholder="No. Rekening">
								</div>
							</div>						
						</div>
					</div>					
				</div>
				<div class="modal-footer">
					<input type="hidden" name="kodekar" class="kodekar" />
					<input type="hidden" name="operation" id="operation" />
					<input type="submit" id="action" class="btn btn-success" value="Submit" />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>