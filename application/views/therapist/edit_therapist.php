<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="br-profile-page">

    <div class="col-xl-12 pd-30 mg-b-0">
        
		<div class="ht-70 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-between shadow-base">
		<ul class="nav nav-outline active-info align-items-center flex-row" role="tablist">
			<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#basic" role="tab">Data Diri</a></li>
			<li class="nav-item"><a class="nav-link " data-toggle="tab" href="#document" role="tab">Data Document</a></li>
			<li class="nav-item"><a class="nav-link " data-toggle="tab" href="#family" role="tab">Data Keluarga</a></li>
			<li class="nav-item"><a class="nav-link " data-toggle="tab" href="#pengalaman" role="tab">Pengalaman Kerja</a></li>
		</ul>
		<h4 class="tx-14 tx-right tx-uppercase tx-bold tx-inverse mg-t-10 mg-b-10 justify-content-right d-flex flex-row">DETAIL THERAPIST</h4>
		</div>

      <div class="tab-content mg-t-30">
        <div class="tab-pane fade active show" id="basic">
          
		  	<div class="br-pagebody">
				<div class="br-section-wrapper">
					<div class="row">
						<div class="col-xl-12 mg-t-0 mg-b-0">
							<div class="form-layout form-layout-1">

								<form action="<?php echo base_url(); ?>therapist/update_therapist/<?php echo $therapist->therapist_id; ?>" class="form-horizontal" id="therapist_form" method="post" enctype="multipart/form-data">

									<div class="d-flex align-items-center justify-content-between">
										<h4 class="tx-inverse tx-normal tx-roboto mg-b-20">Basic Detail</h4>
									</div>

									<input class="form-control" type="hidden" name="therapist_id" value="<?php echo $therapist->therapist_id; ?>" placeholder="Enter Fullname">
									
									<h5 class="tx-inverse tx-normal tx-roboto">Data Diri</h5>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
											<label class="form-control-label">Fullname: <span class="tx-danger">*</span></label>
											<input class="form-control" type="text" name="fullname" value="<?php echo $therapist->fullname; ?>" placeholder="Enter Fullname">
											</div>
										</div><!-- col-4 -->

										<div class="col-lg-6">
											<div class="form-group">
											<label class="form-control-label">Nickname: <span class="tx-danger">*</span></label>
											<input class="form-control" type="text" name="nickname" value="<?php echo $therapist->nickname; ?>" placeholder="Enter Nickname">
											</div>
										</div><!-- col-4 -->
										
										<div class="col-lg-4">
											<div class="form-group">
											<label class="control-label col-xs-3" for="phoneNumber">No. KTP: <span class="tx-danger">*</span></label>
											<input type="text" class="form-control" name="no_ktp" id="no_ktp" value="<?php echo $therapist->no_ktp; ?>" placeholder="Enter NIK KTP">
											</div>
										</div><!-- col-4 -->
										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Berat Badan: <span class="tx-danger">*</span></label>
											<input class="form-control" type="text" name="berat_badan" value="<?php echo $therapist->berat_badan; ?>" placeholder="Enter Berat Badan (Kg)">
											</div>
										</div><!-- col-4 -->
										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Tinggi Badan: <span class="tx-danger">*</span></label>
											<input class="form-control" type="text" name="tinggi_badan" value="<?php echo $therapist->tinggi_badan; ?>" placeholder="Enter Tinggi Badan (Cm)">
											</div>
										</div><!-- col-4 -->
										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">NIT: </label>
											<input class="form-control" type="text" name="nit" value="<?php echo $therapist->nit; ?>" placeholder="Enter NIT">
											</div>
										</div><!-- col-4 -->
										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Tempat Lahir: <span class="tx-danger">*</span></label>
											<input class="form-control" type="text" name="tempat_lahir" value="<?php echo $therapist->tempat_lahir; ?>" placeholder="Enter Tempat Lahir">
											</div>
										</div><!-- col-4 -->
										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Tanggal Lahir: <span class="tx-danger">*</span></label>
											<input type="text" class="form-control datepicker" name="tgl_lahir" value="<?php echo $therapist->tgl_lahir; ?>" placeholder="YYYY-MM-DD">
											</div>
										</div><!-- col-4 -->
									</div>
									
									<div class="row">
										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Wajah: <span class="tx-danger">*</span></label>
											<input class="form-control" type="text" name="wajah" value="<?php echo $therapist->wajah; ?>" placeholder="Enter Wajah">
											</div>
										</div><!-- col-4 -->
										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Phone Number: <span class="tx-danger">*</span></label>
											<input class="form-control" type="text" name="phone_number" value="<?php echo $therapist->phone_number; ?>" placeholder="Enter Phone Number">
											</div>
										</div><!-- col-4 -->
										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Riwayat Penyakit: <span class="tx-danger">*</span></label>
											<input class="form-control" type="text" name="riwayat_penyakit" value="<?php echo $therapist->riwayat_penyakit; ?>" placeholder="Enter Riwayat Penyakit">
											</div>
										</div><!-- col-4 -->
									</div>

									<div class="row">	
										<div class="col-lg-6">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Alamat: </label>
											<textarea rows="3" class="form-control" name="alamat" placeholder="Enter Alamat"><?php echo $therapist->alamat; ?></textarea>
											</div>
										</div><!-- col-4 -->								
									</div>
																
									<div class="row">

										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Agama: <span class="tx-danger">*</span></label>
											<select class="form-control"  name="agama" data-placeholder="Pilih Agama">
												<option label="- Pilih Agama -"></option>
												<option value="islam" <?php echo ($therapist->agama == 'islam' ? 'selected' : ''); ?>>Islam</option>
												<option value="Protestan" <?php echo ($therapist->agama == 'Protestan' ? 'selected' : ''); ?>>Protestan</option>
												<option value="katolik" <?php echo ($therapist->agama == 'katolik' ? 'selected' : ''); ?>>katolik</option>
												<option value="hindu" <?php echo ($therapist->agama == 'hindu' ? 'selected' : ''); ?>>Hindu</option>
												<option value="budha" <?php echo ($therapist->agama == 'budha' ? 'selected' : ''); ?>>Budha</option>
												<option value="konghucu" <?php echo ($therapist->agama == 'konghucu' ? 'selected' : ''); ?>>Konghucu</option>
											</select>
											</div>
										</div><!-- col-4 -->

										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Kewarganegaraan: <span class="tx-danger">*</span></label>
											<input class="form-control" type="text" name="kewarganegaraan" value="<?php echo $therapist->kewarganegaraan; ?>" placeholder="Enter Kewarganegaraan">
											</div>
										</div><!-- col-4 -->
									</div>

									<div class="row mg-b-25">
										

										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Nama Kerabat: </label>
											<input class="form-control" type="text" name="nama_kerabat" value="<?php echo $therapist->nama_kerabat; ?>" placeholder="Enter Nama Kerabat">
											</div>
										</div><!-- col-4 -->

										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">No. Telp Kerabat: </label>
											<input class="form-control" type="text" name="telp_kerabat" value="<?php echo $therapist->telp_kerabat; ?>" placeholder="Enter No. Telp Kerabat">
											</div>
										</div><!-- col-4 -->

										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Hubungan Kerabat: </label>
											<input class="form-control" type="text" name="hub_kerabat" value="<?php echo $therapist->hub_kerabat; ?>" placeholder="Enter Hubungan Kerabat">
											</div>
										</div><!-- col-4 -->
										
										<div class="col-lg-4">
											<div class="form-group">
											<label class="control-label col-xs-3" for="phoneNumber">Supplier:</label>
											<select class="form-control" name="supplier_id" id="supplier_id"  data-placeholder="Choose Supplier">
												<option label="- Pilih Supplier -"></option>
												<?php foreach($suppliers as $supplier){ ?>
													<option value="<?php echo $supplier->supplier_id; ?>" <?php echo ($therapist->supplier_id == $supplier->supplier_id ? 'selected' : ''); ?>><?php echo $supplier->name; ?></option>
												<?php } ?>
											</select>
											</div>
										</div><!-- col-4 -->

										<div class="col-lg-4">
											<div class="form-group mg-b-10-force">
											<label class="form-control-label">Awal Kontrak: </label>
											<input type="text" class="form-control datepicker" name="awal_kontrak" placeholder="YYYY-MM-DD">
											</div>
										</div><!-- col-4 -->
									</div><!-- row -->

									<div class="form-layout-footer">
										<button class="btn btn-info" name="flag" value="flag" >Submit</button>
										<button class="btn btn-secondary">Cancel</button>
									</div><!-- form-layout-footer -->

								</form><!-- form-layout -->
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>

        <div class="tab-pane fade show" id="document">
          
		  	<div class="br-pagebody">
				<div class="br-section-wrapper">
					<div class="row">
						<div class="col-xl-12 mg-t-0 mg-b-0">
							<div class="form-layout form-layout-1">

								<form action="<?php echo base_url(); ?>therapist/update_document_therapist/<?php echo $therapist->therapist_id; ?>" class="form-horizontal" id="therapist_form" method="post" enctype="multipart/form-data">

									<input class="form-control" type="hidden" name="therapist_id" value="<?php echo $therapist->therapist_id; ?>" placeholder="Enter Fullname">
									
									<h5 class="tx-inverse tx-normal tx-roboto">Document Detail</h5>
									
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Photo: </label>
												<input class="form-control" name="image_1" type="file">
											</div>
										</div><!-- col-6 -->
									</div>
									
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Formulir Riwayat Kesehatan: </label>
												<input class="form-control" name="formulir_kesehatan" type="file">
											</div>
										</div><!-- col-6 -->
									</div>
									
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Formulir Pemeriksaan Fisik (Cek Body): </label>
												<input class="form-control" name="formulir_cek_body" type="file">
											</div>
										</div><!-- col-6 -->
									</div>

									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Pernyataan Tidak Pernah Kerja di SPA++: </label>
												<input class="form-control" name="pernyataan_spa_plus" type="file">
											</div>
										</div><!-- col-6 -->
									</div>

									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Pernyataan Penjelasan Ketentuan Kerja: </label>
												<input class="form-control" name="pernyataan_ketentuan_kerja" type="file">
											</div>
										</div><!-- col-6 -->
									</div>

									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Formulir Ciawi: </label>
												<input class="form-control" name="formulir_ciawi" type="file">
											</div>
										</div><!-- col-6 -->
									</div>

									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Formulir Kesanggupan: </label>
												<input class="form-control" name="formulir_kesanggupan" type="file">
											</div>
										</div><!-- col-6 -->
									</div>

									<div class="form-layout-footer">
										<button class="btn btn-info" name="flag" value="flag" >Submit</button>
										<button class="btn btn-secondary">Cancel</button>
									</div><!-- form-layout-footer -->

								</form><!-- form-layout -->
								
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

        <div class="tab-pane fade show" id="family">
          
		  	<div class="br-pagebody">
				<div class="br-section-wrapper">
					<div class="row">
						<div class="col-xl-12 mg-t-0 mg-b-0">
							<div class="form-layout form-layout-1">

								<form action="<?php echo base_url(); ?>therapist/update_family_therapist/<?php echo $therapist->therapist_id; ?>" class="form-horizontal" id="therapist_form" method="post" enctype="multipart/form-data">

									<input class="form-control" type="hidden" name="therapist_id" value="<?php echo $therapist->therapist_id; ?>" placeholder="Enter Fullname">
									
									<h5 class="tx-inverse tx-normal tx-roboto">Data Keluarga</h5>

									<div class="row">

										<div class="col-lg-12">
											<div class="form-group">
												<label class="form-control-label">Status Perkawinan: <span class="tx-danger">*</span></label>
												<div class="col-lg-12">
													<div class="row">
														<div class="col-lg-3 mg-lg-t-0">
															<label class="rdiobox">
																<input type="radio" value="menikah" name="status_perkawinan" >
																<span>Menikah</span>
															</label>
														</div>
														<div class="col-lg-3 mg-lg-t-0">
															<label class="rdiobox">
																<input type="radio" value="belum_menikah" name="status_perkawinan">
																<span>Belum Menikah</span>
															</label>
														</div>
														<div class="col-lg-3 mg-lg-t-0">
															<label class="rdiobox">
																<input type="radio" value="cerai" name="status_perkawinan">
																<span>Cerai (Janda)</span>
															</label>
														</div>
													</div>
												</div>
											</div>
										</div><!-- col-12 -->										

										<div class="col-lg-12">
											<h5 class="tx-inverse tx-normal tx-roboto">Data Suami & Anak (bila menikah)</h5>
										</div>

										<div class="col-lg-6">

											<div class="form-group">
												<label class="form-control-label">Nama Suami: <span class="tx-danger">*</span></label>
												<input class="form-control" type="text" name="nama_suami" value="<?php echo $therapist->fullname; ?>" placeholder="Enter Nama Suami">
											</div>
										</div><!-- col-6 -->

										<div class="col-lg-3">
											<div class="form-group">
												<label class="form-control-label">Tempat & Tgl Lahir Suami: </label>
												<input class="form-control" type="text" name="tempat_lahir_suami" value="<?php echo $therapist->fullname; ?>" placeholder="Enter Tempat Lahir Suami">
											</div>
										</div><!-- col-6 -->

										<div class="col-lg-3">
											<div class="form-group">
												<label class="form-control-label">&nbsp;</label>
												<input type="text" class="form-control datepicker" name="tanggal_lahir_suami" placeholder="YYYY-MM-DD" value="<?php echo $therapist->fullname; ?>">
											</div>
										</div><!-- col-6 -->

										<div class="col-lg-6">
											<div class="form-group">
											<label class="form-control-label">Pekerjaan: </label>
											<input class="form-control" type="text" name="pekerjaan" value="<?php echo $therapist->fullname; ?>" placeholder="Enter Pekerjaan">
											</div>
										</div><!-- col-4 -->

										<div class="col-lg-12">
											<h5 class="tx-inverse tx-normal tx-roboto">Anak</h5>
										</div>

										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Anak ke-1: </label>
												<input class="form-control" type="text" name="anak1" value="<?php echo $therapist->fullname; ?>" placeholder="Enter Anak Ke-1">
											</div>
										</div><!-- col-6 -->
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Anak ke-2: </label>
												<input class="form-control" type="text" name="anak2" value="<?php echo $therapist->fullname; ?>" placeholder="Enter Anak Ke-2">
											</div>
										</div><!-- col-6 -->
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Anak ke-3: </label>
												<input class="form-control" type="text" name="anak3" value="<?php echo $therapist->fullname; ?>" placeholder="Enter Anak Ke-3">
											</div>
										</div><!-- col-6 -->
										<div class="col-lg-6">
											<div class="form-group">
												<label class="form-control-label">Anak ke-4: </label>
												<input class="form-control" type="text" name="anak4 value="<?php echo $therapist->fullname; ?>" placeholder="Enter Anak Ke-4">
											</div>
										</div><!-- col-6 -->
																				
									</div>

									<div class="form-layout-footer">
										<button class="btn btn-info" name="flag" value="flag" >Submit</button>
										<button class="btn btn-secondary">Cancel</button>
									</div><!-- form-layout-footer -->

								</form><!-- form-layout -->
								
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="tab-pane fade show" id="pengalaman">
          
		  	<div class="br-pagebody">
				<div class="br-section-wrapper">
					<div class="row">
						<div class="col-xl-12 mg-t-0 mg-b-0">
							<div class="form-layout form-layout-1">

								<form action="<?php echo base_url(); ?>therapist/update_pengalaman_therapist/<?php echo $therapist->therapist_id; ?>" class="form-horizontal" id="therapist_form" method="post" enctype="multipart/form-data">

									<input class="form-control" type="hidden" name="therapist_id" value="<?php echo $therapist->therapist_id; ?>" placeholder="Enter Fullname">
									
									<h5 class="tx-inverse tx-normal tx-roboto">Pengalaman Kerja</h5>

									<div class="row">
										
									</div>

									<div class="form-layout-footer">
										<button class="btn btn-info" name="flag" value="flag" >Submit</button>
										<button class="btn btn-secondary">Cancel</button>
									</div><!-- form-layout-footer -->

								</form><!-- form-layout -->
								
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>