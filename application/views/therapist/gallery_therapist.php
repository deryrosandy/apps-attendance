<div class="msg" style="">
  <?php echo $this->session->flashdata('msg'); ?>
</div>

<div class="br-pagebody">
  
  <div class="br-section-wrapper pd-20">
            
      <div class="row">
          
        <div class="col-xl-12 mg-t-0 mg-b-0">
          <div class="form-layout form-layout-5  border-0 pd-0">
            <div class="d-flex align-items-center justify-content-between">
              <h4 class="tx-inverse tx-normal tx-roboto mg-b-20"><?php echo $judul; ?></h4>
            </div>
            <div class="table-wrapper">
              <table id="datatables_therapist" class="table display responsive nowrap">
                <thead>
                  <tr>
                    <th class="wd-5p">No.</th>
                    <th class="wd-15p">Nama</th>
                    <th class="wd-10p">NIT</th>
                    <th class="wd-10p">Level</th>
                    <th class="wd-10p">Tgl Lahir</th>
                    <th class="wd-10p">No. Telp</th>

                    <?php if(($this->session->userdata('userdata')->user_type) =='superadmin'){ ?>
                      <th class="wd-10p">Akhir Kontrak</th>
                    <?php } ?>

                    <th class="wd-10p">Supplier</th>
                    <th class="wd-10p">Status</th>
                    <th class="wd-10p">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  <?php foreach ($therapists as $therapist): ?>
                    <tr>
                    <td><?php echo $no; ?></td>
                      <td><?php echo $therapist->fullname; ?></td>
                      <td><?php echo $therapist->nit; ?></td>
											<td><?php echo get_level_therapist_short($therapist->level); ?></td>
											<td><?php echo tgl_indo2($therapist->tgl_lahir); ?></td>                   
                      <td><?php echo $therapist->phone_number; ?></td> 
                      
                      <?php if(($this->session->userdata('userdata')->user_type) =='superadmin'){ ?>
                        <td><?php echo tgl_indo2($therapist->akhir_kontrak); ?></td>
                      <?php } ?>

                      <td><?php echo get_supplier_name($therapist->supplier_id); ?></td>
                      <td><?php echo get_status_therapist($therapist->status); ?></td>
                      <td>
                        <a href="<?php echo base_url(); ?>therapist/detail_gallery/<?php echo $therapist->therapist_id; ?>" data-toggle="tooltip-danger" data-placement="top"  title="Open Gallery" class="btn btn-info btn-sm"><i class="fa fa-image"></i></a>
                      </td>
                    </tr>
                    <?php $no++; ?>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div><!-- table-wrapper -->
          </div><!-- form-layout -->
        </div>
      
      </div>

  </div>
</div>