<div class="br-pagebody">
  
  <div class="br-section-wrapper pd-30">

    <div class="row">

      <div class="col-xl-8 offset-xl-2 mg-t-0 mg-b-0">
        <div class="form-layout form-layout-5  border-0 pd-y-0">
          <form id="form" role="form" enctype="multipart/form-data" action="<?php echo base_url() ?>therapist/mutasi_therapist" method="post" class="form-horizontal form-groups-bordered">
            <div class="row mg-t-10">
              <label class="col-sm-4 form-control-label"><strong> Select Branch:</strong></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <select class="form-control select2" name="branch_id" data-placeholder="Choose Branch">
                <option value="0">- Choose Branch -</option>
                <?php foreach ($branchs as $branch){ ?> 
                  <option value="<?php echo $branch->id; ?>"
                      <?php
                      if (!empty($branch_id)) {
                          echo $branch->id == $branch_id ? 'selected' : '';
                      }
                      ?>><?php echo $branch->name ?>
                  </option>
                <?php } ?>
              </select>
              </div>
            </div>
            <div class="row mg-t-10">
              <div class="col-sm-8 mg-l-auto">
                <div class="form-layout">
                  <button class="btn btn-info btn-sm" name="flag" value="1">Submit</button>
                </div><!-- form-layout-footer -->
              </div><!-- col-8 -->
            </div>
          </form>
        </div><!-- form-layout -->
      </div>
    
    </div>
  </div>
</div>
<div class="br-pagebody">
  
  <div class="br-section-wrapper pd-20">

    <?php if (!empty($flag)): ?>
            
      <div class="row">
          
        <div class="col-xl-12 mg-t-0 mg-b-0">
          <div class="form-layout form-layout-5  border-0 pd-0">
            <div class="d-flex align-items-center justify-content-between">
              <h4 class="tx-inverse tx-normal tx-roboto mg-b-20"><?php echo $judul; ?></h4>
              <h4 class="tx-inverse tx-normal tx-roboto mg-b-30">Branch : <?php echo get_branch_name($branch_id); ?></h4>
            </div>
            <div class="table-wrapper">
              <table id="datatables_mutasi" class="table display responsive nowrap">
                <thead>
                  <tr>
                    <th class="wd-5p">No.</th>
                    <th class="wd-5p">NIT</th>
                    <th class="wd-15p">Nama Lengkap</th>
                    <th class="wd-20p">Level</th>
                    <th class="wd-15p">Supplier</th>
                    <th class="wd-10p">Kontrak Ke</th>
                    <th class="wd-10p">Status</th>
                    <th class="wd-25p">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  <?php foreach ($therapists as $therapist): ?>
                    <tr>
                    <td><?php echo $no; ?></td>
                      <td><?php echo $therapist->nit; ?></td>
                      <td><?php echo $therapist->fullname; ?></td>
                      <td><?php echo get_level_therapist($therapist->level); ?></td>
                      <td><?php echo get_supplier_name($therapist->supplier_id); ?></td>
                      <td><?php echo $therapist->kontrak_ke; ?></td>
                      <td><?php echo get_status_therapist($therapist->status); ?></td>
                      <td>
                        <button data-toggle="modal" data-target="#modal_mutasi" data-branch-id="<?php echo $branch_id; ?>" data-id="<?php echo $therapist->therapist_id; ?>" data-toggle="tooltip-danger" data-placement="top"  title="Mutasi Therapist" class="btn btn-info btn-sm btn-mutasi">Mutasi <i class="menu-item-icon ion-ios-redo-outline"></i></button>
                      </td>
                    </tr>
                    <?php $no++; ?>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div><!-- table-wrapper -->
          </div><!-- form-layout -->
        </div>
      
      </div>

    <?php endif; ?>

  </div>
</div>

<div id="modal_mutasi" class="modal fade" aria-hidden="true">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content bd-0">
    <div class="modal-header pd-y-20 pd-x-25">
      <h4 class="mg-b-5 tx-inverse lh-2 tx-uppercase">MUTASI THERAPIST</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      
      <div class="modal-body pd-0">
        <div class="row no-gutters">          
          <div class="col-lg-12 bg-white">
            <div class="pd-30">
              
            <div class="form-layout border-0 pd-y-0">
              
                <h4 class="tx-inverse tx-normal tx-roboto mg-b-20 fullname"></h4>
                <input type="hidden" name="therapist_id" class="kodekar" />

                <h5 class="tx-inverse tx-normal tx-roboto mg-b-5">From Branch :</h5>

                <div class="alert alert-danger" role="alert">
                  <strong class="d-block d-sm-inline-block-force"><?php echo get_branch_name($branch_id); ?></strong>
                </div><!-- alert -->
                
                <h5 class="tx-inverse tx-normal tx-roboto mg-b-5">To Branch :</h5>

                <div class="form-group mg-b-20">
                  <select class="form-control select" name="branch_id" data-placeholder="Choose Branch">
                    <option value="0">- Choose Branch -</option>
                    <?php foreach ($branchs as $branch){ ?> 
                      <option value="<?php echo $branch->id; ?>"
                          <?php
                          if (!empty($branch_id)) {
                              echo $branch->id == $branch_id ? 'selected' : '';
                          }
                          ?>><?php echo $branch->name ?>
                      </option>
                    <?php } ?>
                  </select>
                </div><!-- form-group -->

                <button type="submit" name="flag" value="mutasi" class="btn btn-info pd-y-12 btn-block  tx-18">Mutasi <i class="menu-item-icon ion-ios-redo-outline"></i></button>

            </div><!-- pd-20 -->

          </div><!-- col-6 -->
        </div><!-- row -->
      </div><!-- modal-body -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div>