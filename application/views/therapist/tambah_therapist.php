<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="br-pagebody">
			<div class="br-section-wrapper">
				<div class="row">
					<div class="col-xl-12 mg-t-0 mg-b-0">
						<div class="form-layout form-layout-1">

							<form action="<?php echo base_url(); ?>therapist/save_therapist" class="form-horizontal" id="therapist_form" method="post" enctype="multipart/form-data">

								<div class="d-flex align-items-center justify-content-between">
									<h4 class="tx-inverse tx-normal tx-roboto mg-b-20"><?php echo $judul; ?></h4>
								</div>


								<h5 class="tx-inverse tx-normal tx-roboto">Data Diri</h5>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
										<label class="form-control-label">Fullname: <span class="tx-danger">*</span></label>
										<input class="form-control" type="text" name="fullname" value="" placeholder="Enter Fullname">
										</div>
									</div><!-- col-4 -->

									<div class="col-lg-6">
										<div class="form-group">
										<label class="form-control-label">Nickname: <span class="tx-danger">*</span></label>
										<input class="form-control" type="text" name="nickname" value="" placeholder="Enter Nickname">
										</div>
									</div><!-- col-4 -->
									
									<div class="col-lg-4">
										<div class="form-group">
										<label class="control-label col-xs-3" for="phoneNumber">No. KTP: <span class="tx-danger">*</span></label>
										<input type="text" class="form-control" name="no_ktp" id="no_ktp" placeholder="Enter NIK KTP">
										</div>
									</div><!-- col-4 -->
									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Berat Badan: <span class="tx-danger">*</span></label>
										<input class="form-control" type="text" name="berat_badan" value="" placeholder="Enter Berat Badan (Kg)">
										</div>
									</div><!-- col-4 -->
									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Tinggi Badan: <span class="tx-danger">*</span></label>
										<input class="form-control" type="text" name="tinggi_badan" value="" placeholder="Enter Tinggi Badan (Cm)">
										</div>
									</div><!-- col-4 -->
									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Tempat Lahir: <span class="tx-danger">*</span></label>
										<input class="form-control" type="text" name="tempat_lahir" value="" placeholder="Enter Tempat Lahir">
										</div>
									</div><!-- col-4 -->
									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Tanggal Lahir: <span class="tx-danger">*</span></label>
										<input type="text" class="form-control datepicker" name="tgl_lahir" placeholder="YYYY-MM-DD">
										</div>
									</div><!-- col-4 -->
								</div>
								
								<div class="row">
									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Wajah: <span class="tx-danger">*</span></label>
										<input class="form-control" type="text" name="wajah" value="" placeholder="Enter Wajah">
										</div>
									</div><!-- col-4 -->
									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Phone Number: <span class="tx-danger">*</span></label>
										<input class="form-control" type="text" name="phone_number" value="" placeholder="Enter Phone Number">
										</div>
									</div><!-- col-4 -->
									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Riwayat Penyakit: <span class="tx-danger">*</span></label>
										<input class="form-control" type="text" name="riwayat_penyakit" value="" placeholder="Enter Riwayat Penyakit">
										</div>
									</div><!-- col-4 -->
								</div>

								<div class="row">	
									<div class="col-lg-6">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Alamat: </label>
										<textarea rows="3"  name="alamat" class="form-control" placeholder="Enter Alamat"></textarea>
										</div>
									</div><!-- col-4 -->								
								</div>
															
								<div class="row">

									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Agama: <span class="tx-danger">*</span></label>
										<select class="form-control"  name="agama" data-placeholder="Pilih Agama">
											<option label="- Pilih Agama -"></option>
											<option value="islam">Islam</option>
											<option value="Protestan">Protestan</option>
											<option value="katolik">katolik</option>
											<option value="hindu">Hindu</option>
											<option value="budha">Budha</option>
											<option value="konghucu">Konghucu</option>
										</select>
										</div>
									</div><!-- col-4 -->

									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Kewarganegaraan: <span class="tx-danger">*</span></label>
										<input class="form-control" type="text" name="kewarganegaraan" value="" placeholder="Enter Kewarganegaraan">
										</div>
									</div><!-- col-4 -->
								</div>

								<div class="row mg-b-25">
									

									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Nama Kerabat: </label>
										<input class="form-control" type="text" name="nama_kerabat" value="" placeholder="Enter Nama Kerabat">
										</div>
									</div><!-- col-4 -->

									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">No. Telp Kerabat: </label>
										<input class="form-control" type="text" name="telp_kerabat" value="" placeholder="Enter No. Telp Kerabat">
										</div>
									</div><!-- col-4 -->

									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Hubungan Kerabat: </label>
										<input class="form-control" type="text" name="hub_kerabat" value="" placeholder="Enter Hubungan Kerabat">
										</div>
									</div><!-- col-4 -->
									
									<div class="col-lg-4">
										<div class="form-group">
										<label class="control-label col-xs-3" for="phoneNumber">Supplier:</label>
										<select class="form-control" name="supplier_id" id="supplier_id"  data-placeholder="Choose Supplier">
											<option label="- Pilih Supplier -"></option>
											<?php foreach($suppliers as $supplier){ ?>
												<option value="<?php echo $supplier->supplier_id; ?>"><?php echo $supplier->name; ?></option>
											<?php } ?>
										</select>
										</div>
									</div><!-- col-4 -->

									<div class="col-lg-4">
										<div class="form-group mg-b-10-force">
										<label class="form-control-label">Awal Kontrak: </label>
										<input type="text" class="form-control datepicker" name="awal_kontrak" placeholder="YYYY-MM-DD">
										</div>
									</div><!-- col-4 -->
								</div><!-- row -->

								<div class="form-layout-footer">
									<button class="btn btn-info" name="flag" value="flag" >Submit</button>
									<button class="btn btn-secondary">Cancel</button>
								</div><!-- form-layout-footer -->

							</form><!-- form-layout -->
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>