<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="br-pagebody">
	<div class="br-section-wrapper">
		<h6 class="br-section-label pull-left">List Data Voucher</h6>
		<div class="btn-group pull-right" role="group" aria-label="Basic example">
			<a href="#" class="btn btn-primary pd-x-25"><i class="fa fa-plus"></i> Voucher</a>
			<a href="#" class="btn btn-primary pd-x-25"><i class="fa fa-arrow-left"></i> Import</a>
		</div>
		<p class="br-section-text"></p>

		<div class="table-wrapper">
			<table id="data_voucher" class="table display responsive nowrap">
				<thead>
					<tr>
						<th class="wd-15p">No.</th>
						<th class="wd-15p">Barcode</th>
						<th class="wd-20p">Nominal</th>						
						<th class="wd-10p">Active Date</th>
						<th class="wd-25p">Expire Date</th>
						<th class="wd-15p">Status</th>
						<th class="wd-10p">Action</th>
					</tr>
				</thead>
				<tbody id="list_data_voucher">
					
				</tbody>
			</table>
		</div><!-- table-wrapper -->
	</div><!-- br-section-wrapper -->
</div>
      