<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-8">
		<div class="box">
			<div class="box-header row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Dari Tanggal :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="dari_tanggal"  data-date="2013-02-26" data-date-format="yyyy-mm-dd">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Sampai Tanggal :</label>

						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right datepicker" id="sampai_tanggal">
						</div>
						<!-- /.input group -->
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>&nbsp;</label>
						<div class="input-group date">
							<button id="button_filter_detail" class="btn btn-md btn-primary">Filter</button>
						</div>
					</div>
				</div>
			</div>
		  <!-- /.box-header -->
		  <div class="box-body">
			<table id="data-rekap-penjualan" class="tabel-report table table-bordered table-striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>Menu</th>
						<th>Quantity</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody id="">

				</tbody>
			</table>
		  </div>
		</div>
	</div>
	
</div>