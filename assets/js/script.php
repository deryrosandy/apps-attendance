<?php
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);


?>

<script type="text/javascript">

	window.onload = function() {
		$(function (){
			//$('.select2').select2();
		});
	}
	
	var table;

	$('#datatable1').DataTable({
		responsive: true,
		language: {
			searchPlaceholder: 'Search...',
			sSearch: '',
			lengthMenu: '_MENU_ items/page',
		}
	});

	$(document).ready(function() {
			$('#datatables_mutasi').DataTable( {
					
					"responsive": true,
					"language": {
						"searchPlaceholder": 'Search...',
						"sSearch": '',
					},
					"pageLength": 20,
					"lengthMenu": [ 10, 20, 50, 100, -1 ],
			});

			$(function(){
				$('#datatables_mutasi').on('click', 'tr', function () {			
					$('#datatables_mutasi').find('tr').removeClass('active');
					$(this).addClass('active');
				});
			})
	});

	$(document).ready(function() {
			$('#datatables_therapist').DataTable( {
					
					"responsive": true,
					"language": {
						"searchPlaceholder": 'Search...',
						"sSearch": '',
					},
					"pageLength": 20,
					"lengthMenu": [ 10, 20, 50, 100, -1 ],
			});

			$(function(){
				$('#datatables_therapist').on('click', 'tr', function () {			
					$('#datatables_therapist').find('tr').removeClass('active');
					$(this).addClass('active');
				});
			})
	});

	//Modal Mutasi Therapist
	$('#datatables_user').on('click', '.btn-user', function () {
			var id_user = $(this).attr('data-id');
		
			$.ajax({
				data: {"id_user" : id_user},
				url:"<?php echo site_url('master/ajax_get_detail_user')?>",
				type: "POST",
				dataType: "JSON",
				success:function(response){
					console.log(response);
					$("#modal_mutasi .fullname").append(response.fullname + ' - ' + response.nit);
					$("#modal_mutasi .kodekar").val(response.therapist_id);
				
				}
			})

	});

	//Modal Mutasi Therapist
	$('#datatables_mutasi').on('click', '.btn-mutasi', function () {

			$("#modal_mutasi .kodekar").val("");
			$("#modal_mutasi .fullname").html("");
			var kodekar = $(this).attr('data-id');
			var branch_id = $(this).attr('data-branch-id');
		
			$.ajax({
				data: {"kodekar" : kodekar},
				url:"<?php echo site_url('therapist/ajax_get_detail_therapist')?>",
				type: "POST",
				dataType: "JSON",
				success:function(response){
				
					$("#modal_mutasi .fullname").append(response.fullname + ' - ' + response.nit);
					$("#modal_mutasi .kodekar").val(response.therapist_id);
				
				}
			})

	});

	$("#modal_mutasi button[type=submit]").click(function(){

		var kodekar = $("#modal_mutasi .kodekar").val();
	  var branch_id = $("#modal_mutasi select[name=branch_id] option").filter(":selected").val();

		$("#modal_mutasi").modal('hide'); 

		swal({
				title: "Are you sure?",
				text: " ",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-warning",
				confirmButtonText: "Proses Mutasi!",
				closeOnConfirm: false
			},
			function () {
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('therapist/ajax_mutasi_therapist'); ?>",
					data: {"kodekar" : kodekar, "branch_id" : branch_id},
					success: function(msg){
						if(msg=='true'){
							swal("Success!", "Mutasi Berhasil", "success");
						}else{
							swal("Error", "Mutasi failed", "error");
						}
						location.reload();
					},
					error: function(){
						swal("Error", "Mutasi failed", "error");
					}
				});    	               
			}
		);

	});
	
	function list_data_voucher(){
		//datatables
		table = $('#data_voucher').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"lengthMenu": [ 10, 20, 40, 60, 80, 100 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "<?php echo site_url('voucher/ajax_list_data_voucher')?>",
				"type": "POST"
			},
			"draw" : false,

			//Set column definition initialisation properties.
			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": true, //set not orderable
				},
				{ "width": "10px", "targets": 0 },
				{ "width": "10px", "targets": 1 },
				{ "width": "10px", "targets": 2 },
				{ "width": "10px", "targets": 3 },
				{ "width": "20px", "targets": 4 },
				{ "width": "20px", "targets": 5 },
				{ "width": "20px", "targets": 6 },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('id', aData[2]);
			}

		});	
		
	};

	function formatCurrency(num) {

		//num = num.toString().replace(/\Rp|/g,'');
		if(isNaN(num))
			num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
			cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
			num = num.substring(0,num.length-(4*i+3))+'.'+
			num.substring(num.length-(4*i+3));
		
		return num;
	}

	function ConfirmDeleteUser(){
		$(document).on('click', '#delete_user', function(){
			var id_user	 = $(this).attr("id_user");
			
			var x = confirm("Are you sure you want to delete?");
			if (x){
				$.ajax({
					data: {"id_user" : id_user},
					url:"<?php echo site_url('master/delete_user')?>",
					type: "POST",
					success:function(response){
						location.reload();
					}
				});
			}else{
				return false;
			}
		});
	}

	function checkisNaN(num) {

		if(isNaN(num))
			num = 0;

		return num;
	}

	function branch_list(){

		$.ajax({
			url:"<?php echo base_url(); ?>ajax/get_branch_list",
			method:"POST",
			dataType:"json",
			success:function(response){
				$("#branch_id_list").html("");
				var str = "<option value='0'>- All Outlet -</option>";
				$("#branch_id_list").append(str);
				//console.log(response.length);
				var len = response.length;
				for(var i=0; i<len; i++){
					var csname = response[i].csname;
					var cname = response[i].cname;
					var str2 = "<option value='" + csname + "'>" + cname + "</option>";
					$("#branch_id_list").append(str2);
				}
				$('.modal-title').text("Tambah User");
				$('#addusersModal').modal('show');
			}
		})

	}

	function total_komisi_additional_therapist(){		
		var kodekar = $('#total_sesi_kar').attr('kodekar');
		var data_from = $('#total_sesi_kar').attr('data-from');
		var data_to = $('#total_sesi_kar').attr('data-to');	

		//datatables
		table = $('#komisi_add_product').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
			"ordering": false,
			"info":     false,
			"searching": false,
			"lengthMenu": [ 10, 20, 50, 75 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"kodekar" : kodekar, "dari_tanggal" : data_from, "sampai_tanggal" : data_to},
				"url": "<?php echo site_url('report/detail_additional_therapist')?>",
				"type": "POST"
			},

			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
				{ "width": "20px",  "className": "text-center",  "targets": 0 },
				{ "width": "40px",  "className": "text-center", "targets": 1 },
				{ "width": "200px",  "className": "text-center", "targets": 2 },
				{ "width": "80px", "className": "text-center",  "targets": 3 },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
				$(nRow).attr('branch_id', aData[1]);
			},
			/*
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api();
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
	
				// Total over all pages
				var totalpoint = api
					.column( 6 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					} );
	
				// Total over this page
				var totalsesi = api
					.column( 5, { page: 'current'} )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					} );
	
				// Update footer
				$( api.column( 5 ).footer() ).html(
					totalsesi
				);
				$( api.column( 6 ).footer() ).html(
					totalpoint
				);
			}
			*/

		});
	};

</script>